﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SanVicParishBooking
{
    public partial class LoginError : Form
    {
        public LoginError()
        {
            InitializeComponent();
        }

        private void btnLoginErrorOk_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
    }
}
