﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace SanVicParishBooking
{

    public partial class formBaptism : Form
    {
        private SanVicParishDb _dbInstance = SanVicParishDb.instance;
        private List<Control> _required = new List<Control>();
        private FormValidation _formValidation;
        private MainScreen _mainScreen;
        private int _baptismPersonId = 0;

        private string _birthCountry;
        private string _birthProvince;
        private string _birthCityState;

        private string _country;
        private string _province;
        private string _cityState;

        private int _birthCountryId;
        private int _birthProvinceId;
        private int _birthCityStateId;

        private int _countryId;
        private int _provinceId;
        private int _cityStateId;

        public int BaptismPersonId
        {
            get
            {
                return _baptismPersonId;
            }

            set
            {
                _baptismPersonId = value;
            }
        }

        public formBaptism( MainScreen mainScreen )
        {
            this._mainScreen = mainScreen;
            InitializeComponent();
            this.initializeRequiredFields();
        }

        public void populateFields()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("SELECT * FROM V_Baptism WHERE BaptismPersonId = @BaptismPersonId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@BaptismPersonId", this._baptismPersonId));

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        Console.WriteLine(rdr["BaptismPersonId"]);

                        this.txtBaptismFirstName.Text = (string)rdr["Firstname"];
                        this.txtBaptismMiddleName.Text = (string)rdr["MiddleName"];
                        this.txtBaptismLastName.Text = (string)rdr["LastName"];
                        this.dateBaptismDateBaptised.Text = (String)rdr["DateBaptized"];

                        this.comboBaptismGender.SelectedIndex = this.comboBaptismGender.FindString((string)rdr["Gender"]);
                        this.comboBaptismMinister.Text = rdr["MinisterFirstName"].ToString() + " " + rdr["MinisterMiddleName"].ToString() + " " + rdr["MinisterLastName"].ToString();
                        this.txtBaptismBookNumber.Text = rdr["BaptismBookNumber"].ToString();
                        this.txtBaptismPageNumber.Text = rdr["BaptismPageNumber"].ToString();
                        this.txtBaptismBirthAddress1.Text = (string)rdr["BirthAddress1"];
                        this.txtBaptismBirthAddress2.Text = (string)rdr["BirthAddress2"];

                        this._birthCountry = (string)rdr["BirthCountry"];
                        this._birthCountryId = (int)rdr["BirthCountryId"];
                        this._birthProvince = (string)rdr["BirthProvince"];
                        this._birthProvinceId = (int)rdr["BirthProvinceId"];
                        this._birthCityState = (string)rdr["BirthCityState"];
                        this._birthCityStateId = (int)rdr["BirthCityStateId"];

                        this.dateBaptismBirthDate.Text = (string)rdr["BirthDay"];

                        this.txtBaptismResidenceAddress1.Text = (string)rdr["Address1"];
                        this.txtBaptismResidenceAddress2.Text = (string)rdr["Address2"];

                        this._country = (string)rdr["Country"];
                        this._countryId = (int)rdr["CountryId"];
                        this._province = (string)rdr["Province"];
                        this._provinceId = (int)rdr["ProvinceId"];
                        this._cityState = (string)rdr["CityState"];
                        this._cityStateId = (int)rdr["CityStateId"];

                        this.txtBaptismFFirstName.Text = (string)rdr["FatherFirstName"];
                        this.txtBaptismFMiddleName.Text = (string)rdr["FatherMiddleName"];
                        this.txtBaptismFLastName.Text = (string)rdr["FatherLastName"];

                        this.txtBaptismMFirstName.Text = (string)rdr["MotherFirstName"];
                        this.txtBaptismMMiddleName.Text = (string)rdr["MotherMiddleName"];
                        this.txtBaptismMLastName.Text = (string)rdr["MotherLastName"];

                        this.txtBaptismS1FirstName.Text = (string)rdr["Sponsor1Firstname"];
                        this.txtBaptismS1MiddleName.Text = (string)rdr["Sponsor1MiddleName"];
                        this.txtBaptismS1LastName.Text = (string)rdr["Sponsor1LastName"];

                        this.txtBaptismS2FirstName.Text = (string)rdr["Sponsor2Firstname"];
                        this.txtBaptismS2MiddleName.Text = (string)rdr["Sponsor2MiddleName"];
                        this.txtBaptismS2LastName.Text = (string)rdr["Sponsor2LastName"];

                        this.btnBaptismReset.Enabled = false;
                    }
                }

            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                this._loadCountries();
            }
        }

        private void initializeRequiredFields()
        {
            string[] requiredFields = { 
                "txtBaptismFirstName",
                "txtBaptismMiddleName",
                "txtBaptismLastName",
                "dateBaptismDateBaptised",
                "comboBaptismGender",
                "comboBaptismMinister",
                "txtBaptismBookNumber",
                "txtBaptismPageNumber",
                "txtBaptismBirthAddress1",
                "comboBaptismBirthCountry",
                "comboBaptismBirthProvince",
                "comboBaptismBirthCity",
                "dateBaptismBirthDate",
                "txtBaptismResidenceAddress1",
                "comboBaptismResidenceCountry",
                "comboBaptismResidenceProvince",
                "comboBaptismResidenceCity",
                "txtBaptismFFirstName",
                "txtBaptismFMiddleName",
                "txtBaptismFLastName",
                "txtBaptismMFirstName",
                "txtBaptismMMiddleName",
                "txtBaptismMLastName"
            };

            this._formValidation = new FormValidation(this.Controls, requiredFields);
        }

        private void btnBaptismCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnBaptismReset_Click(object sender, EventArgs e)
        {
            this._resetAllFields();
        }

        private void _resetAllFields()
        {
            this.txtBaptismFirstName.Text = "";
            this.txtBaptismMiddleName.Text = "";
            this.txtBaptismLastName.Text = "";
            this.dateBaptismDateBaptised.Text = "";

            this.comboBaptismMinister.SelectedIndex = -1;
            this.txtBaptismBookNumber.Text = "";
            this.txtBaptismPageNumber.Text = "";

            this.txtBaptismBirthAddress1.Text = "";
            this.txtBaptismBirthAddress2.Text = "";
            this.comboBaptismBirthCountry.SelectedIndex = -1;
            this.comboBaptismBirthProvince.SelectedIndex = -1;
            this.comboBaptismBirthCity.SelectedIndex = -1;

            this.txtBaptismResidenceAddress1.Text = "";
            this.txtBaptismResidenceAddress2.Text = "";
            this.comboBaptismResidenceCountry.SelectedIndex = -1;
            this.comboBaptismResidenceProvince.SelectedIndex = -1;
            this.comboBaptismResidenceCity.SelectedIndex = -1;

            this.txtBaptismFFirstName.Text = "";
            this.txtBaptismFMiddleName.Text = "";
            this.txtBaptismFLastName.Text = "";
            this.txtBaptismMFirstName.Text = "";
            this.txtBaptismMMiddleName.Text = "";
            this.txtBaptismMLastName.Text = "";

            this.txtBaptismS1FirstName.Text = "";
            this.txtBaptismS1MiddleName.Text = "";
            this.txtBaptismS1LastName.Text = "";
            this.txtBaptismS2FirstName.Text = "";
            this.txtBaptismS2MiddleName.Text = "";
            this.txtBaptismS2LastName.Text = "";
        }

        private void formBaptism_Load(object sender, EventArgs e)
        {
            this._loadMinister();
            this._loadGender();

            if (this._baptismPersonId > 0)
            {
                this.populateFields();
            }
            else
            {
                this._loadCountries();
            }
        }

        private void _loadGender()
        {
            this.comboBaptismGender.Items.Add(new ComboBoxItem("Male", 1));
            this.comboBaptismGender.Items.Add(new ComboBoxItem("Female", 2));
        }

        private void _loadMinister()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            String ministerName = "";

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_Minister", conn);
                cmd.CommandType = CommandType.Text;

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ministerName = rdr["MinisterFirstName"].ToString() + " " + rdr["MinisterMiddleName"].ToString() + " " + rdr["MinisterLastName"].ToString();
                    this.comboBaptismMinister.Items.Add(new ComboBoxItem(ministerName, Int32.Parse(rdr["MinisterId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }
            }
        }

        private void _loadCountries()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM Country", conn);
                cmd.CommandType = CommandType.Text;

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboBaptismBirthCountry.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CountryId"].ToString())));
                    this.comboBaptismResidenceCountry.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CountryId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._baptismPersonId > 0)
                {
                    this.comboBaptismBirthCountry.Text = this._birthCountry;
                    this.comboBaptismResidenceCountry.Text = this._country;

                    this._loadBirthProvince( this._birthCountryId );
                    this._loadResidenceProvince( this._countryId );
                }
            }
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBaptismBirthCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int countryId;

            if ( selectedItem != null )
            {
                countryId = (int)selectedItem.Value;

                this._loadBirthProvince(countryId);

            }
        }

        private void _loadBirthProvince( int countryId )
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboBaptismBirthProvince.SelectedIndex = -1;
            this.comboBaptismBirthProvince.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_ProvinceCountry Where CountryId = @CountryId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@CountryId", countryId ));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboBaptismBirthProvince.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["ProvinceId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._baptismPersonId > 0)
                {
                    this.comboBaptismBirthProvince.Text = this._birthProvince;

                    this._loadBirthCity(this._birthProvinceId);
                }
            }
        }

        private void _loadBirthCity(int provinceId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboBaptismBirthCity.SelectedIndex = -1;
            this.comboBaptismBirthCity.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_CityProvince Where ProvinceId = @ProvinceId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ProvinceId", provinceId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboBaptismBirthCity.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CityId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._baptismPersonId > 0)
                {
                    this.comboBaptismBirthCity.Text = this._birthCityState;
                }
            }
        }

        private void comboBaptismBirthProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int provinceId;

            if (selectedItem != null)
            {
                provinceId = (int)selectedItem.Value;

                this._loadBirthCity(provinceId);

            }
        }

        private void comboBaptismResidenceCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int countryId;

            if (selectedItem != null)
            {
                countryId = (int)selectedItem.Value;

                this._loadResidenceProvince(countryId);

            }
        }

        private void _loadResidenceProvince(int countryId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboBaptismResidenceProvince.SelectedIndex = -1;
            this.comboBaptismResidenceProvince.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_ProvinceCountry Where CountryId = @CountryId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@CountryId", countryId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboBaptismResidenceProvince.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["ProvinceId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._baptismPersonId > 0)
                {
                    this.comboBaptismResidenceProvince.Text = this._province;

                    this._loadResidenceCity(this._provinceId);
                }
            }
        }

        private void comboBaptismResidenceProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int provinceId;

            if (selectedItem != null)
            {
                provinceId = (int)selectedItem.Value;

                this._loadResidenceCity(provinceId);

            }
        }

        private void _loadResidenceCity(int provinceId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboBaptismResidenceCity.SelectedIndex = -1;
            this.comboBaptismResidenceCity.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_CityProvince Where ProvinceId = @ProvinceId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ProvinceId", provinceId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboBaptismResidenceCity.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CityId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._baptismPersonId > 0)
                {
                    this.comboBaptismResidenceCity.Text = this._cityState;
                }
            }
        }

        private void btnBaptismSave_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            string gender;
            bool success = false;

            if (this._formValidation.Validate())
            {
                try
                {
                    conn = this._dbInstance.openConnection();

                    if (this._baptismPersonId > 0 )
                    {
                        cmd = new SqlCommand("SP_EditBaptism", conn);
                    }
                    else
                    {
                        cmd = new SqlCommand("SP_NewBaptism", conn);
                    }
                    
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Firstname", this.txtBaptismFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MiddleName", this.txtBaptismMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@LastName", this.txtBaptismLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@BirthAddress1", this.txtBaptismBirthAddress1.Text));
                    cmd.Parameters.Add(new SqlParameter("@BirthAddress2", this.txtBaptismBirthAddress2.Text));
                    cmd.Parameters.Add(new SqlParameter("@BirthCityState", (int)((ComboBoxItem)this.comboBaptismBirthCity.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@BirthProvince", (int)((ComboBoxItem)this.comboBaptismBirthProvince.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@BirthCountry", (int)((ComboBoxItem)this.comboBaptismBirthCountry.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@BirthDay", this.dateBaptismBirthDate.Value.ToString()));

                    if((int)((ComboBoxItem)this.comboBaptismBirthCountry.SelectedItem).Value == 1 ) {
                        gender = "Male";
                    } else {
                        gender = "Female";
                    }

                    cmd.Parameters.Add(new SqlParameter("@Gender", gender));
                    cmd.Parameters.Add(new SqlParameter("@MotherFirstName", this.txtBaptismMFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MotherMiddleName", this.txtBaptismMMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MotherLastName", this.txtBaptismMLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherFirstName", this.txtBaptismFFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherMiddleName", this.txtBaptismFMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherLastName", this.txtBaptismFLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Address1", this.txtBaptismResidenceAddress1.Text));
                    cmd.Parameters.Add(new SqlParameter("@Address2", this.txtBaptismResidenceAddress2.Text));
                    cmd.Parameters.Add(new SqlParameter("@CityState", (int)((ComboBoxItem)this.comboBaptismResidenceCity.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@Province", (int)((ComboBoxItem)this.comboBaptismResidenceProvince.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@Country", (int)((ComboBoxItem)this.comboBaptismResidenceCountry.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@DateBaptized", this.dateBaptismDateBaptised.Value.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@MinisterId", (int)((ComboBoxItem)this.comboBaptismMinister.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor1Firstname", this.txtBaptismS1FirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor1MiddleName", this.txtBaptismS1MiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor1LastName", this.txtBaptismS1LastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor2Firstname", this.txtBaptismS2FirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor2MiddleName", this.txtBaptismS2MiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor2LastName", this.txtBaptismS2LastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@BaptismBookNumber", this.txtBaptismBookNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@BaptismPageNumber", this.txtBaptismPageNumber.Text));

                    if (this._baptismPersonId > 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@BaptismPersonId", this._baptismPersonId));
                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", Session._UserLoginId));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@AddedBy", Session._UserLoginId));
                    }

                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        if ((int)rdr["BaptismPersonId"] > 0)
                        {
                            success = true;
                        } 
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        this._dbInstance.closeConnection();
                        conn = null;
                    }

                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr = null;
                    }

                    if (success)
                    {
                        MessageBox.Show(
                            "Successfully added/updated record",
                            "Success",
                            MessageBoxButtons.OK
                        );

                        this.Hide();
                    }
                }
            }
            
        }

        private void txtBaptismFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismMiddleName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void dateBaptismDateBaptised_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((DateTimePicker)sender).Text))
            {
                this._formValidation.HideError(((DateTimePicker)sender).Name);
            }
        }

        private void comboBaptismMinister_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void txtBaptismPurpose_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismBookNumber_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismPageNumber_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismBirthAddress1_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void comboBaptismBirthCountry_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboBaptismBirthProvince_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboBaptismBirthCity_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void dateBaptismBirthDate_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((DateTimePicker)sender).Text))
            {
                this._formValidation.HideError(((DateTimePicker)sender).Name);
            }
        }

        private void txtBaptismResidenceAddress1_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void comboBaptismResidenceCountry_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboBaptismResidenceProvince_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboBaptismResidenceCity_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void txtBaptismFFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismFMiddleName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismFLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismMFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismMMiddleName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtBaptismMLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void formConfirmation_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._mainScreen.refreshDataGridBaptism();
        }
    }
}
