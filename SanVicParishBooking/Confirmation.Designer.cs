﻿namespace SanVicParishBooking
{
    partial class formConfirmation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConfCancel = new System.Windows.Forms.Button();
            this.comboConfBirthCity = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.comboConfBirthProvince = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboConfBirthCountry = new System.Windows.Forms.ComboBox();
            this.txtConfS2LastName = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtConfS2FirstName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtConfS1LastName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtConfBirthAddress2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtConfBirthAddress1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtConfS2MiddleName = new System.Windows.Forms.TextBox();
            this.btnConfReset = new System.Windows.Forms.Button();
            this.txtConfPageNumber = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.comboConfMinister = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtConfS1MiddleName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboConfGender = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.dateConfDateBaptised = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtConfLastName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtConfMiddleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtConfFirstName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtConfMLastName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtConfMMiddleName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtConfMFirstName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtConfFLastName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtConfFMiddleName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtConfFFirstName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtConfS1FirstName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtConfBookNumber = new System.Windows.Forms.TextBox();
            this.btnConfSave = new System.Windows.Forms.Button();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnConfCancel
            // 
            this.btnConfCancel.Location = new System.Drawing.Point(972, 395);
            this.btnConfCancel.Name = "btnConfCancel";
            this.btnConfCancel.Size = new System.Drawing.Size(75, 33);
            this.btnConfCancel.TabIndex = 34;
            this.btnConfCancel.Text = "Cancel";
            this.btnConfCancel.UseVisualStyleBackColor = true;
            this.btnConfCancel.Click += new System.EventHandler(this.btnConfCancel_Click);
            // 
            // comboConfBirthCity
            // 
            this.comboConfBirthCity.DisplayMember = "Name";
            this.comboConfBirthCity.FormattingEnabled = true;
            this.comboConfBirthCity.Location = new System.Drawing.Point(418, 81);
            this.comboConfBirthCity.Name = "comboConfBirthCity";
            this.comboConfBirthCity.Size = new System.Drawing.Size(76, 21);
            this.comboConfBirthCity.TabIndex = 15;
            this.comboConfBirthCity.ValueMember = "CityId";
            this.comboConfBirthCity.Leave += new System.EventHandler(this.comboConfBirthCity_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(327, 84);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 21;
            this.label25.Text = "City/Municipality:";
            // 
            // comboConfBirthProvince
            // 
            this.comboConfBirthProvince.DisplayMember = "Name";
            this.comboConfBirthProvince.FormattingEnabled = true;
            this.comboConfBirthProvince.Location = new System.Drawing.Point(238, 81);
            this.comboConfBirthProvince.Name = "comboConfBirthProvince";
            this.comboConfBirthProvince.Size = new System.Drawing.Size(71, 21);
            this.comboConfBirthProvince.TabIndex = 14;
            this.comboConfBirthProvince.ValueMember = "ProvinceId";
            this.comboConfBirthProvince.SelectedIndexChanged += new System.EventHandler(this.comboConfBirthProvince_SelectedIndexChanged);
            this.comboConfBirthProvince.Leave += new System.EventHandler(this.comboConfBirthProvince_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(180, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Province:";
            // 
            // comboConfBirthCountry
            // 
            this.comboConfBirthCountry.DisplayMember = "Name";
            this.comboConfBirthCountry.FormattingEnabled = true;
            this.comboConfBirthCountry.Location = new System.Drawing.Point(94, 79);
            this.comboConfBirthCountry.Name = "comboConfBirthCountry";
            this.comboConfBirthCountry.Size = new System.Drawing.Size(80, 21);
            this.comboConfBirthCountry.TabIndex = 13;
            this.comboConfBirthCountry.ValueMember = "Value";
            this.comboConfBirthCountry.SelectedIndexChanged += new System.EventHandler(this.comboConfBirthCountry_SelectedIndexChanged);
            this.comboConfBirthCountry.Leave += new System.EventHandler(this.comboConfBirthCountry_Leave);
            // 
            // txtConfS2LastName
            // 
            this.txtConfS2LastName.Location = new System.Drawing.Point(343, 75);
            this.txtConfS2LastName.Name = "txtConfS2LastName";
            this.txtConfS2LastName.Size = new System.Drawing.Size(153, 20);
            this.txtConfS2LastName.TabIndex = 31;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 84);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Country:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(267, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Last Name:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(267, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Middle Name:";
            // 
            // txtConfS2FirstName
            // 
            this.txtConfS2FirstName.Location = new System.Drawing.Point(343, 21);
            this.txtConfS2FirstName.Name = "txtConfS2FirstName";
            this.txtConfS2FirstName.Size = new System.Drawing.Size(153, 20);
            this.txtConfS2FirstName.TabIndex = 29;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(267, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "First Name:";
            // 
            // txtConfS1LastName
            // 
            this.txtConfS1LastName.Location = new System.Drawing.Point(99, 77);
            this.txtConfS1LastName.Name = "txtConfS1LastName";
            this.txtConfS1LastName.Size = new System.Drawing.Size(147, 20);
            this.txtConfS1LastName.TabIndex = 28;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Last Name:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.comboConfBirthCity);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.comboConfBirthProvince);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.comboConfBirthCountry);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.txtConfBirthAddress2);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.txtConfBirthAddress1);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Location = new System.Drawing.Point(10, 187);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(516, 121);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Confirmation Place";
            // 
            // txtConfBirthAddress2
            // 
            this.txtConfBirthAddress2.Location = new System.Drawing.Point(94, 53);
            this.txtConfBirthAddress2.Name = "txtConfBirthAddress2";
            this.txtConfBirthAddress2.Size = new System.Drawing.Size(400, 20);
            this.txtConfBirthAddress2.TabIndex = 12;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Address 2:";
            // 
            // txtConfBirthAddress1
            // 
            this.txtConfBirthAddress1.Location = new System.Drawing.Point(94, 27);
            this.txtConfBirthAddress1.Name = "txtConfBirthAddress1";
            this.txtConfBirthAddress1.Size = new System.Drawing.Size(400, 20);
            this.txtConfBirthAddress1.TabIndex = 11;
            this.txtConfBirthAddress1.Leave += new System.EventHandler(this.txtConfBirthAddress1_Leave);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 8;
            this.label24.Text = "Address:";
            // 
            // txtConfS2MiddleName
            // 
            this.txtConfS2MiddleName.Location = new System.Drawing.Point(343, 49);
            this.txtConfS2MiddleName.Name = "txtConfS2MiddleName";
            this.txtConfS2MiddleName.Size = new System.Drawing.Size(153, 20);
            this.txtConfS2MiddleName.TabIndex = 30;
            // 
            // btnConfReset
            // 
            this.btnConfReset.Location = new System.Drawing.Point(889, 395);
            this.btnConfReset.Name = "btnConfReset";
            this.btnConfReset.Size = new System.Drawing.Size(75, 33);
            this.btnConfReset.TabIndex = 33;
            this.btnConfReset.Text = "Reset";
            this.btnConfReset.UseVisualStyleBackColor = true;
            this.btnConfReset.Click += new System.EventHandler(this.btnConfReset_Click);
            // 
            // txtConfPageNumber
            // 
            this.txtConfPageNumber.Location = new System.Drawing.Point(703, 94);
            this.txtConfPageNumber.Name = "txtConfPageNumber";
            this.txtConfPageNumber.Size = new System.Drawing.Size(244, 20);
            this.txtConfPageNumber.TabIndex = 9;
            this.txtConfPageNumber.Leave += new System.EventHandler(this.txtConfPageNumber_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(534, 97);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 13);
            this.label20.TabIndex = 54;
            this.label20.Text = "Page Number:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(533, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(75, 13);
            this.label19.TabIndex = 51;
            this.label19.Text = "Book Number:";
            // 
            // comboConfMinister
            // 
            this.comboConfMinister.DisplayMember = "Name";
            this.comboConfMinister.FormattingEnabled = true;
            this.comboConfMinister.Location = new System.Drawing.Point(703, 37);
            this.comboConfMinister.Name = "comboConfMinister";
            this.comboConfMinister.Size = new System.Drawing.Size(244, 21);
            this.comboConfMinister.TabIndex = 7;
            this.comboConfMinister.ValueMember = "Value";
            this.comboConfMinister.Leave += new System.EventHandler(this.comboConfMinister_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(534, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 13);
            this.label17.TabIndex = 50;
            this.label17.Text = "Minister:";
            // 
            // txtConfS1MiddleName
            // 
            this.txtConfS1MiddleName.Location = new System.Drawing.Point(99, 51);
            this.txtConfS1MiddleName.Name = "txtConfS1MiddleName";
            this.txtConfS1MiddleName.Size = new System.Drawing.Size(147, 20);
            this.txtConfS1MiddleName.TabIndex = 27;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboConfGender);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.dateConfDateBaptised);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtConfLastName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtConfMiddleName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtConfFirstName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(11, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 171);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Confirmation Candidate";
            // 
            // comboConfGender
            // 
            this.comboConfGender.DisplayMember = "Name";
            this.comboConfGender.FormattingEnabled = true;
            this.comboConfGender.Location = new System.Drawing.Point(94, 134);
            this.comboConfGender.Name = "comboConfGender";
            this.comboConfGender.Size = new System.Drawing.Size(400, 21);
            this.comboConfGender.TabIndex = 6;
            this.comboConfGender.ValueMember = "Value";
            this.comboConfGender.Leave += new System.EventHandler(this.comboConfGender_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(14, 139);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "Gender:";
            // 
            // dateConfDateBaptised
            // 
            this.dateConfDateBaptised.Location = new System.Drawing.Point(94, 108);
            this.dateConfDateBaptised.Name = "dateConfDateBaptised";
            this.dateConfDateBaptised.Size = new System.Drawing.Size(400, 20);
            this.dateConfDateBaptised.TabIndex = 5;
            this.dateConfDateBaptised.Leave += new System.EventHandler(this.dateConfDateBaptised_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Date Confirmed:";
            // 
            // txtConfLastName
            // 
            this.txtConfLastName.Location = new System.Drawing.Point(94, 82);
            this.txtConfLastName.Name = "txtConfLastName";
            this.txtConfLastName.Size = new System.Drawing.Size(400, 20);
            this.txtConfLastName.TabIndex = 4;
            this.txtConfLastName.Leave += new System.EventHandler(this.txtConfLastName_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Last Name:";
            // 
            // txtConfMiddleName
            // 
            this.txtConfMiddleName.Location = new System.Drawing.Point(94, 55);
            this.txtConfMiddleName.Name = "txtConfMiddleName";
            this.txtConfMiddleName.Size = new System.Drawing.Size(400, 20);
            this.txtConfMiddleName.TabIndex = 3;
            this.txtConfMiddleName.Leave += new System.EventHandler(this.txtConfMiddleName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Middle Name:";
            // 
            // txtConfFirstName
            // 
            this.txtConfFirstName.Location = new System.Drawing.Point(94, 29);
            this.txtConfFirstName.Name = "txtConfFirstName";
            this.txtConfFirstName.Size = new System.Drawing.Size(400, 20);
            this.txtConfFirstName.TabIndex = 2;
            this.txtConfFirstName.Leave += new System.EventHandler(this.txtConfFirstName_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "First Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(531, 187);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(516, 144);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parents";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtConfMLastName);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtConfMMiddleName);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtConfMFirstName);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(262, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 106);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Mother";
            // 
            // txtConfMLastName
            // 
            this.txtConfMLastName.Location = new System.Drawing.Point(84, 73);
            this.txtConfMLastName.Name = "txtConfMLastName";
            this.txtConfMLastName.Size = new System.Drawing.Size(135, 20);
            this.txtConfMLastName.TabIndex = 24;
            this.txtConfMLastName.Leave += new System.EventHandler(this.txtConfMLastName_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Last Name:";
            // 
            // txtConfMMiddleName
            // 
            this.txtConfMMiddleName.Location = new System.Drawing.Point(84, 47);
            this.txtConfMMiddleName.Name = "txtConfMMiddleName";
            this.txtConfMMiddleName.Size = new System.Drawing.Size(135, 20);
            this.txtConfMMiddleName.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Middle Name:";
            // 
            // txtConfMFirstName
            // 
            this.txtConfMFirstName.Location = new System.Drawing.Point(84, 19);
            this.txtConfMFirstName.Name = "txtConfMFirstName";
            this.txtConfMFirstName.Size = new System.Drawing.Size(135, 20);
            this.txtConfMFirstName.TabIndex = 22;
            this.txtConfMFirstName.Leave += new System.EventHandler(this.txtConfMFirstName_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "First Name:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtConfFLastName);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtConfFMiddleName);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtConfFFirstName);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(12, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 106);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Father";
            // 
            // txtConfFLastName
            // 
            this.txtConfFLastName.Location = new System.Drawing.Point(84, 73);
            this.txtConfFLastName.Name = "txtConfFLastName";
            this.txtConfFLastName.Size = new System.Drawing.Size(136, 20);
            this.txtConfFLastName.TabIndex = 20;
            this.txtConfFLastName.Leave += new System.EventHandler(this.txtConfFLastName_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Last Name:";
            // 
            // txtConfFMiddleName
            // 
            this.txtConfFMiddleName.Location = new System.Drawing.Point(84, 47);
            this.txtConfFMiddleName.Name = "txtConfFMiddleName";
            this.txtConfFMiddleName.Size = new System.Drawing.Size(136, 20);
            this.txtConfFMiddleName.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Middle Name:";
            // 
            // txtConfFFirstName
            // 
            this.txtConfFFirstName.Location = new System.Drawing.Point(84, 19);
            this.txtConfFFirstName.Name = "txtConfFFirstName";
            this.txtConfFFirstName.Size = new System.Drawing.Size(136, 20);
            this.txtConfFFirstName.TabIndex = 18;
            this.txtConfFFirstName.Leave += new System.EventHandler(this.txtConfFFirstName_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "First Name:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtConfS2LastName);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.txtConfS2MiddleName);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtConfS2FirstName);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtConfS1LastName);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.txtConfS1MiddleName);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtConfS1FirstName);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(10, 314);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(515, 114);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sponsors";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Middle Name:";
            // 
            // txtConfS1FirstName
            // 
            this.txtConfS1FirstName.Location = new System.Drawing.Point(99, 23);
            this.txtConfS1FirstName.Name = "txtConfS1FirstName";
            this.txtConfS1FirstName.Size = new System.Drawing.Size(147, 20);
            this.txtConfS1FirstName.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "First Name:";
            // 
            // txtConfBookNumber
            // 
            this.txtConfBookNumber.Location = new System.Drawing.Point(703, 65);
            this.txtConfBookNumber.Name = "txtConfBookNumber";
            this.txtConfBookNumber.Size = new System.Drawing.Size(244, 20);
            this.txtConfBookNumber.TabIndex = 8;
            this.txtConfBookNumber.Leave += new System.EventHandler(this.txtConfBookNumber_Leave);
            // 
            // btnConfSave
            // 
            this.btnConfSave.Location = new System.Drawing.Point(808, 395);
            this.btnConfSave.Name = "btnConfSave";
            this.btnConfSave.Size = new System.Drawing.Size(75, 33);
            this.btnConfSave.TabIndex = 32;
            this.btnConfSave.Text = "Save";
            this.btnConfSave.UseVisualStyleBackColor = true;
            this.btnConfSave.Click += new System.EventHandler(this.btnConfSave_Click);
            // 
            // formConfirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1061, 445);
            this.ControlBox = false;
            this.Controls.Add(this.btnConfCancel);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.btnConfReset);
            this.Controls.Add(this.txtConfPageNumber);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.comboConfMinister);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.txtConfBookNumber);
            this.Controls.Add(this.btnConfSave);
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Name = "formConfirmation";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Confirmation";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formConfirmation_FormClosing);
            this.Load += new System.EventHandler(this.formConfirmation_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConfCancel;
        private System.Windows.Forms.ComboBox comboConfBirthCity;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboConfBirthProvince;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboConfBirthCountry;
        private System.Windows.Forms.TextBox txtConfS2LastName;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtConfS2FirstName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtConfS1LastName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtConfBirthAddress2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtConfBirthAddress1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtConfS2MiddleName;
        private System.Windows.Forms.Button btnConfReset;
        private System.Windows.Forms.TextBox txtConfPageNumber;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtConfS1MiddleName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboConfGender;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.DateTimePicker dateConfDateBaptised;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtConfLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtConfMiddleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtConfFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtConfMLastName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtConfMMiddleName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtConfMFirstName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtConfFLastName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtConfFMiddleName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtConfFFirstName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtConfS1FirstName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtConfBookNumber;
        private System.Windows.Forms.Button btnConfSave;
        private System.Windows.Forms.ComboBox comboConfMinister;
    }
}