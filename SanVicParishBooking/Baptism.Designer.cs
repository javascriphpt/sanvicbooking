﻿namespace SanVicParishBooking
{
    partial class formBaptism
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBaptismGender = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.dateBaptismDateBaptised = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBaptismLastName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBaptismMiddleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBaptismFirstName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtBaptismMLastName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBaptismMMiddleName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBaptismMFirstName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtBaptismFLastName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBaptismFMiddleName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBaptismFFirstName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtBaptismS2LastName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtBaptismS2MiddleName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtBaptismS2FirstName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBaptismS1LastName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtBaptismS1MiddleName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBaptismS1FirstName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnBaptismSave = new System.Windows.Forms.Button();
            this.btnBaptismReset = new System.Windows.Forms.Button();
            this.btnBaptismCancel = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dateBaptismBirthDate = new System.Windows.Forms.DateTimePicker();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBaptismBirthCity = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBaptismBirthProvince = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBaptismBirthCountry = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBaptismBirthAddress2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtBaptismBirthAddress1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.comboBaptismResidenceCity = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBaptismResidenceProvince = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboBaptismResidenceCountry = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtBaptismResidenceAddress2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtBaptismResidenceAddress1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.comboBaptismMinister = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtBaptismPageNumber = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtBaptismBookNumber = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBaptismGender);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.dateBaptismDateBaptised);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtBaptismLastName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtBaptismMiddleName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtBaptismFirstName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 171);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Baptismal Candidate";
            // 
            // comboBaptismGender
            // 
            this.comboBaptismGender.DisplayMember = "Name";
            this.comboBaptismGender.FormattingEnabled = true;
            this.comboBaptismGender.Location = new System.Drawing.Point(94, 134);
            this.comboBaptismGender.Name = "comboBaptismGender";
            this.comboBaptismGender.Size = new System.Drawing.Size(400, 21);
            this.comboBaptismGender.TabIndex = 5;
            this.comboBaptismGender.ValueMember = "Value";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(14, 139);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "Gender:";
            // 
            // dateBaptismDateBaptised
            // 
            this.dateBaptismDateBaptised.CustomFormat = "";
            this.dateBaptismDateBaptised.Location = new System.Drawing.Point(94, 108);
            this.dateBaptismDateBaptised.Name = "dateBaptismDateBaptised";
            this.dateBaptismDateBaptised.Size = new System.Drawing.Size(400, 20);
            this.dateBaptismDateBaptised.TabIndex = 4;
            this.dateBaptismDateBaptised.Leave += new System.EventHandler(this.dateBaptismDateBaptised_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Date Baptised:";
            // 
            // txtBaptismLastName
            // 
            this.txtBaptismLastName.Location = new System.Drawing.Point(94, 82);
            this.txtBaptismLastName.Name = "txtBaptismLastName";
            this.txtBaptismLastName.Size = new System.Drawing.Size(400, 20);
            this.txtBaptismLastName.TabIndex = 3;
            this.txtBaptismLastName.Leave += new System.EventHandler(this.txtBaptismLastName_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Last Name:";
            // 
            // txtBaptismMiddleName
            // 
            this.txtBaptismMiddleName.Location = new System.Drawing.Point(94, 55);
            this.txtBaptismMiddleName.Name = "txtBaptismMiddleName";
            this.txtBaptismMiddleName.Size = new System.Drawing.Size(400, 20);
            this.txtBaptismMiddleName.TabIndex = 2;
            this.txtBaptismMiddleName.Leave += new System.EventHandler(this.txtBaptismMiddleName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Middle Name:";
            // 
            // txtBaptismFirstName
            // 
            this.txtBaptismFirstName.Location = new System.Drawing.Point(94, 29);
            this.txtBaptismFirstName.Name = "txtBaptismFirstName";
            this.txtBaptismFirstName.Size = new System.Drawing.Size(400, 20);
            this.txtBaptismFirstName.TabIndex = 1;
            this.txtBaptismFirstName.Leave += new System.EventHandler(this.txtBaptismFirstName_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "First Name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(12, 338);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(516, 144);
            this.groupBox2.TabIndex = 23;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Parents";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtBaptismMLastName);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtBaptismMMiddleName);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txtBaptismMFirstName);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Location = new System.Drawing.Point(262, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 106);
            this.groupBox4.TabIndex = 28;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Mother";
            // 
            // txtBaptismMLastName
            // 
            this.txtBaptismMLastName.Location = new System.Drawing.Point(84, 73);
            this.txtBaptismMLastName.Name = "txtBaptismMLastName";
            this.txtBaptismMLastName.Size = new System.Drawing.Size(135, 20);
            this.txtBaptismMLastName.TabIndex = 31;
            this.txtBaptismMLastName.Leave += new System.EventHandler(this.txtBaptismMLastName_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Last Name:";
            // 
            // txtBaptismMMiddleName
            // 
            this.txtBaptismMMiddleName.Location = new System.Drawing.Point(84, 47);
            this.txtBaptismMMiddleName.Name = "txtBaptismMMiddleName";
            this.txtBaptismMMiddleName.Size = new System.Drawing.Size(135, 20);
            this.txtBaptismMMiddleName.TabIndex = 30;
            this.txtBaptismMMiddleName.Leave += new System.EventHandler(this.txtBaptismMMiddleName_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 50);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Middle Name:";
            // 
            // txtBaptismMFirstName
            // 
            this.txtBaptismMFirstName.Location = new System.Drawing.Point(84, 19);
            this.txtBaptismMFirstName.Name = "txtBaptismMFirstName";
            this.txtBaptismMFirstName.Size = new System.Drawing.Size(135, 20);
            this.txtBaptismMFirstName.TabIndex = 29;
            this.txtBaptismMFirstName.Leave += new System.EventHandler(this.txtBaptismMFirstName_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "First Name:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtBaptismFLastName);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtBaptismFMiddleName);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtBaptismFFirstName);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Location = new System.Drawing.Point(12, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 106);
            this.groupBox3.TabIndex = 24;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Father";
            // 
            // txtBaptismFLastName
            // 
            this.txtBaptismFLastName.Location = new System.Drawing.Point(84, 73);
            this.txtBaptismFLastName.Name = "txtBaptismFLastName";
            this.txtBaptismFLastName.Size = new System.Drawing.Size(136, 20);
            this.txtBaptismFLastName.TabIndex = 27;
            this.txtBaptismFLastName.Leave += new System.EventHandler(this.txtBaptismFLastName_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Last Name:";
            // 
            // txtBaptismFMiddleName
            // 
            this.txtBaptismFMiddleName.Location = new System.Drawing.Point(84, 47);
            this.txtBaptismFMiddleName.Name = "txtBaptismFMiddleName";
            this.txtBaptismFMiddleName.Size = new System.Drawing.Size(136, 20);
            this.txtBaptismFMiddleName.TabIndex = 26;
            this.txtBaptismFMiddleName.Leave += new System.EventHandler(this.txtBaptismFMiddleName_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Middle Name:";
            // 
            // txtBaptismFFirstName
            // 
            this.txtBaptismFFirstName.Location = new System.Drawing.Point(84, 19);
            this.txtBaptismFFirstName.Name = "txtBaptismFFirstName";
            this.txtBaptismFFirstName.Size = new System.Drawing.Size(136, 20);
            this.txtBaptismFFirstName.TabIndex = 25;
            this.txtBaptismFFirstName.Leave += new System.EventHandler(this.txtBaptismFFirstName_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "First Name:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtBaptismS2LastName);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.txtBaptismS2MiddleName);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtBaptismS2FirstName);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtBaptismS1LastName);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.txtBaptismS1MiddleName);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtBaptismS1FirstName);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Location = new System.Drawing.Point(534, 314);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(515, 114);
            this.groupBox5.TabIndex = 32;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sponsors";
            // 
            // txtBaptismS2LastName
            // 
            this.txtBaptismS2LastName.Location = new System.Drawing.Point(343, 75);
            this.txtBaptismS2LastName.Name = "txtBaptismS2LastName";
            this.txtBaptismS2LastName.Size = new System.Drawing.Size(153, 20);
            this.txtBaptismS2LastName.TabIndex = 38;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(267, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Last Name:";
            // 
            // txtBaptismS2MiddleName
            // 
            this.txtBaptismS2MiddleName.Location = new System.Drawing.Point(343, 49);
            this.txtBaptismS2MiddleName.Name = "txtBaptismS2MiddleName";
            this.txtBaptismS2MiddleName.Size = new System.Drawing.Size(153, 20);
            this.txtBaptismS2MiddleName.TabIndex = 37;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(267, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Middle Name:";
            // 
            // txtBaptismS2FirstName
            // 
            this.txtBaptismS2FirstName.Location = new System.Drawing.Point(343, 21);
            this.txtBaptismS2FirstName.Name = "txtBaptismS2FirstName";
            this.txtBaptismS2FirstName.Size = new System.Drawing.Size(153, 20);
            this.txtBaptismS2FirstName.TabIndex = 36;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(267, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "First Name:";
            // 
            // txtBaptismS1LastName
            // 
            this.txtBaptismS1LastName.Location = new System.Drawing.Point(99, 77);
            this.txtBaptismS1LastName.Name = "txtBaptismS1LastName";
            this.txtBaptismS1LastName.Size = new System.Drawing.Size(147, 20);
            this.txtBaptismS1LastName.TabIndex = 35;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Last Name:";
            // 
            // txtBaptismS1MiddleName
            // 
            this.txtBaptismS1MiddleName.Location = new System.Drawing.Point(99, 51);
            this.txtBaptismS1MiddleName.Name = "txtBaptismS1MiddleName";
            this.txtBaptismS1MiddleName.Size = new System.Drawing.Size(147, 20);
            this.txtBaptismS1MiddleName.TabIndex = 34;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Middle Name:";
            // 
            // txtBaptismS1FirstName
            // 
            this.txtBaptismS1FirstName.Location = new System.Drawing.Point(99, 23);
            this.txtBaptismS1FirstName.Name = "txtBaptismS1FirstName";
            this.txtBaptismS1FirstName.Size = new System.Drawing.Size(147, 20);
            this.txtBaptismS1FirstName.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(60, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "First Name:";
            // 
            // btnBaptismSave
            // 
            this.btnBaptismSave.Location = new System.Drawing.Point(810, 446);
            this.btnBaptismSave.Name = "btnBaptismSave";
            this.btnBaptismSave.Size = new System.Drawing.Size(75, 33);
            this.btnBaptismSave.TabIndex = 39;
            this.btnBaptismSave.Text = "Save";
            this.btnBaptismSave.UseVisualStyleBackColor = true;
            this.btnBaptismSave.Click += new System.EventHandler(this.btnBaptismSave_Click);
            // 
            // btnBaptismReset
            // 
            this.btnBaptismReset.Location = new System.Drawing.Point(891, 446);
            this.btnBaptismReset.Name = "btnBaptismReset";
            this.btnBaptismReset.Size = new System.Drawing.Size(75, 33);
            this.btnBaptismReset.TabIndex = 40;
            this.btnBaptismReset.Text = "Reset";
            this.btnBaptismReset.UseVisualStyleBackColor = true;
            this.btnBaptismReset.Click += new System.EventHandler(this.btnBaptismReset_Click);
            // 
            // btnBaptismCancel
            // 
            this.btnBaptismCancel.Location = new System.Drawing.Point(974, 446);
            this.btnBaptismCancel.Name = "btnBaptismCancel";
            this.btnBaptismCancel.Size = new System.Drawing.Size(75, 33);
            this.btnBaptismCancel.TabIndex = 41;
            this.btnBaptismCancel.Text = "Cancel";
            this.btnBaptismCancel.UseVisualStyleBackColor = true;
            this.btnBaptismCancel.Click += new System.EventHandler(this.btnBaptismCancel_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dateBaptismBirthDate);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.comboBaptismBirthCity);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.comboBaptismBirthProvince);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.comboBaptismBirthCountry);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.txtBaptismBirthAddress2);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.txtBaptismBirthAddress1);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Location = new System.Drawing.Point(12, 187);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(516, 145);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Birth Date and Place";
            // 
            // dateBaptismBirthDate
            // 
            this.dateBaptismBirthDate.Location = new System.Drawing.Point(94, 108);
            this.dateBaptismBirthDate.Name = "dateBaptismBirthDate";
            this.dateBaptismBirthDate.Size = new System.Drawing.Size(174, 20);
            this.dateBaptismBirthDate.TabIndex = 16;
            this.dateBaptismBirthDate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            this.dateBaptismBirthDate.Leave += new System.EventHandler(this.dateBaptismBirthDate_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 114);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(57, 13);
            this.label26.TabIndex = 24;
            this.label26.Text = "Birth Date:";
            // 
            // comboBaptismBirthCity
            // 
            this.comboBaptismBirthCity.DisplayMember = "Name";
            this.comboBaptismBirthCity.FormattingEnabled = true;
            this.comboBaptismBirthCity.Location = new System.Drawing.Point(418, 81);
            this.comboBaptismBirthCity.Name = "comboBaptismBirthCity";
            this.comboBaptismBirthCity.Size = new System.Drawing.Size(76, 21);
            this.comboBaptismBirthCity.TabIndex = 15;
            this.comboBaptismBirthCity.ValueMember = "CityId";
            this.comboBaptismBirthCity.Leave += new System.EventHandler(this.comboBaptismBirthCity_Leave);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(327, 84);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 21;
            this.label25.Text = "City/Municipality:";
            // 
            // comboBaptismBirthProvince
            // 
            this.comboBaptismBirthProvince.DisplayMember = "Name";
            this.comboBaptismBirthProvince.FormattingEnabled = true;
            this.comboBaptismBirthProvince.Location = new System.Drawing.Point(238, 81);
            this.comboBaptismBirthProvince.Name = "comboBaptismBirthProvince";
            this.comboBaptismBirthProvince.Size = new System.Drawing.Size(71, 21);
            this.comboBaptismBirthProvince.TabIndex = 14;
            this.comboBaptismBirthProvince.ValueMember = "ProvinceId";
            this.comboBaptismBirthProvince.SelectedIndexChanged += new System.EventHandler(this.comboBaptismBirthProvince_SelectedIndexChanged);
            this.comboBaptismBirthProvince.Leave += new System.EventHandler(this.comboBaptismBirthProvince_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(180, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 19;
            this.label21.Text = "Province:";
            // 
            // comboBaptismBirthCountry
            // 
            this.comboBaptismBirthCountry.DisplayMember = "Name";
            this.comboBaptismBirthCountry.FormattingEnabled = true;
            this.comboBaptismBirthCountry.Location = new System.Drawing.Point(94, 79);
            this.comboBaptismBirthCountry.Name = "comboBaptismBirthCountry";
            this.comboBaptismBirthCountry.Size = new System.Drawing.Size(80, 21);
            this.comboBaptismBirthCountry.TabIndex = 13;
            this.comboBaptismBirthCountry.ValueMember = "Value";
            this.comboBaptismBirthCountry.SelectedIndexChanged += new System.EventHandler(this.comboBaptismBirthCountry_SelectedIndexChanged);
            this.comboBaptismBirthCountry.Leave += new System.EventHandler(this.comboBaptismBirthCountry_Leave);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 84);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Country:";
            // 
            // txtBaptismBirthAddress2
            // 
            this.txtBaptismBirthAddress2.Location = new System.Drawing.Point(94, 53);
            this.txtBaptismBirthAddress2.Name = "txtBaptismBirthAddress2";
            this.txtBaptismBirthAddress2.Size = new System.Drawing.Size(400, 20);
            this.txtBaptismBirthAddress2.TabIndex = 12;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 56);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Address 2:";
            // 
            // txtBaptismBirthAddress1
            // 
            this.txtBaptismBirthAddress1.Location = new System.Drawing.Point(94, 27);
            this.txtBaptismBirthAddress1.Name = "txtBaptismBirthAddress1";
            this.txtBaptismBirthAddress1.Size = new System.Drawing.Size(400, 20);
            this.txtBaptismBirthAddress1.TabIndex = 11;
            this.txtBaptismBirthAddress1.Leave += new System.EventHandler(this.txtBaptismBirthAddress1_Leave);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(12, 30);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 8;
            this.label24.Text = "Address:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.comboBaptismResidenceCity);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.comboBaptismResidenceProvince);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.comboBaptismResidenceCountry);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.txtBaptismResidenceAddress2);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.txtBaptismResidenceAddress1);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Location = new System.Drawing.Point(534, 163);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(516, 145);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Residence";
            // 
            // comboBaptismResidenceCity
            // 
            this.comboBaptismResidenceCity.DisplayMember = "Name";
            this.comboBaptismResidenceCity.FormattingEnabled = true;
            this.comboBaptismResidenceCity.Location = new System.Drawing.Point(420, 81);
            this.comboBaptismResidenceCity.Name = "comboBaptismResidenceCity";
            this.comboBaptismResidenceCity.Size = new System.Drawing.Size(76, 21);
            this.comboBaptismResidenceCity.TabIndex = 22;
            this.comboBaptismResidenceCity.ValueMember = "CityId";
            this.comboBaptismResidenceCity.Leave += new System.EventHandler(this.comboBaptismResidenceCity_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(327, 84);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 13);
            this.label28.TabIndex = 21;
            this.label28.Text = "City/Municipality:";
            // 
            // comboBaptismResidenceProvince
            // 
            this.comboBaptismResidenceProvince.DisplayMember = "Name";
            this.comboBaptismResidenceProvince.FormattingEnabled = true;
            this.comboBaptismResidenceProvince.Location = new System.Drawing.Point(238, 81);
            this.comboBaptismResidenceProvince.Name = "comboBaptismResidenceProvince";
            this.comboBaptismResidenceProvince.Size = new System.Drawing.Size(71, 21);
            this.comboBaptismResidenceProvince.TabIndex = 21;
            this.comboBaptismResidenceProvince.ValueMember = "ProvinceId";
            this.comboBaptismResidenceProvince.SelectedIndexChanged += new System.EventHandler(this.comboBaptismResidenceProvince_SelectedIndexChanged);
            this.comboBaptismResidenceProvince.Leave += new System.EventHandler(this.comboBaptismResidenceProvince_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(180, 84);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 19;
            this.label29.Text = "Province:";
            // 
            // comboBaptismResidenceCountry
            // 
            this.comboBaptismResidenceCountry.DisplayMember = "Name";
            this.comboBaptismResidenceCountry.FormattingEnabled = true;
            this.comboBaptismResidenceCountry.Location = new System.Drawing.Point(78, 81);
            this.comboBaptismResidenceCountry.Name = "comboBaptismResidenceCountry";
            this.comboBaptismResidenceCountry.Size = new System.Drawing.Size(80, 21);
            this.comboBaptismResidenceCountry.TabIndex = 20;
            this.comboBaptismResidenceCountry.ValueMember = "CountryId";
            this.comboBaptismResidenceCountry.SelectedIndexChanged += new System.EventHandler(this.comboBaptismResidenceCountry_SelectedIndexChanged);
            this.comboBaptismResidenceCountry.Leave += new System.EventHandler(this.comboBaptismResidenceCountry_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 84);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 13);
            this.label30.TabIndex = 12;
            this.label30.Text = "Country:";
            // 
            // txtBaptismResidenceAddress2
            // 
            this.txtBaptismResidenceAddress2.Location = new System.Drawing.Point(78, 55);
            this.txtBaptismResidenceAddress2.Name = "txtBaptismResidenceAddress2";
            this.txtBaptismResidenceAddress2.Size = new System.Drawing.Size(418, 20);
            this.txtBaptismResidenceAddress2.TabIndex = 19;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 56);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(57, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Address 2:";
            // 
            // txtBaptismResidenceAddress1
            // 
            this.txtBaptismResidenceAddress1.Location = new System.Drawing.Point(78, 29);
            this.txtBaptismResidenceAddress1.Name = "txtBaptismResidenceAddress1";
            this.txtBaptismResidenceAddress1.Size = new System.Drawing.Size(418, 20);
            this.txtBaptismResidenceAddress1.TabIndex = 18;
            this.txtBaptismResidenceAddress1.Leave += new System.EventHandler(this.txtBaptismResidenceAddress1_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 30);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(48, 13);
            this.label32.TabIndex = 8;
            this.label32.Text = "Address:";
            // 
            // comboBaptismMinister
            // 
            this.comboBaptismMinister.DisplayMember = "Name";
            this.comboBaptismMinister.FormattingEnabled = true;
            this.comboBaptismMinister.Location = new System.Drawing.Point(704, 39);
            this.comboBaptismMinister.Name = "comboBaptismMinister";
            this.comboBaptismMinister.Size = new System.Drawing.Size(244, 21);
            this.comboBaptismMinister.TabIndex = 6;
            this.comboBaptismMinister.ValueMember = "Value";
            this.comboBaptismMinister.Leave += new System.EventHandler(this.comboBaptismMinister_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(535, 44);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "Minister:";
            // 
            // txtBaptismPageNumber
            // 
            this.txtBaptismPageNumber.Location = new System.Drawing.Point(704, 96);
            this.txtBaptismPageNumber.Name = "txtBaptismPageNumber";
            this.txtBaptismPageNumber.Size = new System.Drawing.Size(244, 20);
            this.txtBaptismPageNumber.TabIndex = 9;
            this.txtBaptismPageNumber.Leave += new System.EventHandler(this.txtBaptismPageNumber_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(535, 99);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(75, 13);
            this.label20.TabIndex = 40;
            this.label20.Text = "Page Number:";
            // 
            // txtBaptismBookNumber
            // 
            this.txtBaptismBookNumber.Location = new System.Drawing.Point(704, 67);
            this.txtBaptismBookNumber.Name = "txtBaptismBookNumber";
            this.txtBaptismBookNumber.Size = new System.Drawing.Size(244, 20);
            this.txtBaptismBookNumber.TabIndex = 8;
            this.txtBaptismBookNumber.Leave += new System.EventHandler(this.txtBaptismBookNumber_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(534, 70);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(165, 13);
            this.label19.TabIndex = 39;
            this.label19.Text = "Baptismal Register Book Number:";
            // 
            // formBaptism
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1061, 491);
            this.ControlBox = false;
            this.Controls.Add(this.txtBaptismPageNumber);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.txtBaptismBookNumber);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.comboBaptismMinister);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.btnBaptismCancel);
            this.Controls.Add(this.btnBaptismReset);
            this.Controls.Add(this.btnBaptismSave);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "formBaptism";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Baptism";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formConfirmation_FormClosing);
            this.Load += new System.EventHandler(this.formBaptism_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateBaptismDateBaptised;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBaptismLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBaptismMiddleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBaptismFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtBaptismMLastName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBaptismMMiddleName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBaptismMFirstName;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtBaptismFLastName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBaptismFMiddleName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBaptismFFirstName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtBaptismS2LastName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtBaptismS2MiddleName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtBaptismS2FirstName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtBaptismS1LastName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtBaptismS1MiddleName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtBaptismS1FirstName;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnBaptismSave;
        private System.Windows.Forms.Button btnBaptismReset;
        private System.Windows.Forms.Button btnBaptismCancel;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DateTimePicker dateBaptismBirthDate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBaptismBirthCity;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBaptismBirthProvince;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBaptismBirthCountry;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBaptismBirthAddress2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtBaptismBirthAddress1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox comboBaptismResidenceCity;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboBaptismResidenceProvince;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboBaptismResidenceCountry;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtBaptismResidenceAddress2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtBaptismResidenceAddress1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox comboBaptismMinister;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtBaptismPageNumber;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtBaptismBookNumber;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox comboBaptismGender;
        private System.Windows.Forms.Label label27;
    }
}