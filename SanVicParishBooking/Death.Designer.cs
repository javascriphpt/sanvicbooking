﻿namespace SanVicParishBooking
{
    partial class formDeath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numDeathAge = new System.Windows.Forms.NumericUpDown();
            this.dateDeathBirth = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboDeathGender = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtDeathLastName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDeathMiddleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDeathFirstName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDeathSave = new System.Windows.Forms.Button();
            this.btnDeathCancel = new System.Windows.Forms.Button();
            this.btnDeathReset = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.comboDeathResidenceCity = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.comboDeathResidenceProvince = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.comboDeathResidenceCountry = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtDeathResidenceAddress2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtDeathResidenceAddress1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.timeDeath = new System.Windows.Forms.DateTimePicker();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboDeathBurialCityState = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboDeathBurialProvince = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.comboDeathBurialCountry = new System.Windows.Forms.ComboBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtDeathBurialAddress2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtDeathBurialAddress1 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.comboDeathPlaceCityState = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboDeathPlaceProvince = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.comboDeathPlaceCountry = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDeathPlaceAddress2 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDeathPlaceAddress1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.dateTimeDeathBurial = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dateDeath = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtDeathMLastName = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDeathMMiddleName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDeathMFirstName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtDeathFLastName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDeathFMiddleName = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDeathFFirstName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtDeathSpouseLastName = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtDeathSpouseMiddleName = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDeathSpouseFirstName = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.numDeathPageNo = new System.Windows.Forms.NumericUpDown();
            this.numDeathRegisterNumber = new System.Windows.Forms.NumericUpDown();
            this.numDeathYear = new System.Windows.Forms.NumericUpDown();
            this.numDeathVolume = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathAge)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathPageNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathRegisterNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numDeathAge);
            this.groupBox1.Controls.Add(this.dateDeathBirth);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboDeathGender);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtDeathLastName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDeathMiddleName);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtDeathFirstName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(515, 198);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Deceased";
            // 
            // numDeathAge
            // 
            this.numDeathAge.Location = new System.Drawing.Point(94, 136);
            this.numDeathAge.Name = "numDeathAge";
            this.numDeathAge.Size = new System.Drawing.Size(400, 20);
            this.numDeathAge.TabIndex = 6;
            this.numDeathAge.Leave += new System.EventHandler(this.numDeathAge_Leave);
            // 
            // dateDeathBirth
            // 
            this.dateDeathBirth.Location = new System.Drawing.Point(94, 162);
            this.dateDeathBirth.Name = "dateDeathBirth";
            this.dateDeathBirth.Size = new System.Drawing.Size(400, 20);
            this.dateDeathBirth.TabIndex = 7;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(14, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Birth:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Age:";
            // 
            // comboDeathGender
            // 
            this.comboDeathGender.DisplayMember = "Name";
            this.comboDeathGender.FormattingEnabled = true;
            this.comboDeathGender.Location = new System.Drawing.Point(94, 109);
            this.comboDeathGender.Name = "comboDeathGender";
            this.comboDeathGender.Size = new System.Drawing.Size(400, 21);
            this.comboDeathGender.TabIndex = 5;
            this.comboDeathGender.ValueMember = "Value";
            this.comboDeathGender.Leave += new System.EventHandler(this.comboDeathGender_Leave);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(14, 113);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(45, 13);
            this.label27.TabIndex = 37;
            this.label27.Text = "Gender:";
            // 
            // txtDeathLastName
            // 
            this.txtDeathLastName.Location = new System.Drawing.Point(94, 82);
            this.txtDeathLastName.Name = "txtDeathLastName";
            this.txtDeathLastName.Size = new System.Drawing.Size(400, 20);
            this.txtDeathLastName.TabIndex = 4;
            this.txtDeathLastName.Leave += new System.EventHandler(this.txtDeathLastName_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Last Name:";
            // 
            // txtDeathMiddleName
            // 
            this.txtDeathMiddleName.Location = new System.Drawing.Point(94, 55);
            this.txtDeathMiddleName.Name = "txtDeathMiddleName";
            this.txtDeathMiddleName.Size = new System.Drawing.Size(400, 20);
            this.txtDeathMiddleName.TabIndex = 3;
            this.txtDeathMiddleName.Leave += new System.EventHandler(this.txtDeathMiddleName_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Middle Name:";
            // 
            // txtDeathFirstName
            // 
            this.txtDeathFirstName.Location = new System.Drawing.Point(94, 29);
            this.txtDeathFirstName.Name = "txtDeathFirstName";
            this.txtDeathFirstName.Size = new System.Drawing.Size(400, 20);
            this.txtDeathFirstName.TabIndex = 2;
            this.txtDeathFirstName.Leave += new System.EventHandler(this.txtDeathFirstName_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "First Name:";
            // 
            // btnDeathSave
            // 
            this.btnDeathSave.Location = new System.Drawing.Point(816, 537);
            this.btnDeathSave.Name = "btnDeathSave";
            this.btnDeathSave.Size = new System.Drawing.Size(75, 33);
            this.btnDeathSave.TabIndex = 47;
            this.btnDeathSave.Text = "Save";
            this.btnDeathSave.UseVisualStyleBackColor = true;
            this.btnDeathSave.Click += new System.EventHandler(this.btnConfSave_Click);
            // 
            // btnDeathCancel
            // 
            this.btnDeathCancel.Location = new System.Drawing.Point(980, 537);
            this.btnDeathCancel.Name = "btnDeathCancel";
            this.btnDeathCancel.Size = new System.Drawing.Size(75, 33);
            this.btnDeathCancel.TabIndex = 49;
            this.btnDeathCancel.Text = "Cancel";
            this.btnDeathCancel.UseVisualStyleBackColor = true;
            this.btnDeathCancel.Click += new System.EventHandler(this.btnConfCancel_Click);
            // 
            // btnDeathReset
            // 
            this.btnDeathReset.Location = new System.Drawing.Point(897, 537);
            this.btnDeathReset.Name = "btnDeathReset";
            this.btnDeathReset.Size = new System.Drawing.Size(75, 33);
            this.btnDeathReset.TabIndex = 48;
            this.btnDeathReset.Text = "Reset";
            this.btnDeathReset.UseVisualStyleBackColor = true;
            this.btnDeathReset.Click += new System.EventHandler(this.btnDeathReset_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(536, 100);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 67;
            this.label20.Text = "Year:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(535, 71);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 13);
            this.label19.TabIndex = 66;
            this.label19.Text = "Register Number:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(535, 43);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 69;
            this.label5.Text = "Page No:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(536, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 13);
            this.label6.TabIndex = 71;
            this.label6.Text = "Volume:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.comboDeathResidenceCity);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.comboDeathResidenceProvince);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.comboDeathResidenceCountry);
            this.groupBox7.Controls.Add(this.label30);
            this.groupBox7.Controls.Add(this.txtDeathResidenceAddress2);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.txtDeathResidenceAddress1);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Location = new System.Drawing.Point(538, 153);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(516, 115);
            this.groupBox7.TabIndex = 28;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Residence";
            // 
            // comboDeathResidenceCity
            // 
            this.comboDeathResidenceCity.DisplayMember = "Name";
            this.comboDeathResidenceCity.FormattingEnabled = true;
            this.comboDeathResidenceCity.Location = new System.Drawing.Point(420, 81);
            this.comboDeathResidenceCity.Name = "comboDeathResidenceCity";
            this.comboDeathResidenceCity.Size = new System.Drawing.Size(76, 21);
            this.comboDeathResidenceCity.TabIndex = 33;
            this.comboDeathResidenceCity.ValueMember = "CityId";
            this.comboDeathResidenceCity.Leave += new System.EventHandler(this.comboDeathResidenceCity_Leave);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(327, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 13);
            this.label28.TabIndex = 21;
            this.label28.Text = "City/Municipality:";
            // 
            // comboDeathResidenceProvince
            // 
            this.comboDeathResidenceProvince.DisplayMember = "Name";
            this.comboDeathResidenceProvince.FormattingEnabled = true;
            this.comboDeathResidenceProvince.Location = new System.Drawing.Point(238, 81);
            this.comboDeathResidenceProvince.Name = "comboDeathResidenceProvince";
            this.comboDeathResidenceProvince.Size = new System.Drawing.Size(71, 21);
            this.comboDeathResidenceProvince.TabIndex = 32;
            this.comboDeathResidenceProvince.ValueMember = "ProvinceId";
            this.comboDeathResidenceProvince.SelectedIndexChanged += new System.EventHandler(this.comboDeathResidenceProvince_SelectedIndexChanged);
            this.comboDeathResidenceProvince.Leave += new System.EventHandler(this.comboDeathResidenceProvince_Leave);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(180, 85);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(52, 13);
            this.label29.TabIndex = 19;
            this.label29.Text = "Province:";
            // 
            // comboDeathResidenceCountry
            // 
            this.comboDeathResidenceCountry.DisplayMember = "Name";
            this.comboDeathResidenceCountry.FormattingEnabled = true;
            this.comboDeathResidenceCountry.Location = new System.Drawing.Point(78, 81);
            this.comboDeathResidenceCountry.Name = "comboDeathResidenceCountry";
            this.comboDeathResidenceCountry.Size = new System.Drawing.Size(80, 21);
            this.comboDeathResidenceCountry.TabIndex = 31;
            this.comboDeathResidenceCountry.ValueMember = "CountryId";
            this.comboDeathResidenceCountry.SelectedIndexChanged += new System.EventHandler(this.comboDeathResidenceCountry_SelectedIndexChanged);
            this.comboDeathResidenceCountry.Leave += new System.EventHandler(this.comboDeathResidenceCountry_Leave);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 85);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 13);
            this.label30.TabIndex = 12;
            this.label30.Text = "Country:";
            // 
            // txtDeathResidenceAddress2
            // 
            this.txtDeathResidenceAddress2.Location = new System.Drawing.Point(78, 55);
            this.txtDeathResidenceAddress2.Name = "txtDeathResidenceAddress2";
            this.txtDeathResidenceAddress2.Size = new System.Drawing.Size(418, 20);
            this.txtDeathResidenceAddress2.TabIndex = 30;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 58);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(57, 13);
            this.label31.TabIndex = 10;
            this.label31.Text = "Address 2:";
            // 
            // txtDeathResidenceAddress1
            // 
            this.txtDeathResidenceAddress1.Location = new System.Drawing.Point(78, 29);
            this.txtDeathResidenceAddress1.Name = "txtDeathResidenceAddress1";
            this.txtDeathResidenceAddress1.Size = new System.Drawing.Size(418, 20);
            this.txtDeathResidenceAddress1.TabIndex = 29;
            this.txtDeathResidenceAddress1.Leave += new System.EventHandler(this.txtDeathResidenceAddress1_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 32);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(48, 13);
            this.label32.TabIndex = 8;
            this.label32.Text = "Address:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.timeDeath);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.groupBox9);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Controls.Add(this.dateTimeDeathBurial);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.dateDeath);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(13, 217);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(516, 351);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Death Information";
            // 
            // timeDeath
            // 
            this.timeDeath.CustomFormat = "HH:mm:ss";
            this.timeDeath.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeDeath.Location = new System.Drawing.Point(96, 167);
            this.timeDeath.Name = "timeDeath";
            this.timeDeath.ShowUpDown = true;
            this.timeDeath.Size = new System.Drawing.Size(398, 20);
            this.timeDeath.TabIndex = 20;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(12, 170);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(58, 13);
            this.label37.TabIndex = 51;
            this.label37.Text = "Time Died:";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.comboDeathBurialCityState);
            this.groupBox9.Controls.Add(this.label9);
            this.groupBox9.Controls.Add(this.comboDeathBurialProvince);
            this.groupBox9.Controls.Add(this.label33);
            this.groupBox9.Controls.Add(this.comboDeathBurialCountry);
            this.groupBox9.Controls.Add(this.label34);
            this.groupBox9.Controls.Add(this.txtDeathBurialAddress2);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.txtDeathBurialAddress1);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Location = new System.Drawing.Point(8, 192);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(502, 115);
            this.groupBox9.TabIndex = 21;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Place of Burial";
            // 
            // comboDeathBurialCityState
            // 
            this.comboDeathBurialCityState.DisplayMember = "Name";
            this.comboDeathBurialCityState.FormattingEnabled = true;
            this.comboDeathBurialCityState.Location = new System.Drawing.Point(420, 81);
            this.comboDeathBurialCityState.Name = "comboDeathBurialCityState";
            this.comboDeathBurialCityState.Size = new System.Drawing.Size(66, 21);
            this.comboDeathBurialCityState.TabIndex = 26;
            this.comboDeathBurialCityState.ValueMember = "CityId";
            this.comboDeathBurialCityState.Leave += new System.EventHandler(this.comboDeathBurialCityState_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(327, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(87, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "City/Municipality:";
            // 
            // comboDeathBurialProvince
            // 
            this.comboDeathBurialProvince.DisplayMember = "Name";
            this.comboDeathBurialProvince.FormattingEnabled = true;
            this.comboDeathBurialProvince.Location = new System.Drawing.Point(238, 81);
            this.comboDeathBurialProvince.Name = "comboDeathBurialProvince";
            this.comboDeathBurialProvince.Size = new System.Drawing.Size(71, 21);
            this.comboDeathBurialProvince.TabIndex = 25;
            this.comboDeathBurialProvince.ValueMember = "ProvinceId";
            this.comboDeathBurialProvince.SelectedIndexChanged += new System.EventHandler(this.comboDeathBurialProvince_SelectedIndexChanged);
            this.comboDeathBurialProvince.Leave += new System.EventHandler(this.comboDeathBurialProvince_Leave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(180, 85);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(52, 13);
            this.label33.TabIndex = 19;
            this.label33.Text = "Province:";
            // 
            // comboDeathBurialCountry
            // 
            this.comboDeathBurialCountry.DisplayMember = "Name";
            this.comboDeathBurialCountry.FormattingEnabled = true;
            this.comboDeathBurialCountry.Location = new System.Drawing.Point(78, 81);
            this.comboDeathBurialCountry.Name = "comboDeathBurialCountry";
            this.comboDeathBurialCountry.Size = new System.Drawing.Size(80, 21);
            this.comboDeathBurialCountry.TabIndex = 24;
            this.comboDeathBurialCountry.ValueMember = "CountryId";
            this.comboDeathBurialCountry.SelectedIndexChanged += new System.EventHandler(this.comboDeathBurialCountry_SelectedIndexChanged);
            this.comboDeathBurialCountry.Leave += new System.EventHandler(this.comboDeathBurialCountry_Leave);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(12, 85);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(46, 13);
            this.label34.TabIndex = 12;
            this.label34.Text = "Country:";
            // 
            // txtDeathBurialAddress2
            // 
            this.txtDeathBurialAddress2.Location = new System.Drawing.Point(78, 55);
            this.txtDeathBurialAddress2.Name = "txtDeathBurialAddress2";
            this.txtDeathBurialAddress2.Size = new System.Drawing.Size(408, 20);
            this.txtDeathBurialAddress2.TabIndex = 23;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(12, 58);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(57, 13);
            this.label35.TabIndex = 10;
            this.label35.Text = "Address 2:";
            // 
            // txtDeathBurialAddress1
            // 
            this.txtDeathBurialAddress1.Location = new System.Drawing.Point(78, 29);
            this.txtDeathBurialAddress1.Name = "txtDeathBurialAddress1";
            this.txtDeathBurialAddress1.Size = new System.Drawing.Size(408, 20);
            this.txtDeathBurialAddress1.TabIndex = 22;
            this.txtDeathBurialAddress1.Leave += new System.EventHandler(this.txtDeathBurialAddress1_Leave);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(12, 32);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(48, 13);
            this.label36.TabIndex = 8;
            this.label36.Text = "Address:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.comboDeathPlaceCityState);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.comboDeathPlaceProvince);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.comboDeathPlaceCountry);
            this.groupBox8.Controls.Add(this.label21);
            this.groupBox8.Controls.Add(this.txtDeathPlaceAddress2);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.txtDeathPlaceAddress1);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Location = new System.Drawing.Point(8, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(502, 115);
            this.groupBox8.TabIndex = 13;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Place of Death";
            // 
            // comboDeathPlaceCityState
            // 
            this.comboDeathPlaceCityState.DisplayMember = "Name";
            this.comboDeathPlaceCityState.FormattingEnabled = true;
            this.comboDeathPlaceCityState.Location = new System.Drawing.Point(420, 81);
            this.comboDeathPlaceCityState.Name = "comboDeathPlaceCityState";
            this.comboDeathPlaceCityState.Size = new System.Drawing.Size(66, 21);
            this.comboDeathPlaceCityState.TabIndex = 18;
            this.comboDeathPlaceCityState.ValueMember = "CityId";
            this.comboDeathPlaceCityState.Leave += new System.EventHandler(this.comboDeathPlaceCityState_Leave);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(327, 85);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "City/Municipality:";
            // 
            // comboDeathPlaceProvince
            // 
            this.comboDeathPlaceProvince.DisplayMember = "Name";
            this.comboDeathPlaceProvince.FormattingEnabled = true;
            this.comboDeathPlaceProvince.Location = new System.Drawing.Point(238, 81);
            this.comboDeathPlaceProvince.Name = "comboDeathPlaceProvince";
            this.comboDeathPlaceProvince.Size = new System.Drawing.Size(71, 21);
            this.comboDeathPlaceProvince.TabIndex = 17;
            this.comboDeathPlaceProvince.ValueMember = "ProvinceId";
            this.comboDeathPlaceProvince.SelectedIndexChanged += new System.EventHandler(this.comboDeathPlaceProvince_SelectedIndexChanged);
            this.comboDeathPlaceProvince.Leave += new System.EventHandler(this.comboDeathPlaceProvince_Leave);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(180, 85);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "Province:";
            // 
            // comboDeathPlaceCountry
            // 
            this.comboDeathPlaceCountry.DisplayMember = "Name";
            this.comboDeathPlaceCountry.FormattingEnabled = true;
            this.comboDeathPlaceCountry.Location = new System.Drawing.Point(78, 81);
            this.comboDeathPlaceCountry.Name = "comboDeathPlaceCountry";
            this.comboDeathPlaceCountry.Size = new System.Drawing.Size(80, 21);
            this.comboDeathPlaceCountry.TabIndex = 16;
            this.comboDeathPlaceCountry.ValueMember = "CountryId";
            this.comboDeathPlaceCountry.SelectedIndexChanged += new System.EventHandler(this.comboDeathPlaceCountry_SelectedIndexChanged);
            this.comboDeathPlaceCountry.Leave += new System.EventHandler(this.comboDeathPlaceCountry_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(12, 85);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(46, 13);
            this.label21.TabIndex = 12;
            this.label21.Text = "Country:";
            // 
            // txtDeathPlaceAddress2
            // 
            this.txtDeathPlaceAddress2.Location = new System.Drawing.Point(78, 55);
            this.txtDeathPlaceAddress2.Name = "txtDeathPlaceAddress2";
            this.txtDeathPlaceAddress2.Size = new System.Drawing.Size(408, 20);
            this.txtDeathPlaceAddress2.TabIndex = 15;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(57, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Address 2:";
            // 
            // txtDeathPlaceAddress1
            // 
            this.txtDeathPlaceAddress1.Location = new System.Drawing.Point(78, 29);
            this.txtDeathPlaceAddress1.Name = "txtDeathPlaceAddress1";
            this.txtDeathPlaceAddress1.Size = new System.Drawing.Size(408, 20);
            this.txtDeathPlaceAddress1.TabIndex = 14;
            this.txtDeathPlaceAddress1.Leave += new System.EventHandler(this.txtDeathPlaceAddress1_Leave);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 32);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(48, 13);
            this.label26.TabIndex = 8;
            this.label26.Text = "Address:";
            // 
            // dateTimeDeathBurial
            // 
            this.dateTimeDeathBurial.Location = new System.Drawing.Point(96, 318);
            this.dateTimeDeathBurial.Name = "dateTimeDeathBurial";
            this.dateTimeDeathBurial.Size = new System.Drawing.Size(398, 20);
            this.dateTimeDeathBurial.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 321);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(62, 13);
            this.label10.TabIndex = 47;
            this.label10.Text = "Burial Date:";
            // 
            // dateDeath
            // 
            this.dateDeath.Location = new System.Drawing.Point(96, 141);
            this.dateDeath.Name = "dateDeath";
            this.dateDeath.Size = new System.Drawing.Size(398, 20);
            this.dateDeath.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 144);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Date Died:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Location = new System.Drawing.Point(539, 386);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(516, 144);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Parents";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtDeathMLastName);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txtDeathMMiddleName);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.txtDeathMFirstName);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(262, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 106);
            this.groupBox4.TabIndex = 43;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Mother";
            // 
            // txtDeathMLastName
            // 
            this.txtDeathMLastName.Location = new System.Drawing.Point(84, 73);
            this.txtDeathMLastName.Name = "txtDeathMLastName";
            this.txtDeathMLastName.Size = new System.Drawing.Size(135, 20);
            this.txtDeathMLastName.TabIndex = 46;
            this.txtDeathMLastName.Leave += new System.EventHandler(this.txtDeathMLastName_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 76);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Last Name:";
            // 
            // txtDeathMMiddleName
            // 
            this.txtDeathMMiddleName.Location = new System.Drawing.Point(84, 47);
            this.txtDeathMMiddleName.Name = "txtDeathMMiddleName";
            this.txtDeathMMiddleName.Size = new System.Drawing.Size(135, 20);
            this.txtDeathMMiddleName.TabIndex = 45;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(8, 50);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Middle Name:";
            // 
            // txtDeathMFirstName
            // 
            this.txtDeathMFirstName.Location = new System.Drawing.Point(84, 19);
            this.txtDeathMFirstName.Name = "txtDeathMFirstName";
            this.txtDeathMFirstName.Size = new System.Drawing.Size(135, 20);
            this.txtDeathMFirstName.TabIndex = 44;
            this.txtDeathMFirstName.Leave += new System.EventHandler(this.txtDeathMFirstName_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(8, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 14;
            this.label14.Text = "First Name:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtDeathFLastName);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.txtDeathFMiddleName);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Controls.Add(this.txtDeathFFirstName);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Location = new System.Drawing.Point(12, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(240, 106);
            this.groupBox5.TabIndex = 39;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Father";
            // 
            // txtDeathFLastName
            // 
            this.txtDeathFLastName.Location = new System.Drawing.Point(84, 73);
            this.txtDeathFLastName.Name = "txtDeathFLastName";
            this.txtDeathFLastName.Size = new System.Drawing.Size(136, 20);
            this.txtDeathFLastName.TabIndex = 42;
            this.txtDeathFLastName.Leave += new System.EventHandler(this.txtDeathFLastName_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(61, 13);
            this.label15.TabIndex = 18;
            this.label15.Text = "Last Name:";
            // 
            // txtDeathFMiddleName
            // 
            this.txtDeathFMiddleName.Location = new System.Drawing.Point(84, 47);
            this.txtDeathFMiddleName.Name = "txtDeathFMiddleName";
            this.txtDeathFMiddleName.Size = new System.Drawing.Size(136, 20);
            this.txtDeathFMiddleName.TabIndex = 41;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 50);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(72, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "Middle Name:";
            // 
            // txtDeathFFirstName
            // 
            this.txtDeathFFirstName.Location = new System.Drawing.Point(84, 19);
            this.txtDeathFFirstName.Name = "txtDeathFFirstName";
            this.txtDeathFFirstName.Size = new System.Drawing.Size(136, 20);
            this.txtDeathFFirstName.TabIndex = 40;
            this.txtDeathFFirstName.Leave += new System.EventHandler(this.txtDeathFFirstName_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 22);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "First Name:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtDeathSpouseLastName);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.txtDeathSpouseMiddleName);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.txtDeathSpouseFirstName);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Location = new System.Drawing.Point(539, 274);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(516, 111);
            this.groupBox6.TabIndex = 34;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Spouse";
            // 
            // txtDeathSpouseLastName
            // 
            this.txtDeathSpouseLastName.Location = new System.Drawing.Point(89, 75);
            this.txtDeathSpouseLastName.Name = "txtDeathSpouseLastName";
            this.txtDeathSpouseLastName.Size = new System.Drawing.Size(408, 20);
            this.txtDeathSpouseLastName.TabIndex = 37;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 78);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(61, 13);
            this.label23.TabIndex = 30;
            this.label23.Text = "Last Name:";
            // 
            // txtDeathSpouseMiddleName
            // 
            this.txtDeathSpouseMiddleName.Location = new System.Drawing.Point(89, 49);
            this.txtDeathSpouseMiddleName.Name = "txtDeathSpouseMiddleName";
            this.txtDeathSpouseMiddleName.Size = new System.Drawing.Size(408, 20);
            this.txtDeathSpouseMiddleName.TabIndex = 36;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 52);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(72, 13);
            this.label24.TabIndex = 29;
            this.label24.Text = "Middle Name:";
            // 
            // txtDeathSpouseFirstName
            // 
            this.txtDeathSpouseFirstName.Location = new System.Drawing.Point(89, 21);
            this.txtDeathSpouseFirstName.Name = "txtDeathSpouseFirstName";
            this.txtDeathSpouseFirstName.Size = new System.Drawing.Size(408, 20);
            this.txtDeathSpouseFirstName.TabIndex = 35;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(13, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(60, 13);
            this.label25.TabIndex = 28;
            this.label25.Text = "First Name:";
            // 
            // numDeathPageNo
            // 
            this.numDeathPageNo.Location = new System.Drawing.Point(705, 38);
            this.numDeathPageNo.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numDeathPageNo.Name = "numDeathPageNo";
            this.numDeathPageNo.Size = new System.Drawing.Size(244, 20);
            this.numDeathPageNo.TabIndex = 8;
            this.numDeathPageNo.Leave += new System.EventHandler(this.numDeathPageNo_Leave);
            // 
            // numDeathRegisterNumber
            // 
            this.numDeathRegisterNumber.Location = new System.Drawing.Point(705, 67);
            this.numDeathRegisterNumber.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.numDeathRegisterNumber.Name = "numDeathRegisterNumber";
            this.numDeathRegisterNumber.Size = new System.Drawing.Size(244, 20);
            this.numDeathRegisterNumber.TabIndex = 9;
            this.numDeathRegisterNumber.Leave += new System.EventHandler(this.numDeathRegisterNumber_Leave);
            // 
            // numDeathYear
            // 
            this.numDeathYear.Location = new System.Drawing.Point(705, 95);
            this.numDeathYear.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.numDeathYear.Name = "numDeathYear";
            this.numDeathYear.Size = new System.Drawing.Size(244, 20);
            this.numDeathYear.TabIndex = 10;
            this.numDeathYear.Leave += new System.EventHandler(this.numDeathYear_Leave);
            // 
            // numDeathVolume
            // 
            this.numDeathVolume.Location = new System.Drawing.Point(705, 124);
            this.numDeathVolume.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.numDeathVolume.Name = "numDeathVolume";
            this.numDeathVolume.Size = new System.Drawing.Size(244, 20);
            this.numDeathVolume.TabIndex = 11;
            this.numDeathVolume.Leave += new System.EventHandler(this.numDeathVolume_Leave);
            // 
            // formDeath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1061, 581);
            this.ControlBox = false;
            this.Controls.Add(this.numDeathVolume);
            this.Controls.Add(this.numDeathYear);
            this.Controls.Add(this.numDeathRegisterNumber);
            this.Controls.Add(this.numDeathPageNo);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnDeathSave);
            this.Controls.Add(this.btnDeathCancel);
            this.Controls.Add(this.btnDeathReset);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Name = "formDeath";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Death";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formDeath_FormClosing);
            this.Load += new System.EventHandler(this.formDeath_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathAge)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathPageNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathRegisterNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDeathVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboDeathGender;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtDeathLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDeathMiddleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDeathFirstName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnDeathSave;
        private System.Windows.Forms.Button btnDeathCancel;
        private System.Windows.Forms.Button btnDeathReset;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateDeathBirth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.ComboBox comboDeathResidenceCity;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboDeathResidenceProvince;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox comboDeathResidenceCountry;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtDeathResidenceAddress2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtDeathResidenceAddress1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dateDeath;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dateTimeDeathBurial;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtDeathMLastName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDeathMMiddleName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDeathMFirstName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtDeathFLastName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDeathFFirstName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtDeathSpouseLastName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtDeathSpouseMiddleName;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtDeathSpouseFirstName;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ComboBox comboDeathPlaceCityState;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboDeathPlaceProvince;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboDeathPlaceCountry;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtDeathPlaceAddress2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDeathPlaceAddress1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboDeathBurialCityState;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboDeathBurialProvince;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.ComboBox comboDeathBurialCountry;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtDeathBurialAddress2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtDeathBurialAddress1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DateTimePicker timeDeath;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown numDeathAge;
        private System.Windows.Forms.NumericUpDown numDeathPageNo;
        private System.Windows.Forms.NumericUpDown numDeathRegisterNumber;
        private System.Windows.Forms.NumericUpDown numDeathYear;
        private System.Windows.Forms.NumericUpDown numDeathVolume;
        private System.Windows.Forms.TextBox txtDeathFMiddleName;

    }
}