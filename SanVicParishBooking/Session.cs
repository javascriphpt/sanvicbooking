﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SanVicParishBooking
{
    public sealed class Session
    {

        private SanVicParishDb _dbInstance = SanVicParishDb.instance;
        //private int _UserLoginId = 0;

        public static bool inSession = false;
        public static int _UserLoginId = 0;
        public static readonly Session instance = new Session();

        private string _username;
        private string _password;

        private Session() {}

        public static Session Instance
        {
	        get
	        {
                return instance;
	        }
        }

        public string Username {
            get {
                return _username;
            }

            set {
                _username = value;
            }
        }

        public string Password {
            get {
                return _password;
            }

            set {
                _password = value;
            }
        }

        public bool validateUser()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;
            bool found = false;

            if (this._username == "superadmin" && this._password == "superadmin")
            {
                return true;
            }

            try
            {
                conn = this._dbInstance.openConnection();
                
                cmd = new SqlCommand("SP_Login", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Username", this._username));
                cmd.Parameters.Add(new SqlParameter("@Password", this._password));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    if (Int32.Parse(rdr["UserLoginId"].ToString()) != 0)
                    {
                        Console.WriteLine(rdr["UserLoginId"]);
                        Console.WriteLine(rdr["UserId"]);

                        Session._UserLoginId = Int32.Parse(rdr["UserLoginId"].ToString());
                        found = true;
                        break;
                    }
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }
            }

            if (inSession)
            {
                inSession = true;
            }

            return found;
        }

        public bool logOut()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;
            bool found = false;

            try
            {
                conn = this._dbInstance.openConnection();

                cmd = new SqlCommand("SP_ChangeLoginStatus", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserLoginId", Session._UserLoginId));
                cmd.Parameters.Add(new SqlParameter("@UserLoginType", 201608072));
                cmd.Parameters.Add(new SqlParameter("@Return", 1));

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        if (Int32.Parse(rdr["UserLoginHistoryId"].ToString()) != 0)
                        {
                            Console.WriteLine(rdr["UserLoginHistoryId"]);
                            found = true;
                            break;
                        }
                    }
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }
            }

            return found;
        }
    }
}
