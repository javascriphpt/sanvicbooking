﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace SanVicParishBooking
{
    public partial class MainScreen : Form
    {
        private SanVicParishDb _dbInstance = SanVicParishDb.instance;
        private Session _sessInstance = Session.instance;

        public MainScreen()
        {
            InitializeComponent();
        }

        private void panelHome_Paint(object sender, PaintEventArgs e)
        {
            this.panelHome.Location = new System.Drawing.Point(
                this.ClientSize.Width / 2 - this.panelHome.Size.Width / 2,
                this.ClientSize.Height / 2 - this.panelHome.Size.Height / 2);
            this.panelHome.Anchor = System.Windows.Forms.AnchorStyles.None;
        }

        private void MainScreen_Close(object sender, FormClosingEventArgs e)
        {
            
            if (Session.inSession)
            {
                e.Cancel = true;
                //this.WindowState = FormWindowState.Minimized;
                this.Hide();
                this.notifyIcon1.Visible = true;
            }
            else
            {
                Console.WriteLine("Close main screen");
                Session.inSession = false;
                Application.Exit();
            }
            //this.Hide();
        }

        private void logutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Session.inSession && _sessInstance.logOut())
            {
                Console.WriteLine("Logout tool stript");
                Session.inSession = false;
                this.Hide();
                new LoginScreen().Show();
            }
            
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Session.inSession && _sessInstance.logOut())
            {
                Console.WriteLine("Exit tool strip main screen");
                Session.inSession = false;
                Application.Exit();
            }
        }

        private void btnAddRecord_Click(object sender, EventArgs e)
        {
            this.addRecord();
        }

        private void addRecordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.addRecord();
        }

        private void addRecord()
        {
            Console.WriteLine("add record");
        }

        private void btnRequestCert_Click(object sender, EventArgs e)
        {
            this.requestCert();
        }

        private void requestCertificateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.requestCert();
        }

        private void requestCert()
        {
            Console.WriteLine("request certificate");
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //Application.Exit();
            if (this.WindowState == FormWindowState.Minimized)
                this.WindowState = FormWindowState.Maximized;

            // Activate the form.
            this.Show();
            this.Activate();
        }

        private void certificatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.panelRecords.Show();
            //this.panelRequests.Hide();
        }

        private void requestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.panelRequests.Show();
            this.panelRecords.Hide();
        }

        private void btnBaptismAdd_Click(object sender, EventArgs e)
        {
            formBaptism fmBaptism = new formBaptism( this );
            fmBaptism.ShowDialog();
        }

        public void refreshDataGridBaptism()
        {
            Console.WriteLine("Paint here");
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter;
            DataTable dataTable;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("SELECT * FROM V_Baptism", conn);
                cmd.CommandType = CommandType.Text;
                sqlDataAdapter = new SqlDataAdapter(cmd);
                dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                this.dataGridBaptism.DataSource = dataTable;
                
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }
            }
        }

        public void refreshDataGridConfirmation()
        {
            Console.WriteLine("Paint here");
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter;
            DataTable dataTable;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("SELECT * FROM V_Confirmation", conn);
                cmd.CommandType = CommandType.Text;
                sqlDataAdapter = new SqlDataAdapter(cmd);
                dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                this.dataGridConfirmation.DataSource = dataTable;

            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }
            }
        }

        public void refreshDataGridDeath()
        {
            Console.WriteLine("Paint here");
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataAdapter sqlDataAdapter;
            DataTable dataTable;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("SELECT * FROM V_Death", conn);
                cmd.CommandType = CommandType.Text;
                sqlDataAdapter = new SqlDataAdapter(cmd);
                dataTable = new DataTable();

                sqlDataAdapter.Fill(dataTable);

                this.dataGridDeath.DataSource = dataTable;

            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine(((TabControl)sender).SelectedTab.Name);
            switch (((TabControl)sender).SelectedTab.Name )
            {
                case "tabBaptism" :
                    this.refreshDataGridBaptism();
                    break;
                case "tabConfirmation" :
                    this.refreshDataGridConfirmation();
                    break;
                case "tabDeath":
                    this.refreshDataGridDeath();
                    break;
            }
        }

        private void MainScreen_Load(object sender, EventArgs e)
        {
            this.dataGridBaptism.AutoGenerateColumns = false;
            this.dataGridConfirmation.AutoGenerateColumns = false;
            this.dataGridDeath.AutoGenerateColumns = false;
            this.refreshDataGridBaptism();
        }

        private void dataGridBaptism_SelectionChanged(object sender, EventArgs e)
        {
            if ( ((DataGridView)sender).SelectedRows.Count > 0 )
            {
                this.btnBaptismEdit.Enabled = true;
                this.btnBaptismDelete.Enabled = true;
            }
            else
            {
                this.btnBaptismEdit.Enabled = false;
                this.btnBaptismDelete.Enabled = false;
            }
        }

        private void btnBaptismDelete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show(
                "Are you sure you want to delete this record?",
                "Confirm Delete",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question
            );

            int baptismPersonid = (int)this.dataGridBaptism.SelectedRows[0].Cells["BaptismPersonId"].Value;

            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            bool success = false;

            if (confirmResult == DialogResult.Yes)
            {
                try
                {
                    conn = this._dbInstance.openConnection();

                    cmd = new SqlCommand("SP_DeleteBaptism", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BaptismPersonId", baptismPersonid));
                    cmd.Parameters.Add(new SqlParameter("@DeletedBy", Session._UserLoginId));

                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        if ((int)rdr["UpdatedRows"] > 0)
                        {
                            success = true;
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        this._dbInstance.closeConnection();
                        conn = null;
                    }

                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr = null;
                    }

                    if (success)
                    {
                        MessageBox.Show(
                            "Record successfully deleted!",
                            "Delete",
                            MessageBoxButtons.OK
                        );

                        this.refreshDataGridBaptism();
                    }
                }
            }
        }

        private void btnBaptismEdit_Click(object sender, EventArgs e)
        {
            int baptismPersonid = (int)this.dataGridBaptism.SelectedRows[0].Cells["BaptismPersonId"].Value;
            formBaptism fmBaptism = new formBaptism(this);
            fmBaptism.BaptismPersonId = baptismPersonid;
            fmBaptism.ShowDialog();
        }

        private void btnConfirmationAdd_Click(object sender, EventArgs e)
        {
            formConfirmation formConf = new formConfirmation( this );
            formConf.ShowDialog();
        }

        private void btnConfirmationDelete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show(
                "Are you sure you want to delete this record?",
                "Confirm Delete",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question
            );

            int confirmationPersonid = (int)this.dataGridConfirmation.SelectedRows[0].Cells["ConfirmationPersonId"].Value;

            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            bool success = false;

            if (confirmResult == DialogResult.Yes)
            {
                try
                {
                    conn = this._dbInstance.openConnection();

                    cmd = new SqlCommand("SP_DeleteConfirmation", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@ConfirmationPersonId", confirmationPersonid));
                    cmd.Parameters.Add(new SqlParameter("@DeletedBy", Session._UserLoginId));

                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        if ((int)rdr["UpdatedRows"] > 0)
                        {
                            success = true;
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        this._dbInstance.closeConnection();
                        conn = null;
                    }

                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr = null;
                    }

                    if (success)
                    {
                        MessageBox.Show(
                            "Record successfully deleted!",
                            "Delete",
                            MessageBoxButtons.OK
                        );

                        this.refreshDataGridConfirmation();
                    }
                }
            }
        }

        private void dataGridConfirmation_SelectionChanged(object sender, EventArgs e)
        {
            if (((DataGridView)sender).SelectedRows.Count > 0)
            {
                this.btnConfirmationEdit.Enabled = true;
                this.btnConfirmationDelete.Enabled = true;
            }
            else
            {
                this.btnConfirmationEdit.Enabled = false;
                this.btnConfirmationDelete.Enabled = false;
            }
        }

        private void btnConfirmationEdit_Click(object sender, EventArgs e)
        {
            int confirmationPersonid = (int)this.dataGridConfirmation.SelectedRows[0].Cells["ConfirmationPersonId"].Value;
            formConfirmation formConf = new formConfirmation(this);
            formConf.ConfirmationPersonId = confirmationPersonid;
            formConf.ShowDialog();
        }

        private void btnDeathAdd_Click(object sender, EventArgs e)
        {
            formDeath formDeath = new formDeath(this );
            formDeath.ShowDialog();
        }

        private void btbDeathEdit_Click(object sender, EventArgs e)
        {
            int deathPersonid = (int)this.dataGridDeath.SelectedRows[0].Cells["DeathPersonId"].Value;
            formDeath formDeath = new formDeath(this);
            formDeath.DeathPersonId = deathPersonid;
            formDeath.ShowDialog();
        }

        private void dataGridDeath_SelectionChanged(object sender, EventArgs e)
        {
            if (((DataGridView)sender).SelectedRows.Count > 0)
            {
                this.btnDeathEdit.Enabled = true;
                this.btnDeathDelete.Enabled = true;
            }
            else
            {
                this.btnDeathEdit.Enabled = false;
                this.btnDeathDelete.Enabled = false;
            }
        }

    }
}
