﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace SanVicParishBooking
{
    public partial class formConfirmation : Form
    {
        private SanVicParishDb _dbInstance = SanVicParishDb.instance;
        private List<Control> _required = new List<Control>();
        private FormValidation _formValidation;
        private MainScreen _mainScreen;

        private int _confirmationPersonId;
        private int _countryId;
        private int _proviceId;
        private int _cityId;

        private string _countryName;
        private string _provinceName;
        private string _cityName;

        public int ConfirmationPersonId
        {
            get
            {
                return _confirmationPersonId;
            }
            set
            {
                _confirmationPersonId = value;
            }
        }

        public formConfirmation()
        {
            InitializeComponent();
            this.initializeRequiredFields();
        }

        public formConfirmation( MainScreen mainScreen )
        {
            this._mainScreen = mainScreen;
            InitializeComponent();
            this.initializeRequiredFields();
        }

        public void initializeRequiredFields()
        {
            string[] requiredFields = { 
                "txtConfFirstName",
                "txtConfMiddleName",
                "txtConfLastName",
                "dateConfDateBaptised",
                "comboConfGender",
                "comboConfMinister",
                "txtConfBookNumber",
                "txtConfPageNumber",
                "txtConfBirthAddress1",
                "comboConfBirthCountry",
                "comboConfBirthProvince",
                "comboConfBirthCity",
                "txtConfFFirstName",
                "txtConfFLastName",
                "txtConfMFirstName",
                "txtConfMLastName"
            };

            this._formValidation = new FormValidation(this.Controls, requiredFields);
        }

        private void btnConfCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnConfReset_Click(object sender, EventArgs e)
        {
            this._resetFields();
        }

        public void _resetFields()
        {
            this.txtConfFirstName.Clear();
            this.txtConfMiddleName.Clear();
            this.txtConfLastName.Clear();
            this.dateConfDateBaptised.Text = "";
            this.comboConfGender.SelectedIndex = -1;

            this.comboConfMinister.SelectedIndex = -1;
            this.txtConfBookNumber.Clear();
            this.txtConfPageNumber.Clear();

            this.txtConfBirthAddress1.Clear();
            this.txtConfBirthAddress2.Clear();
            this.comboConfBirthCountry.SelectedIndex = -1;
            this.comboConfBirthProvince.SelectedIndex = -1;
            this.comboConfBirthCity.SelectedIndex = -1;

            this.txtConfFFirstName.Clear();
            this.txtConfFMiddleName.Clear();
            this.txtConfFLastName.Clear();
            this.txtConfMFirstName.Clear();
            this.txtConfMMiddleName.Clear();
            this.txtConfMLastName.Clear();

            this.txtConfS1FirstName.Clear();
            this.txtConfS1MiddleName.Clear();
            this.txtConfS1LastName.Clear();
            this.txtConfS2FirstName.Clear();
            this.txtConfS2MiddleName.Clear();
            this.txtConfS2LastName.Clear();
        }

        private void txtConfFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfMiddleName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void dateConfDateBaptised_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((DateTimePicker)sender).Text))
            {
                this._formValidation.HideError(((DateTimePicker)sender).Name);
            }
        }

        private void comboConfGender_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboConfMinister_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void txtConfBookNumber_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfPageNumber_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfBirthAddress1_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfBirthAddress2_Leave(object sender, EventArgs e)
        {

        }

        private void comboConfBirthCountry_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboConfBirthProvince_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboConfBirthCity_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void txtConfFFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfFMiddleName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfFLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfMFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfMMiddleName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfMLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtConfS1FirstName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfS1MiddleName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfS1LastName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfS2FirstName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfS2MiddleName_Leave(object sender, EventArgs e)
        {

        }

        private void txtConfS2LastName_Leave(object sender, EventArgs e)
        {

        }

        private void formConfirmation_Load(object sender, EventArgs e)
        {
            this._loadGender();
            this._loadMinister();

            if (this._confirmationPersonId > 0)
            {
                this._populateFields();
            }
            else
            {
                this._loadCountries();
            }
        }

        private void _populateFields()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("SELECT * FROM V_Confirmation WHERE ConfirmationPersonId = @ConfirmationPersonId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ConfirmationPersonId", this._confirmationPersonId));

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        this.txtConfFirstName.Text = (string) rdr["Firstname"];
                        this.txtConfMiddleName.Text = (string)rdr["MiddleName"];
                        this.txtConfLastName.Text = (string)rdr["LastName"];
                        this.dateConfDateBaptised.Text = (String)rdr["DateConfirmed"];
                        this.comboConfGender.SelectedIndex = this.comboConfGender.FindString((string)rdr["Gender"]);

                        this.comboConfMinister.Text = rdr["MinisterFirstName"].ToString() + " " + rdr["MinisterMiddleName"].ToString() + " " + rdr["MinisterLastName"].ToString();
                        this.txtConfBookNumber.Text = rdr["ConfirmationBookNumber"].ToString();
                        this.txtConfPageNumber.Text = rdr["ConfirmationPageNumber"].ToString();

                        this.txtConfBirthAddress1.Text = (string)rdr["ConfirmationAddress1"];
                        this.txtConfBirthAddress2.Text = (string)rdr["ConfirmationAddress2"];

                        this._countryId = (int)rdr["CCountryId"];
                        this._proviceId = (int)rdr["CProvinceId"];
                        this._cityId = (int)rdr["CCityStateId"];
                        this._countryName = (string)rdr["CCountry"];
                        this._provinceName = (string)rdr["CProvince"];
                        this._cityName = (string)rdr["CCityState"];

                        this.txtConfFFirstName.Text = (string)rdr["FatherFirstName"];
                        this.txtConfFMiddleName.Text = (string)rdr["FatherMiddleName"];
                        this.txtConfFLastName.Text = (string)rdr["FatherLastName"];
                        this.txtConfMFirstName.Text = (string)rdr["MotherFirstName"];
                        this.txtConfMMiddleName.Text = (string)rdr["MotherMiddleName"];
                        this.txtConfMLastName.Text = (string)rdr["MotherLastName"];

                        this.txtConfS1FirstName.Text = (string)rdr["Sponsor1Firstname"];
                        this.txtConfS1MiddleName.Text = (string)rdr["Sponsor1MiddleName"];
                        this.txtConfS1LastName.Text = (string)rdr["Sponsor1LastName"];
                        this.txtConfS2FirstName.Text = (string)rdr["Sponsor2Firstname"];
                        this.txtConfS2MiddleName.Text = (string)rdr["Sponsor2MiddleName"];
                        this.txtConfS2LastName.Text = (string)rdr["Sponsor2LastName"];

                        this.btnConfReset.Enabled = false;

                    }
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                this._loadCountries();
            }
        }

        public void _loadGender()
        {
            this.comboConfGender.Items.Add(new ComboBoxItem("Male", 1));
            this.comboConfGender.Items.Add(new ComboBoxItem("Female", 2));
        }

        public void _loadMinister()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            String ministerName = "";

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_Minister", conn);
                cmd.CommandType = CommandType.Text;

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ministerName = rdr["MinisterFirstName"].ToString() + " " + rdr["MinisterMiddleName"].ToString() + " " + rdr["MinisterLastName"].ToString();
                    this.comboConfMinister.Items.Add(new ComboBoxItem(ministerName, Int32.Parse(rdr["MinisterId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }
            }
        }

        public void _loadCountries()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM Country", conn);
                cmd.CommandType = CommandType.Text;

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboConfBirthCountry.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CountryId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._confirmationPersonId > 0)
                {
                    this.comboConfBirthCountry.Text = this._countryName;
                    this._loadProvince( this._countryId );
                }
            }
        }

        private void comboConfBirthCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int countryId;

            if (selectedItem != null)
            {
                countryId = (int)selectedItem.Value;

                this._loadProvince(countryId);

            }
        }

        public void _loadProvince(int countryId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboConfBirthProvince.SelectedIndex = -1;
            this.comboConfBirthProvince.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_ProvinceCountry Where CountryId = @CountryId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@CountryId", countryId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboConfBirthProvince.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["ProvinceId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._confirmationPersonId > 0)
                {
                    this.comboConfBirthProvince.Text = this._provinceName;
                    this._loadCity( this._proviceId );
                }
            }
        }

        private void comboConfBirthProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int provinceId;

            if (selectedItem != null)
            {
                provinceId = (int)selectedItem.Value;

                this._loadCity(provinceId);

            }
        }

        public void _loadCity(int provinceId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboConfBirthCity.SelectedIndex = -1;
            this.comboConfBirthCity.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_CityProvince Where ProvinceId = @ProvinceId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ProvinceId", provinceId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboConfBirthCity.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CityId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._confirmationPersonId > 0)
                {
                    this.comboConfBirthCity.Text = this._cityName;
                }
            }
        }

        private void btnConfSave_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            string gender;
            bool success = false;

            if (this._formValidation.Validate())
            {
                try
                {
                    conn = this._dbInstance.openConnection();

                    if (this._confirmationPersonId > 0)
                    {
                        cmd = new SqlCommand("SP_EditConfirmation", conn);
                    }
                    else
                    {
                        cmd = new SqlCommand("SP_NewConfirmation", conn);
                    }
                    
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Firstname", this.txtConfFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MiddleName", this.txtConfMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@LastName", this.txtConfLastName.Text));

                    if ((int)((ComboBoxItem)this.comboConfGender.SelectedItem).Value == 1)
                    {
                        gender = "Male";
                    }
                    else
                    {
                        gender = "Female";
                    }
                    cmd.Parameters.Add(new SqlParameter("@Gender", gender));

                    cmd.Parameters.Add(new SqlParameter("@MotherFirstName", this.txtConfMFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MotherMiddleName", this.txtConfMMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MotherLastName", this.txtConfMLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherFirstName", this.txtConfFFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherMiddleName", this.txtConfFMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherLastName", this.txtConfFLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@ConfAddress1", this.txtConfBirthAddress1.Text));
                    cmd.Parameters.Add(new SqlParameter("@ConfAddress2", this.txtConfBirthAddress2.Text));
                    cmd.Parameters.Add(new SqlParameter("@ConfCityState", (int)((ComboBoxItem)this.comboConfBirthCity.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@ConfProvince", (int)((ComboBoxItem)this.comboConfBirthProvince.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@ConfCountry", (int)((ComboBoxItem)this.comboConfBirthCountry.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@DateConfirmed", this.dateConfDateBaptised.Value.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@MinisterId", (int)((ComboBoxItem)this.comboConfMinister.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor1Firstname", this.txtConfS1FirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor1MiddleName", this.txtConfS1MiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor1LastName", this.txtConfS1LastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor2Firstname", this.txtConfS2FirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor2MiddleName", this.txtConfS2MiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Sponsor2LastName", this.txtConfS2LastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@ConfirmationBookNumber", this.txtConfBookNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@ConfirmationPageNumber", this.txtConfPageNumber.Text));

                    if (this._confirmationPersonId > 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", Session._UserLoginId));
                        cmd.Parameters.Add(new SqlParameter("@ConfirmationPersonId", this._confirmationPersonId));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@AddedBy", Session._UserLoginId));
                    }

                    

                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        if ((int)rdr["ConfirmationPersonId"] > 0)
                        {
                            success = true;
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        this._dbInstance.closeConnection();
                        conn = null;
                    }

                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr = null;
                    }

                    if (success)
                    {
                        MessageBox.Show(
                            "Successfully added/updated record",
                            "Success",
                            MessageBoxButtons.OK
                        );

                        this.Hide();
                    }
                }
            }
        }

        private void formConfirmation_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._mainScreen.refreshDataGridConfirmation();
        }
    }
}
