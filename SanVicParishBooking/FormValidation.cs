﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SanVicParishBooking
{
    class FormValidation
    {
        private Hashtable _formHashTable;
        private Control.ControlCollection _controls;

        public FormValidation(Control.ControlCollection controls, string[] forms)
        {

            this._formHashTable = new Hashtable();
            this._controls = controls;
            string controlType;

            foreach (string field in forms)
            {
                _formHashTable.Add(field, new ErrorProvider());
                Console.WriteLine(controls.Find(field, true).First().GetType());
            }
        }

        public bool Validate()
        {
            object field;
            bool retVal = false;
            string controlType;
            int errorCount = 0;

            ErrorProvider errorProvider;

            foreach (DictionaryEntry fields in this._formHashTable )
            {
                controlType = this._controls.Find(fields.Key.ToString(), true).First().GetType().ToString();
                errorProvider = (ErrorProvider)fields.Value;

                errorProvider.BlinkRate = 0;

                switch (controlType)
                {
                    case "System.Windows.Forms.TextBox":
                        //String.IsNullOrEmpty(((TextBox)sender).Text)
                        field = (TextBox)this._controls.Find(fields.Key.ToString(), true).First();

                        if (String.IsNullOrEmpty(((TextBox)field).Text) )
                        {
                            errorProvider.SetError((TextBox)field, "Cannot be empty");
                            errorCount++;
                        }
                        else
                        {
                            errorProvider.Clear();
                        }
                        break;
                    case "System.Windows.Forms.DateTimePicker" :
                        field = (DateTimePicker)this._controls.Find(fields.Key.ToString(), true).First();

                        if (String.IsNullOrEmpty(((DateTimePicker)field).Text))
                        {
                            errorProvider.SetError((DateTimePicker)field, "Cannot be empty");
                            errorCount++;
                        }
                        else
                        {
                            errorProvider.Clear();
                        }
                        break;
                    case "System.Windows.Forms.ComboBox":
                        field = (ComboBox)this._controls.Find(fields.Key.ToString(), true).First();

                        if (String.IsNullOrEmpty(((ComboBox)field).Text))
                        {
                            errorProvider.SetError((ComboBox)field, "Cannot be empty");
                            errorCount++;
                        }
                        else
                        {
                            errorProvider.Clear();
                        }
                        break;
                    case "System.Windows.Forms.NumericUpDown":
                        field = (NumericUpDown)this._controls.Find(fields.Key.ToString(), true).First();

                        if (String.IsNullOrEmpty(((NumericUpDown)field).Text))
                        {
                            errorProvider.SetError((NumericUpDown)field, "Cannot be empty");
                            errorCount++;
                        }
                        else
                        {
                            errorProvider.Clear();
                        }
                        break;
                }
            }

            if (errorCount > 0)
            {
                retVal = false;
            }
            else
            {
                retVal = true;
            }

            return retVal;
        }

        public void HideError( string fieldName )
        {
            ErrorProvider errorProvider = (ErrorProvider)this._formHashTable[fieldName];
            errorProvider.Clear();
        }
    }
}
