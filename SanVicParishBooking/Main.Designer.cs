﻿using System.Drawing;
namespace SanVicParishBooking
{
    partial class MainScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainScreen));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.certificatesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.requestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelHome = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabBaptism = new System.Windows.Forms.TabPage();
            this.dataGridBaptism = new System.Windows.Forms.DataGridView();
            this.BaptismPersonId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismPersonName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismBirthDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismBirthPlace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismMother = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismFather = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaptismDateBaptized = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnBaptismSearch = new System.Windows.Forms.Button();
            this.txtBaptismSearch = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBaptismAdd = new System.Windows.Forms.Button();
            this.btnBaptismEdit = new System.Windows.Forms.Button();
            this.btnBaptismDelete = new System.Windows.Forms.Button();
            this.tabConfirmation = new System.Windows.Forms.TabPage();
            this.dataGridConfirmation = new System.Windows.Forms.DataGridView();
            this.ConfirmationPersonId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmationName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmationGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmationMother = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmationFather = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ConfirmationDateConfirmed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnConfirmationSearch = new System.Windows.Forms.Button();
            this.txtConfirmationSearch = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnConfirmationAdd = new System.Windows.Forms.Button();
            this.btnConfirmationEdit = new System.Windows.Forms.Button();
            this.btnConfirmationDelete = new System.Windows.Forms.Button();
            this.tabDeath = new System.Windows.Forms.TabPage();
            this.dataGridDeath = new System.Windows.Forms.DataGridView();
            this.DeathPersonId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathBirthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathGender = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathMother = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathFather = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathSpouse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeathDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnDeathSearch = new System.Windows.Forms.Button();
            this.txtDeathSearch = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnDeathAdd = new System.Windows.Forms.Button();
            this.btnDeathEdit = new System.Windows.Forms.Button();
            this.btnDeathDelete = new System.Windows.Forms.Button();
            this.panelRecords = new System.Windows.Forms.Panel();
            this.menuStrip1.SuspendLayout();
            this.panelHome.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabBaptism.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBaptism)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabConfirmation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridConfirmation)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabDeath.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDeath)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panelRecords.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.certificatesToolStripMenuItem,
            this.requestsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // logutToolStripMenuItem
            // 
            this.logutToolStripMenuItem.Name = "logutToolStripMenuItem";
            this.logutToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.logutToolStripMenuItem.Text = "Logout";
            this.logutToolStripMenuItem.Click += new System.EventHandler(this.logutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // certificatesToolStripMenuItem
            // 
            this.certificatesToolStripMenuItem.Name = "certificatesToolStripMenuItem";
            this.certificatesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.certificatesToolStripMenuItem.Text = "Records";
            this.certificatesToolStripMenuItem.Click += new System.EventHandler(this.certificatesToolStripMenuItem_Click);
            // 
            // requestsToolStripMenuItem
            // 
            this.requestsToolStripMenuItem.Name = "requestsToolStripMenuItem";
            this.requestsToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.requestsToolStripMenuItem.Text = "Requests";
            this.requestsToolStripMenuItem.Click += new System.EventHandler(this.requestsToolStripMenuItem_Click);
            // 
            // panelHome
            // 
            this.panelHome.Controls.Add(this.label5);
            this.panelHome.Controls.Add(this.label4);
            this.panelHome.Controls.Add(this.label3);
            this.panelHome.Controls.Add(this.pictureBox1);
            this.panelHome.Location = new System.Drawing.Point(65, 146);
            this.panelHome.Name = "panelHome";
            this.panelHome.Size = new System.Drawing.Size(654, 230);
            this.panelHome.TabIndex = 1;
            this.panelHome.Paint += new System.Windows.Forms.PaintEventHandler(this.panelHome_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Georgia", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(297, 140);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(246, 34);
            this.label5.TabIndex = 3;
            this.label5.Text = "6002, Philippines";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Georgia", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(242, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(345, 34);
            this.label4.TabIndex = 2;
            this.label4.Text = "San Vicente, Liloan, Cebu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Old English Text MT", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(186, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(463, 44);
            this.label3.TabIndex = 1;
            this.label3.Text = "San Vicente Ferrer Parish";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(168, 167);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "Test";
            this.notifyIcon1.BalloonTipTitle = "Test";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Test Icon";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabBaptism);
            this.tabControl1.Controls.Add(this.tabConfirmation);
            this.tabControl1.Controls.Add(this.tabDeath);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(10, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(764, 527);
            this.tabControl1.TabIndex = 5;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabBaptism
            // 
            this.tabBaptism.Controls.Add(this.dataGridBaptism);
            this.tabBaptism.Controls.Add(this.panel2);
            this.tabBaptism.Controls.Add(this.panel1);
            this.tabBaptism.Location = new System.Drawing.Point(4, 22);
            this.tabBaptism.Name = "tabBaptism";
            this.tabBaptism.Padding = new System.Windows.Forms.Padding(3);
            this.tabBaptism.Size = new System.Drawing.Size(756, 501);
            this.tabBaptism.TabIndex = 0;
            this.tabBaptism.Text = "Baptism";
            this.tabBaptism.UseVisualStyleBackColor = true;
            // 
            // dataGridBaptism
            // 
            this.dataGridBaptism.AllowUserToAddRows = false;
            this.dataGridBaptism.AllowUserToDeleteRows = false;
            this.dataGridBaptism.AllowUserToResizeRows = false;
            this.dataGridBaptism.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridBaptism.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BaptismPersonId,
            this.BaptismPersonName,
            this.BaptismBirthDay,
            this.BaptismBirthPlace,
            this.BaptismGender,
            this.BaptismMother,
            this.BaptismFather,
            this.BaptismAddress,
            this.BaptismDateBaptized});
            this.dataGridBaptism.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridBaptism.Location = new System.Drawing.Point(3, 34);
            this.dataGridBaptism.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.dataGridBaptism.MultiSelect = false;
            this.dataGridBaptism.Name = "dataGridBaptism";
            this.dataGridBaptism.ReadOnly = true;
            this.dataGridBaptism.RowHeadersVisible = false;
            this.dataGridBaptism.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridBaptism.ShowEditingIcon = false;
            this.dataGridBaptism.Size = new System.Drawing.Size(750, 410);
            this.dataGridBaptism.TabIndex = 7;
            this.dataGridBaptism.SelectionChanged += new System.EventHandler(this.dataGridBaptism_SelectionChanged);
            // 
            // BaptismPersonId
            // 
            this.BaptismPersonId.DataPropertyName = "BaptismPersonId";
            this.BaptismPersonId.HeaderText = "Id";
            this.BaptismPersonId.Name = "BaptismPersonId";
            this.BaptismPersonId.ReadOnly = true;
            this.BaptismPersonId.Visible = false;
            // 
            // BaptismPersonName
            // 
            this.BaptismPersonName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaptismPersonName.DataPropertyName = "BaptismPersonName";
            this.BaptismPersonName.HeaderText = "Name";
            this.BaptismPersonName.Name = "BaptismPersonName";
            this.BaptismPersonName.ReadOnly = true;
            // 
            // BaptismBirthDay
            // 
            this.BaptismBirthDay.DataPropertyName = "BirthDay";
            this.BaptismBirthDay.HeaderText = "Birthday";
            this.BaptismBirthDay.Name = "BaptismBirthDay";
            this.BaptismBirthDay.ReadOnly = true;
            // 
            // BaptismBirthPlace
            // 
            this.BaptismBirthPlace.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaptismBirthPlace.DataPropertyName = "BaptismBirthPlace";
            this.BaptismBirthPlace.HeaderText = "Birth Place";
            this.BaptismBirthPlace.Name = "BaptismBirthPlace";
            this.BaptismBirthPlace.ReadOnly = true;
            // 
            // BaptismGender
            // 
            this.BaptismGender.DataPropertyName = "BaptismGender";
            this.BaptismGender.HeaderText = "Gender";
            this.BaptismGender.Name = "BaptismGender";
            this.BaptismGender.ReadOnly = true;
            // 
            // BaptismMother
            // 
            this.BaptismMother.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaptismMother.DataPropertyName = "BaptismMother";
            this.BaptismMother.HeaderText = "Mother";
            this.BaptismMother.Name = "BaptismMother";
            this.BaptismMother.ReadOnly = true;
            // 
            // BaptismFather
            // 
            this.BaptismFather.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaptismFather.DataPropertyName = "BaptismFather";
            this.BaptismFather.HeaderText = "Father";
            this.BaptismFather.Name = "BaptismFather";
            this.BaptismFather.ReadOnly = true;
            // 
            // BaptismAddress
            // 
            this.BaptismAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaptismAddress.DataPropertyName = "BaptismAddress";
            this.BaptismAddress.HeaderText = "Residence";
            this.BaptismAddress.Name = "BaptismAddress";
            this.BaptismAddress.ReadOnly = true;
            // 
            // BaptismDateBaptized
            // 
            this.BaptismDateBaptized.DataPropertyName = "BaptismDateBaptized";
            this.BaptismDateBaptized.HeaderText = "Date Baptized";
            this.BaptismDateBaptized.Name = "BaptismDateBaptized";
            this.BaptismDateBaptized.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnBaptismSearch);
            this.panel2.Controls.Add(this.txtBaptismSearch);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.panel2.Size = new System.Drawing.Size(750, 31);
            this.panel2.TabIndex = 6;
            // 
            // btnBaptismSearch
            // 
            this.btnBaptismSearch.Location = new System.Drawing.Point(343, 1);
            this.btnBaptismSearch.Name = "btnBaptismSearch";
            this.btnBaptismSearch.Size = new System.Drawing.Size(75, 23);
            this.btnBaptismSearch.TabIndex = 2;
            this.btnBaptismSearch.Text = "Search";
            this.btnBaptismSearch.UseVisualStyleBackColor = true;
            // 
            // txtBaptismSearch
            // 
            this.txtBaptismSearch.Location = new System.Drawing.Point(52, 3);
            this.txtBaptismSearch.Name = "txtBaptismSearch";
            this.txtBaptismSearch.Size = new System.Drawing.Size(288, 20);
            this.txtBaptismSearch.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Search:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnBaptismAdd);
            this.panel1.Controls.Add(this.btnBaptismEdit);
            this.panel1.Controls.Add(this.btnBaptismDelete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 444);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel1.Size = new System.Drawing.Size(750, 54);
            this.panel1.TabIndex = 5;
            // 
            // btnBaptismAdd
            // 
            this.btnBaptismAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBaptismAdd.Location = new System.Drawing.Point(525, 10);
            this.btnBaptismAdd.Name = "btnBaptismAdd";
            this.btnBaptismAdd.Size = new System.Drawing.Size(75, 34);
            this.btnBaptismAdd.TabIndex = 2;
            this.btnBaptismAdd.Text = "Add";
            this.btnBaptismAdd.UseVisualStyleBackColor = true;
            this.btnBaptismAdd.Click += new System.EventHandler(this.btnBaptismAdd_Click);
            // 
            // btnBaptismEdit
            // 
            this.btnBaptismEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBaptismEdit.Enabled = false;
            this.btnBaptismEdit.Location = new System.Drawing.Point(600, 10);
            this.btnBaptismEdit.Name = "btnBaptismEdit";
            this.btnBaptismEdit.Size = new System.Drawing.Size(75, 34);
            this.btnBaptismEdit.TabIndex = 3;
            this.btnBaptismEdit.Text = "Edit";
            this.btnBaptismEdit.UseVisualStyleBackColor = true;
            this.btnBaptismEdit.Click += new System.EventHandler(this.btnBaptismEdit_Click);
            // 
            // btnBaptismDelete
            // 
            this.btnBaptismDelete.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnBaptismDelete.Enabled = false;
            this.btnBaptismDelete.Location = new System.Drawing.Point(675, 10);
            this.btnBaptismDelete.Name = "btnBaptismDelete";
            this.btnBaptismDelete.Size = new System.Drawing.Size(75, 34);
            this.btnBaptismDelete.TabIndex = 4;
            this.btnBaptismDelete.Text = "Delete";
            this.btnBaptismDelete.UseVisualStyleBackColor = true;
            this.btnBaptismDelete.Click += new System.EventHandler(this.btnBaptismDelete_Click);
            // 
            // tabConfirmation
            // 
            this.tabConfirmation.Controls.Add(this.dataGridConfirmation);
            this.tabConfirmation.Controls.Add(this.panel3);
            this.tabConfirmation.Controls.Add(this.panel4);
            this.tabConfirmation.Location = new System.Drawing.Point(4, 22);
            this.tabConfirmation.Name = "tabConfirmation";
            this.tabConfirmation.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfirmation.Size = new System.Drawing.Size(756, 501);
            this.tabConfirmation.TabIndex = 1;
            this.tabConfirmation.Text = "Confirmation";
            this.tabConfirmation.UseVisualStyleBackColor = true;
            // 
            // dataGridConfirmation
            // 
            this.dataGridConfirmation.AllowUserToAddRows = false;
            this.dataGridConfirmation.AllowUserToDeleteRows = false;
            this.dataGridConfirmation.AllowUserToResizeRows = false;
            this.dataGridConfirmation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridConfirmation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ConfirmationPersonId,
            this.ConfirmationName,
            this.ConfirmationGender,
            this.ConfirmationMother,
            this.ConfirmationFather,
            this.ConfirmationDateConfirmed});
            this.dataGridConfirmation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridConfirmation.Location = new System.Drawing.Point(3, 34);
            this.dataGridConfirmation.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.dataGridConfirmation.MultiSelect = false;
            this.dataGridConfirmation.Name = "dataGridConfirmation";
            this.dataGridConfirmation.ReadOnly = true;
            this.dataGridConfirmation.RowHeadersVisible = false;
            this.dataGridConfirmation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridConfirmation.ShowEditingIcon = false;
            this.dataGridConfirmation.Size = new System.Drawing.Size(750, 410);
            this.dataGridConfirmation.TabIndex = 10;
            this.dataGridConfirmation.SelectionChanged += new System.EventHandler(this.dataGridConfirmation_SelectionChanged);
            // 
            // ConfirmationPersonId
            // 
            this.ConfirmationPersonId.DataPropertyName = "ConfirmationPersonId";
            this.ConfirmationPersonId.HeaderText = "Id";
            this.ConfirmationPersonId.Name = "ConfirmationPersonId";
            this.ConfirmationPersonId.ReadOnly = true;
            this.ConfirmationPersonId.Visible = false;
            // 
            // ConfirmationName
            // 
            this.ConfirmationName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ConfirmationName.DataPropertyName = "ConfirmationName";
            this.ConfirmationName.HeaderText = "Name";
            this.ConfirmationName.Name = "ConfirmationName";
            this.ConfirmationName.ReadOnly = true;
            // 
            // ConfirmationGender
            // 
            this.ConfirmationGender.DataPropertyName = "ConfirmationGender";
            this.ConfirmationGender.HeaderText = "Gender";
            this.ConfirmationGender.Name = "ConfirmationGender";
            this.ConfirmationGender.ReadOnly = true;
            // 
            // ConfirmationMother
            // 
            this.ConfirmationMother.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ConfirmationMother.DataPropertyName = "ConfirmationMother";
            this.ConfirmationMother.HeaderText = "Mother";
            this.ConfirmationMother.Name = "ConfirmationMother";
            this.ConfirmationMother.ReadOnly = true;
            // 
            // ConfirmationFather
            // 
            this.ConfirmationFather.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ConfirmationFather.DataPropertyName = "ConfirmationFather";
            this.ConfirmationFather.HeaderText = "Father";
            this.ConfirmationFather.Name = "ConfirmationFather";
            this.ConfirmationFather.ReadOnly = true;
            // 
            // ConfirmationDateConfirmed
            // 
            this.ConfirmationDateConfirmed.DataPropertyName = "ConfirmationDateConfirmed";
            this.ConfirmationDateConfirmed.HeaderText = "Date Confirmed";
            this.ConfirmationDateConfirmed.Name = "ConfirmationDateConfirmed";
            this.ConfirmationDateConfirmed.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnConfirmationSearch);
            this.panel3.Controls.Add(this.txtConfirmationSearch);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.panel3.Size = new System.Drawing.Size(750, 31);
            this.panel3.TabIndex = 9;
            // 
            // btnConfirmationSearch
            // 
            this.btnConfirmationSearch.Location = new System.Drawing.Point(343, 1);
            this.btnConfirmationSearch.Name = "btnConfirmationSearch";
            this.btnConfirmationSearch.Size = new System.Drawing.Size(75, 23);
            this.btnConfirmationSearch.TabIndex = 2;
            this.btnConfirmationSearch.Text = "Search";
            this.btnConfirmationSearch.UseVisualStyleBackColor = true;
            // 
            // txtConfirmationSearch
            // 
            this.txtConfirmationSearch.Location = new System.Drawing.Point(52, 3);
            this.txtConfirmationSearch.Name = "txtConfirmationSearch";
            this.txtConfirmationSearch.Size = new System.Drawing.Size(288, 20);
            this.txtConfirmationSearch.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Search:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnConfirmationAdd);
            this.panel4.Controls.Add(this.btnConfirmationEdit);
            this.panel4.Controls.Add(this.btnConfirmationDelete);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(3, 444);
            this.panel4.Name = "panel4";
            this.panel4.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel4.Size = new System.Drawing.Size(750, 54);
            this.panel4.TabIndex = 8;
            // 
            // btnConfirmationAdd
            // 
            this.btnConfirmationAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConfirmationAdd.Location = new System.Drawing.Point(525, 10);
            this.btnConfirmationAdd.Name = "btnConfirmationAdd";
            this.btnConfirmationAdd.Size = new System.Drawing.Size(75, 34);
            this.btnConfirmationAdd.TabIndex = 2;
            this.btnConfirmationAdd.Text = "Add";
            this.btnConfirmationAdd.UseVisualStyleBackColor = true;
            this.btnConfirmationAdd.Click += new System.EventHandler(this.btnConfirmationAdd_Click);
            // 
            // btnConfirmationEdit
            // 
            this.btnConfirmationEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConfirmationEdit.Enabled = false;
            this.btnConfirmationEdit.Location = new System.Drawing.Point(600, 10);
            this.btnConfirmationEdit.Name = "btnConfirmationEdit";
            this.btnConfirmationEdit.Size = new System.Drawing.Size(75, 34);
            this.btnConfirmationEdit.TabIndex = 3;
            this.btnConfirmationEdit.Text = "Edit";
            this.btnConfirmationEdit.UseVisualStyleBackColor = true;
            this.btnConfirmationEdit.Click += new System.EventHandler(this.btnConfirmationEdit_Click);
            // 
            // btnConfirmationDelete
            // 
            this.btnConfirmationDelete.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnConfirmationDelete.Enabled = false;
            this.btnConfirmationDelete.Location = new System.Drawing.Point(675, 10);
            this.btnConfirmationDelete.Name = "btnConfirmationDelete";
            this.btnConfirmationDelete.Size = new System.Drawing.Size(75, 34);
            this.btnConfirmationDelete.TabIndex = 4;
            this.btnConfirmationDelete.Text = "Delete";
            this.btnConfirmationDelete.UseVisualStyleBackColor = true;
            this.btnConfirmationDelete.Click += new System.EventHandler(this.btnConfirmationDelete_Click);
            // 
            // tabDeath
            // 
            this.tabDeath.Controls.Add(this.dataGridDeath);
            this.tabDeath.Controls.Add(this.panel5);
            this.tabDeath.Controls.Add(this.panel6);
            this.tabDeath.Location = new System.Drawing.Point(4, 22);
            this.tabDeath.Name = "tabDeath";
            this.tabDeath.Size = new System.Drawing.Size(756, 501);
            this.tabDeath.TabIndex = 2;
            this.tabDeath.Text = "Death";
            this.tabDeath.UseVisualStyleBackColor = true;
            // 
            // dataGridDeath
            // 
            this.dataGridDeath.AllowUserToAddRows = false;
            this.dataGridDeath.AllowUserToDeleteRows = false;
            this.dataGridDeath.AllowUserToResizeRows = false;
            this.dataGridDeath.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridDeath.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DeathPersonId,
            this.DeathName,
            this.DeathBirthday,
            this.DeathGender,
            this.DeathMother,
            this.DeathFather,
            this.DeathSpouse,
            this.DeathAddress,
            this.DeathDate});
            this.dataGridDeath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridDeath.Location = new System.Drawing.Point(0, 31);
            this.dataGridDeath.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.dataGridDeath.MultiSelect = false;
            this.dataGridDeath.Name = "dataGridDeath";
            this.dataGridDeath.ReadOnly = true;
            this.dataGridDeath.RowHeadersVisible = false;
            this.dataGridDeath.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridDeath.ShowEditingIcon = false;
            this.dataGridDeath.Size = new System.Drawing.Size(756, 416);
            this.dataGridDeath.TabIndex = 13;
            this.dataGridDeath.SelectionChanged += new System.EventHandler(this.dataGridDeath_SelectionChanged);
            // 
            // DeathPersonId
            // 
            this.DeathPersonId.DataPropertyName = "DeathPersonId";
            this.DeathPersonId.HeaderText = "Id";
            this.DeathPersonId.Name = "DeathPersonId";
            this.DeathPersonId.ReadOnly = true;
            this.DeathPersonId.Visible = false;
            // 
            // DeathName
            // 
            this.DeathName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeathName.DataPropertyName = "DeathName";
            this.DeathName.HeaderText = "Name";
            this.DeathName.Name = "DeathName";
            this.DeathName.ReadOnly = true;
            // 
            // DeathBirthday
            // 
            this.DeathBirthday.DataPropertyName = "DeathBirthday";
            this.DeathBirthday.HeaderText = "Birthday";
            this.DeathBirthday.Name = "DeathBirthday";
            this.DeathBirthday.ReadOnly = true;
            // 
            // DeathGender
            // 
            this.DeathGender.DataPropertyName = "DeathGender";
            this.DeathGender.HeaderText = "Gender";
            this.DeathGender.Name = "DeathGender";
            this.DeathGender.ReadOnly = true;
            // 
            // DeathMother
            // 
            this.DeathMother.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeathMother.DataPropertyName = "DeathMother";
            this.DeathMother.HeaderText = "Mother";
            this.DeathMother.Name = "DeathMother";
            this.DeathMother.ReadOnly = true;
            // 
            // DeathFather
            // 
            this.DeathFather.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeathFather.DataPropertyName = "DeathFather";
            this.DeathFather.HeaderText = "Father";
            this.DeathFather.Name = "DeathFather";
            this.DeathFather.ReadOnly = true;
            // 
            // DeathSpouse
            // 
            this.DeathSpouse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeathSpouse.DataPropertyName = "DeathSpouse";
            this.DeathSpouse.HeaderText = "Spouse";
            this.DeathSpouse.Name = "DeathSpouse";
            this.DeathSpouse.ReadOnly = true;
            // 
            // DeathAddress
            // 
            this.DeathAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DeathAddress.DataPropertyName = "DeathAddress";
            this.DeathAddress.HeaderText = "Address";
            this.DeathAddress.Name = "DeathAddress";
            this.DeathAddress.ReadOnly = true;
            // 
            // DeathDate
            // 
            this.DeathDate.DataPropertyName = "DeathDate";
            this.DeathDate.HeaderText = "Date Died";
            this.DeathDate.Name = "DeathDate";
            this.DeathDate.ReadOnly = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnDeathSearch);
            this.panel5.Controls.Add(this.txtDeathSearch);
            this.panel5.Controls.Add(this.label8);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.panel5.Size = new System.Drawing.Size(756, 31);
            this.panel5.TabIndex = 12;
            // 
            // btnDeathSearch
            // 
            this.btnDeathSearch.Location = new System.Drawing.Point(343, 1);
            this.btnDeathSearch.Name = "btnDeathSearch";
            this.btnDeathSearch.Size = new System.Drawing.Size(75, 23);
            this.btnDeathSearch.TabIndex = 2;
            this.btnDeathSearch.Text = "Search";
            this.btnDeathSearch.UseVisualStyleBackColor = true;
            // 
            // txtDeathSearch
            // 
            this.txtDeathSearch.Location = new System.Drawing.Point(52, 3);
            this.txtDeathSearch.Name = "txtDeathSearch";
            this.txtDeathSearch.Size = new System.Drawing.Size(288, 20);
            this.txtDeathSearch.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Search:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnDeathAdd);
            this.panel6.Controls.Add(this.btnDeathEdit);
            this.panel6.Controls.Add(this.btnDeathDelete);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 447);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.panel6.Size = new System.Drawing.Size(756, 54);
            this.panel6.TabIndex = 11;
            // 
            // btnDeathAdd
            // 
            this.btnDeathAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeathAdd.Location = new System.Drawing.Point(531, 10);
            this.btnDeathAdd.Name = "btnDeathAdd";
            this.btnDeathAdd.Size = new System.Drawing.Size(75, 34);
            this.btnDeathAdd.TabIndex = 2;
            this.btnDeathAdd.Text = "Add";
            this.btnDeathAdd.UseVisualStyleBackColor = true;
            this.btnDeathAdd.Click += new System.EventHandler(this.btnDeathAdd_Click);
            // 
            // btnDeathEdit
            // 
            this.btnDeathEdit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeathEdit.Enabled = false;
            this.btnDeathEdit.Location = new System.Drawing.Point(606, 10);
            this.btnDeathEdit.Name = "btnDeathEdit";
            this.btnDeathEdit.Size = new System.Drawing.Size(75, 34);
            this.btnDeathEdit.TabIndex = 3;
            this.btnDeathEdit.Text = "Edit";
            this.btnDeathEdit.UseVisualStyleBackColor = true;
            this.btnDeathEdit.Click += new System.EventHandler(this.btbDeathEdit_Click);
            // 
            // btnDeathDelete
            // 
            this.btnDeathDelete.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnDeathDelete.Enabled = false;
            this.btnDeathDelete.Location = new System.Drawing.Point(681, 10);
            this.btnDeathDelete.Name = "btnDeathDelete";
            this.btnDeathDelete.Size = new System.Drawing.Size(75, 34);
            this.btnDeathDelete.TabIndex = 4;
            this.btnDeathDelete.Text = "Delete";
            this.btnDeathDelete.UseVisualStyleBackColor = true;
            // 
            // panelRecords
            // 
            this.panelRecords.Controls.Add(this.tabControl1);
            this.panelRecords.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRecords.Location = new System.Drawing.Point(0, 24);
            this.panelRecords.Name = "panelRecords";
            this.panelRecords.Padding = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.panelRecords.Size = new System.Drawing.Size(784, 537);
            this.panelRecords.TabIndex = 2;
            this.panelRecords.Visible = false;
            // 
            // MainScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.panelRecords);
            this.Controls.Add(this.panelHome);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "San Vicente Parish Booking";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainScreen_Close);
            this.Load += new System.EventHandler(this.MainScreen_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panelHome.ResumeLayout(false);
            this.panelHome.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabBaptism.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridBaptism)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabConfirmation.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridConfirmation)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.tabDeath.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridDeath)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panelRecords.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLogCancel;
        private System.Windows.Forms.Button btnLogLogin;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem certificatesToolStripMenuItem;
        private System.Windows.Forms.Panel panelHome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabBaptism;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnBaptismSearch;
        private System.Windows.Forms.TextBox txtBaptismSearch;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBaptismAdd;
        private System.Windows.Forms.Button btnBaptismEdit;
        private System.Windows.Forms.Button btnBaptismDelete;
        private System.Windows.Forms.TabPage tabConfirmation;
        private System.Windows.Forms.Panel panelRecords;
        private System.Windows.Forms.TabPage tabDeath;
        private System.Windows.Forms.ToolStripMenuItem requestsToolStripMenuItem;
        private System.Windows.Forms.DataGridView dataGridConfirmation;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnConfirmationSearch;
        private System.Windows.Forms.TextBox txtConfirmationSearch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnConfirmationAdd;
        private System.Windows.Forms.Button btnConfirmationEdit;
        private System.Windows.Forms.Button btnConfirmationDelete;
        private System.Windows.Forms.DataGridView dataGridDeath;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnDeathSearch;
        private System.Windows.Forms.TextBox txtDeathSearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnDeathAdd;
        private System.Windows.Forms.Button btnDeathEdit;
        private System.Windows.Forms.Button btnDeathDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismPersonId;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismPersonName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismBirthDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismBirthPlace;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismMother;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismFather;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaptismDateBaptized;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmationPersonId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmationName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmationGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmationMother;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmationFather;
        private System.Windows.Forms.DataGridViewTextBoxColumn ConfirmationDateConfirmed;
        private System.Windows.Forms.DataGridView dataGridBaptism;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathPersonId;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathBirthday;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathGender;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathMother;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathFather;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathSpouse;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeathDate;

    }
}

