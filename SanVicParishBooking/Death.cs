﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlClient;

namespace SanVicParishBooking
{
    public partial class formDeath : Form
    {
        private SanVicParishDb _dbInstance = SanVicParishDb.instance;
        private List<Control> _required = new List<Control>();
        private FormValidation _formValidation;
        private MainScreen _mainScreen;

        private int _deathPersonId;

        private string _resCountryName;
        private string _resProvinceName;
        private string _resCityStateName;
        private string _deathCountryName;
        private string _deathProvinceName;
        private string _deathCityStateName;
        private string _burialCountryName;
        private string _burialProvinceName;
        private string _burialCityStateName;

        private int _resCountryId;
        private int _resProvinceId;
        private int _resCityStateId;
        private int _deathCountryId;
        private int _deathProvinceId;
        private int _deathCityStateId;
        private int _burialCountryId;
        private int _burialProvinceId;
        private int _burialCityStateId;

        public int DeathPersonId
        {
            get
            {
                return _deathPersonId;
            }
            set
            {
                _deathPersonId = value;
            }
        }

        public formDeath()
        {
            InitializeComponent();
            this.initializeRequiredFields();
        }

        public formDeath(MainScreen mainScreen )
        {
            InitializeComponent();
            this._mainScreen = mainScreen;
            this.initializeRequiredFields();
        }

        private void initializeRequiredFields()
        {
            string[] requiredFields = {
                "txtDeathFirstName",
                "txtDeathMiddleName",
                "txtDeathLastName",
                "comboDeathGender",
                "numDeathAge",
                "dateDeathBirth",
                "numDeathPageNo",
                "numDeathRegisterNumber",
                "numDeathYear",
                "numDeathVolume",
                "txtDeathResidenceAddress1",
                "comboDeathResidenceCountry",
                "comboDeathResidenceProvince",
                "comboDeathResidenceCity",
                "txtDeathPlaceAddress1",
                "comboDeathPlaceCountry",
                "comboDeathPlaceProvince",
                "comboDeathPlaceCityState",
                "dateDeath",
                "timeDeath",
                "txtDeathBurialAddress1",
                "comboDeathBurialCountry",
                "comboDeathBurialProvince",
                "comboDeathBurialCityState",
                "dateTimeDeathBurial",
                "txtDeathFFirstName",
                "txtDeathFLastName",
                "txtDeathMFirstName",
                "txtDeathMLastName"
            };

            this._formValidation = new FormValidation(this.Controls, requiredFields);
        }

        private void btnConfCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void txtDeathFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathMiddleName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathAge_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathRegisterNumber_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathYear_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathVolume_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void btnConfSave_Click(object sender, EventArgs e)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            string gender;
            bool success = false;

            if (this._formValidation.Validate())
            {
                try
                {
                    conn = this._dbInstance.openConnection();

                    if (this._deathPersonId > 0)
                    {
                        cmd = new SqlCommand("SP_EditDeath", conn);
                    }
                    else
                    {
                        cmd = new SqlCommand("SP_NewDeath", conn);
                    }

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@Firstname", this.txtDeathFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MiddleName", this.txtDeathMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@LastName", this.txtDeathLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@BirthDay", this.dateDeathBirth.Value.ToString()));
                    Console.WriteLine(this.dateDeathBirth.Value.ToString());

                    Console.WriteLine(((ComboBoxItem)this.comboDeathGender.SelectedItem).Value);
                    if ((int)((ComboBoxItem)this.comboDeathGender.SelectedItem).Value == 1)
                    {
                        gender = "Male";
                    }
                    else
                    {
                        gender = "Female";
                    }
                    cmd.Parameters.Add(new SqlParameter("@Gender", gender));

                    cmd.Parameters.Add(new SqlParameter("@MotherFirstName", this.txtDeathMFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MotherMiddleName", this.txtDeathMMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@MotherLastName", this.txtDeathMLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherFirstName", this.txtDeathFFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherMiddleName", this.txtDeathFMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@FatherLastName", this.txtDeathFLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@SpouseFirstname", this.txtDeathSpouseFirstName.Text));
                    cmd.Parameters.Add(new SqlParameter("@SpouseMiddleName", this.txtDeathSpouseMiddleName.Text));
                    cmd.Parameters.Add(new SqlParameter("@SpouseLastName", this.txtDeathSpouseLastName.Text));
                    cmd.Parameters.Add(new SqlParameter("@Address1", this.txtDeathResidenceAddress1.Text));
                    cmd.Parameters.Add(new SqlParameter("@Address2", this.txtDeathResidenceAddress2.Text));
                    cmd.Parameters.Add(new SqlParameter("@CityState", (int)((ComboBoxItem)this.comboDeathResidenceCity.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@Province", (int)((ComboBoxItem)this.comboDeathResidenceProvince.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@Country", (int)((ComboBoxItem)this.comboDeathResidenceCountry.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@DeathRegisterPage", this.numDeathPageNo.Text));
                    cmd.Parameters.Add(new SqlParameter("@DeathRegisterNumber", this.numDeathRegisterNumber.Text));
                    cmd.Parameters.Add(new SqlParameter("@DeathRegisterYear", this.numDeathYear.Text));
                    cmd.Parameters.Add(new SqlParameter("@DeathRegisterVolume", this.numDeathVolume.Text));
                    cmd.Parameters.Add(new SqlParameter("@DeathAddress1", this.txtDeathPlaceAddress1.Text));
                    cmd.Parameters.Add(new SqlParameter("@DeathAddress2", this.txtDeathPlaceAddress2.Text));
                    cmd.Parameters.Add(new SqlParameter("@DeathCityState", (int)((ComboBoxItem)this.comboDeathPlaceCityState.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@DeathProvince", (int)((ComboBoxItem)this.comboDeathPlaceProvince.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@DeathCountry", (int)((ComboBoxItem)this.comboDeathPlaceCountry.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@DateDied", this.dateDeath.Value.ToString()));
                    cmd.Parameters.Add(new SqlParameter("@TimeDied", this.timeDeath.Text.ToString()));

                    Console.WriteLine(this.dateDeath.Value.ToString());
                    Console.WriteLine(this.timeDeath.Text.ToString());
                    cmd.Parameters.Add(new SqlParameter("@Age", this.numDeathAge.Text));
                    cmd.Parameters.Add(new SqlParameter("@BurialAddress1", this.txtDeathBurialAddress1.Text));
                    cmd.Parameters.Add(new SqlParameter("@BurialAddress2", this.txtDeathBurialAddress2.Text));
                    cmd.Parameters.Add(new SqlParameter("@BurialCityState", (int)((ComboBoxItem)this.comboDeathBurialCityState.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@BurialProvince", (int)((ComboBoxItem)this.comboDeathBurialProvince.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@BurialCountry", (int)((ComboBoxItem)this.comboDeathBurialCountry.SelectedItem).Value));
                    cmd.Parameters.Add(new SqlParameter("@BurialDate", this.dateTimeDeathBurial.Value.ToString()));

                    Console.WriteLine(this.dateTimeDeathBurial.Value.ToString());
                    if (this._deathPersonId > 0)
                    {
                        cmd.Parameters.Add(new SqlParameter("@UpdatedBy", Session._UserLoginId));
                        cmd.Parameters.Add(new SqlParameter("@DeathPersonId", this._deathPersonId));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@AddedBy", Session._UserLoginId));
                    }

                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        if ((int)rdr["DeathPersonId"] > 0)
                        {
                            success = true;
                        }
                    }
                }
                finally
                {
                    if (conn != null)
                    {
                        this._dbInstance.closeConnection();
                        conn = null;
                    }

                    if (rdr != null)
                    {
                        rdr.Close();
                        rdr = null;
                    }

                    if (success)
                    {
                        MessageBox.Show(
                            "Successfully added/updated record",
                            "Success",
                            MessageBoxButtons.OK
                        );

                        this.Hide();
                    }
                }
            }
        }

        private void formDeath_Load(object sender, EventArgs e)
        {
            this._loadGender();

            if (this._deathPersonId > 0)
            {
                this._populateFields();
            }
            else
            {
                this._loadCountries();
            }
        }

        private void _loadGender()
        {
            this.comboDeathGender.Items.Add(new ComboBoxItem("Male", 1));
            this.comboDeathGender.Items.Add(new ComboBoxItem("Female", 2));
        }

        private void _populateFields()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("SELECT * FROM V_Death WHERE DeathPersonId = @DeathPersonId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@DeathPersonId", this._deathPersonId));

                rdr = cmd.ExecuteReader();

                if (rdr.HasRows)
                {
                    while (rdr.Read())
                    {
                        this.txtDeathFirstName.Text = (string)rdr["Firstname"];
                        this.txtDeathMiddleName.Text = (string)rdr["MiddleName"];
                        this.txtDeathLastName.Text = (string)rdr["LastName"];
                        this.comboDeathGender.SelectedIndex = this.comboDeathGender.FindString((string)rdr["Gender"]);
                        this.numDeathAge.Text = rdr["AgeDied"].ToString();
                        this.dateDeathBirth.Text = (String)rdr["DeathBirthday"];

                        this.txtDeathPlaceAddress1.Text = (string)rdr["DeathAddress1"];
                        this.txtDeathPlaceAddress2.Text = (string)rdr["DeathAddress2"];

                        this._deathCountryId = (int)rdr["DeathCountryId"];
                        this._deathProvinceId = (int)rdr["DeathProvinceId"];
                        this._deathCityStateId = (int)rdr["DeathCityStateId"];

                        this._deathCountryName = (string)rdr["DeathCountry"];
                        this._deathProvinceName = (string)rdr["DeathProvince"];
                        this._deathCityStateName = (string)rdr["DeathCityState"];

                        this.dateDeath.Text = (String)rdr["DeathDate"];
                        this.timeDeath.Text = (String)rdr["TimeDied"];

                        this.txtDeathBurialAddress1.Text = (string)rdr["BurialAddress1"];
                        this.txtDeathBurialAddress2.Text = (string)rdr["BurialAddress2"];

                        this._burialCountryId = (int)rdr["BurialCountryId"];
                        this._burialProvinceId = (int)rdr["BurialProvinceId"];
                        this._burialCityStateId = (int)rdr["BurialCityStateId"];

                        this._burialCountryName = (string)rdr["BurialCountry"];
                        this._burialProvinceName = (string)rdr["BurialProvince"];
                        this._burialCityStateName = (string)rdr["BurialCityState"];
                        this.dateTimeDeathBurial.Text = (String)rdr["BurialDate"];

                        this.numDeathPageNo.Text = rdr["DeathRegisterPage"].ToString();
                        this.numDeathRegisterNumber.Text = rdr["DeathRegisterNumber"].ToString();
                        this.numDeathYear.Text = rdr["DeathRegisterYear"].ToString();
                        this.numDeathVolume.Text = rdr["DeathRegisterVolume"].ToString();

                        this.txtDeathResidenceAddress1.Text = (string)rdr["Address1"];
                        this.txtDeathResidenceAddress2.Text = (string)rdr["Address2"];

                        this._resCountryId = (int)rdr["ResCountryId"];
                        this._resProvinceId = (int)rdr["ResProvinceId"];
                        this._resCityStateId = (int)rdr["ResCityStateId"];

                        this._resCountryName = (string)rdr["ResCountry"];
                        this._resProvinceName = (string)rdr["ResProvince"];
                        this._resCityStateName = (string)rdr["ResCityState"];

                        this.txtDeathSpouseFirstName.Text = (string)rdr["SpouseFirstName"];
                        this.txtDeathSpouseMiddleName.Text = (string)rdr["SpouseMiddleName"];
                        this.txtDeathSpouseLastName.Text = (string)rdr["SpouseLastName"];

                        this.txtDeathFFirstName.Text = (string)rdr["FatherFirstName"];
                        this.txtDeathFMiddleName.Text = (string)rdr["FatherMiddleName"];
                        this.txtDeathFLastName.Text = (string)rdr["FatherLastName"];

                        this.txtDeathMFirstName.Text = (string)rdr["MotherFirstName"];
                        this.txtDeathMMiddleName.Text = (string)rdr["MotherMiddleName"];
                        this.txtDeathMLastName.Text = (string)rdr["MotherLastName"];

                        this.btnDeathReset.Enabled = false;

                    }
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                this._loadCountries();
            }
        }

        private void _loadCountries()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM Country", conn);
                cmd.CommandType = CommandType.Text;

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathResidenceCountry.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CountryId"].ToString())));
                    this.comboDeathPlaceCountry.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CountryId"].ToString())));
                    this.comboDeathBurialCountry.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CountryId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathResidenceCountry.Text = this._resCountryName;
                    this.comboDeathPlaceCountry.Text = this._deathCountryName;
                    this.comboDeathBurialCountry.Text = this._burialCountryName;

                    this._loadResProvince( this._resCountryId );
                    this._loadDeathProvince(this._deathCountryId);
                    this._loadBurialProvince(this._burialCountryId);
                    
                }
            }
        }

        private void _loadResProvince( int countryId ) {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboDeathResidenceProvince.SelectedIndex = -1;
            this.comboDeathResidenceProvince.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_ProvinceCountry Where CountryId = @CountryId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@CountryId", countryId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathResidenceProvince.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["ProvinceId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathResidenceProvince.Text = this._resProvinceName;
                    this._loadResCity(this._resProvinceId);
                }
            }
        }

        private void _loadDeathProvince(int countryId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboDeathPlaceProvince.SelectedIndex = -1;
            this.comboDeathPlaceProvince.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_ProvinceCountry Where CountryId = @CountryId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@CountryId", countryId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathPlaceProvince.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["ProvinceId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathPlaceProvince.Text = this._deathProvinceName;
                    this._loadDeathCity(this._deathProvinceId);
                }
            }
        }

        private void _loadBurialProvince(int countryId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboDeathBurialProvince.SelectedIndex = -1;
            this.comboDeathBurialProvince.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_ProvinceCountry Where CountryId = @CountryId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@CountryId", countryId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathBurialProvince.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["ProvinceId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathBurialProvince.Text = this._burialProvinceName;
                    this._loadBurialCity(this._burialProvinceId);
                }
            }
        }

        private void _loadResCity(int provinceId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboDeathResidenceCity.SelectedIndex = -1;
            this.comboDeathResidenceCity.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_CityProvince Where ProvinceId = @ProvinceId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ProvinceId", provinceId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathResidenceCity.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CityId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathResidenceCity.Text = this._resCityStateName;
                }
            }
        }

        private void _loadDeathCity(int provinceId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboDeathPlaceCityState.SelectedIndex = -1;
            this.comboDeathPlaceCityState.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_CityProvince Where ProvinceId = @ProvinceId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ProvinceId", provinceId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathPlaceCityState.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CityId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathPlaceCityState.Text = this._deathCityStateName;
                }
            }
        }

        private void _loadBurialCity(int provinceId)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            this.comboDeathBurialCityState.SelectedIndex = -1;
            this.comboDeathBurialCityState.Items.Clear();

            try
            {
                conn = this._dbInstance.openConnection();
                cmd = new SqlCommand("Select * FROM V_CityProvince Where ProvinceId = @ProvinceId", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add(new SqlParameter("@ProvinceId", provinceId));

                rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    this.comboDeathBurialCityState.Items.Add(new ComboBoxItem(rdr["Name"].ToString(), Int32.Parse(rdr["CityId"].ToString())));
                }
            }
            finally
            {
                if (conn != null)
                {
                    this._dbInstance.closeConnection();
                    conn = null;
                }

                if (rdr != null)
                {
                    rdr.Close();
                    rdr = null;
                }

                if (this._deathPersonId > 0)
                {
                    this.comboDeathBurialCityState.Text = this._burialCityStateName;
                }
            }
        }

        private void comboDeathResidenceCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int countryId;

            if (selectedItem != null)
            {
                countryId = (int)selectedItem.Value;

                this._loadResProvince(countryId);

            }
        }

        private void comboDeathPlaceCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int countryId;

            if (selectedItem != null)
            {
                countryId = (int)selectedItem.Value;

                this._loadDeathProvince(countryId);

            }
        }

        private void comboDeathBurialCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int countryId;

            if (selectedItem != null)
            {
                countryId = (int)selectedItem.Value;

                this._loadBurialProvince(countryId);

            }
        }

        private void comboDeathResidenceProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int provinceId;

            if (selectedItem != null)
            {
                provinceId = (int)selectedItem.Value;

                this._loadResCity(provinceId);

            }
        }

        private void comboDeathPlaceProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int provinceId;

            if (selectedItem != null)
            {
                provinceId = (int)selectedItem.Value;

                this._loadDeathCity(provinceId);

            }
        }

        private void comboDeathBurialProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cmb = (ComboBox)sender;
            ComboBoxItem selectedItem = (ComboBoxItem)cmb.SelectedItem;
            int provinceId;

            if (selectedItem != null)
            {
                provinceId = (int)selectedItem.Value;

                this._loadBurialCity(provinceId);

            }
        }

        private void formDeath_FormClosing(object sender, FormClosingEventArgs e)
        {
            this._mainScreen.refreshDataGridDeath();
        }

        private void btnDeathReset_Click(object sender, EventArgs e)
        {
            this._resetFields();
        }

        public void _resetFields()
        {
            this.txtDeathFirstName.Clear();
            this.txtDeathMiddleName.Clear();
            this.txtDeathLastName.Clear();
            this.comboDeathGender.SelectedIndex = -1;
            this.numDeathAge.ResetText();
            this.dateDeathBirth.Text = "";

            this.txtDeathPlaceAddress1.Clear();
            this.txtDeathPlaceAddress2.Clear();
            this.comboDeathPlaceCountry.SelectedIndex = -1;
            this.comboDeathPlaceProvince.SelectedIndex = -1;
            this.comboDeathPlaceCityState.SelectedIndex = -1;

            this.dateDeath.Text = "";
            this.timeDeath.Text = "";

            this.txtDeathBurialAddress1.Clear();
            this.txtDeathBurialAddress2.Clear();
            this.comboDeathBurialCountry.SelectedIndex = -1;
            this.comboDeathBurialProvince.SelectedIndex = -1;
            this.comboDeathBurialCityState.SelectedIndex = -1;

            this.dateTimeDeathBurial.Text = "";

            this.numDeathPageNo.ResetText();
            this.numDeathRegisterNumber.ResetText();
            this.numDeathYear.ResetText();
            this.numDeathVolume.ResetText();

            this.txtDeathResidenceAddress1.Clear();
            this.txtDeathResidenceAddress2.Clear();
            this.comboDeathResidenceCountry.SelectedIndex = -1;
            this.comboDeathResidenceProvince.SelectedIndex = -1;
            this.comboDeathResidenceCity.SelectedIndex = -1;

            this.txtDeathSpouseFirstName.Clear();
            this.txtDeathSpouseMiddleName.Clear();
            this.txtDeathSpouseLastName.Clear();

            this.txtDeathFFirstName.Clear();
            this.txtDeathFMiddleName.Clear();
            this.txtDeathFLastName.Clear();

            this.txtDeathMFirstName.Clear();
            this.txtDeathMMiddleName.Clear();
            this.txtDeathMLastName.Clear();
        }

        private void comboDeathGender_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void numDeathAge_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((NumericUpDown)sender).Text))
            {
                this._formValidation.HideError(((NumericUpDown)sender).Name);
            }
        }

        private void txtDeathPlaceAddress2_Leave(object sender, EventArgs e)
        {

        }

        private void txtDeathPlaceAddress1_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void comboDeathPlaceCountry_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboDeathPlaceProvince_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboDeathPlaceCityState_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void txtDeathBurialAddress1_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void comboDeathBurialCountry_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboDeathBurialProvince_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboDeathBurialCityState_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void numDeathPageNo_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((NumericUpDown)sender).Text))
            {
                this._formValidation.HideError(((NumericUpDown)sender).Name);
            }
        }

        private void numDeathRegisterNumber_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((NumericUpDown)sender).Text))
            {
                this._formValidation.HideError(((NumericUpDown)sender).Name);
            }
        }

        private void numDeathYear_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((NumericUpDown)sender).Text))
            {
                this._formValidation.HideError(((NumericUpDown)sender).Name);
            }
        }

        private void numDeathVolume_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((NumericUpDown)sender).Text))
            {
                this._formValidation.HideError(((NumericUpDown)sender).Name);
            }
        }

        private void txtDeathResidenceAddress1_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void comboDeathResidenceCountry_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboDeathResidenceProvince_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void comboDeathResidenceCity_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((ComboBox)sender).Text))
            {
                this._formValidation.HideError(((ComboBox)sender).Name);
            }
        }

        private void txtDeathFFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathFLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathMFirstName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }

        private void txtDeathMLastName_Leave(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(((TextBox)sender).Text))
            {
                this._formValidation.HideError(((TextBox)sender).Name);
            }
        }
    }
}
