﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace SanVicParishBooking
{
    public sealed class SanVicParishDb
    {
        private static SqlConnection conn;

        public static readonly SanVicParishDb instance = new SanVicParishDb();
        public static SanVicParishDb Instance
        {
	        get
	        {
                return instance;
	        }
        }

        private SanVicParishDb()
        {
            this.createConnection();
        }

        private void createConnection()
        {
            conn = new SqlConnection(ConfigurationManager.AppSettings["DataSource"]);
        }

        public SqlConnection openConnection()
        {
            if (conn == null)
            {
                this.createConnection();
            }
            conn.Open();
            Console.WriteLine("Open Connection");
            return conn;
        }

        public void closeConnection()
        {
            conn.Close();
            Console.WriteLine("Close Connection");
        }

        public void errorTransaction()
        {
            conn.Close();
        }
    }
}
