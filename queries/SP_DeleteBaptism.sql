IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_DeleteBaptism')
BEGIN
	DROP PROCEDURE SP_DeleteBaptism
END

GO

CREATE PROCEDURE SP_DeleteBaptism(
	@BaptismPersonId int,
	@DeletedBy int
)

AS
BEGIN
	DECLARE @PersonId INT;

	SELECT	@PersonId = PersonId
	FROM	BaptismPerson
	WHERE	BaptismPersonId = @BaptismPersonId

	UPDATE	BaptismPerson
	SET		Active = 0,
			DeletedBy = @DeletedBy,
			DateDeleted = GETDATE()
	WHERE BaptismPersonId = @BaptismPersonId

	UPDATE	Person
	SET		Active = 0,
			DeletedBy = @DeletedBy,
			DateDeleted = GETDATE()
	WHERE	PersonId = @PersonId

	SELECT @@ROWCOUNT as UpdatedRows
END
 
GO