IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		WHERE NAME = 'V_CityProvince'
		)
BEGIN
	DROP VIEW V_CityProvince
END
GO

CREATE VIEW V_CityProvince
AS
SELECT		City.CityId,
			City.Code,
			City.Name,
			CityProvince.ProvinceId
FROM		City
INNER JOIN	CityProvince
		ON	City.CityId = CityProvince.CityId

GO