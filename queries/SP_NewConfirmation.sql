IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_NewConfirmation')
BEGIN
	DROP PROCEDURE SP_NewConfirmation
END

GO

CREATE PROCEDURE SP_NewConfirmation(
	@Firstname NVARCHAR(50),
	@MiddleName NVARCHAR(50) = '',
	@LastName NVARCHAR(50),
	@Gender NVARCHAR(10),
	@MotherFirstName NVARCHAR(50),
	@MotherMiddleName NVARCHAR(50),
	@MotherLastName NVARCHAR(50),
	@FatherFirstName NVARCHAR(50),
	@FatherMiddleName NVARCHAR(50),
	@FatherLastName NVARCHAR(50), --end for person
	@ConfAddress1 NVARCHAR(50),
	@ConfAddress2 NVARCHAR(50) = '',
	@ConfCityState int,
	@ConfProvince int,
	@ConfCountry int,
	@DateConfirmed NVARCHAR(50),
	@MinisterId int,
	@Sponsor1Firstname NVARCHAR(50) = '',
	@Sponsor1MiddleName NVARCHAR(50)  = '',
	@Sponsor1LastName NVARCHAR(50)  = '',
	@Sponsor2Firstname NVARCHAR(50)  = '',
	@Sponsor2MiddleName NVARCHAR(50)  = '',
	@Sponsor2LastName NVARCHAR(50)  = '',
	@ConfirmationBookNumber int,
	@ConfirmationPageNumber int,
	@AddedBy int
)

AS
BEGIN
	DECLARE @LatestId INT;
	DECLARE @PersonId INT;

	INSERT INTO Person (
		Firstname,
		MiddleName,
		LastName,
		Gender,
		MotherFirstName,
		MotherMiddleName,
		MotherLastName,
		FatherFirstName,
		FatherMiddleName,
		FatherLastName,
		AddedBy,
		Active
	) VALUES (
		@Firstname,
		@MiddleName,
		@LastName,
		@Gender,
		@MotherFirstName,
		@MotherMiddleName,
		@MotherLastName,
		@FatherFirstName,
		@FatherMiddleName,
		@FatherLastName,
		@AddedBy,
		1
	)

	SET @LatestId = SCOPE_IDENTITY();

	SELECT @PersonId = PersonId
	FROM Person
	WHERE Id = @LatestId

	INSERT INTO ConfirmationPerson(
		PersonId,
		DateConfirmed,
		MinisterId,
		Sponsor1Firstname,
		Sponsor1MiddleName,
		Sponsor1LastName,
		Sponsor2Firstname,
		Sponsor2MiddleName,
		Sponsor2LastName,
		ConfirmationAddress1,
		ConfirmationAddress2,
		ConfirmationCityState,
		ConfirmationProvince,
		ConfirmationCountry,
		ConfirmationBookNumber,
		ConfirmationPageNumber,
		AddedBy,
		Active
	) VALUES (
		@PersonId,
		CONVERT(DATETIME,@DateConfirmed,101),
		@MinisterId,
		@Sponsor1Firstname,
		@Sponsor1MiddleName,
		@Sponsor1LastName,
		@Sponsor2Firstname,
		@Sponsor2MiddleName,
		@Sponsor2LastName,
		@ConfAddress1,
		@ConfAddress2,
		@ConfCityState,
		@ConfProvince,
		@ConfCountry,
		@ConfirmationBookNumber,
		@ConfirmationPageNumber,
		@AddedBy,
		1
	)

	SET @LatestId = SCOPE_IDENTITY();

	SELECT ConfirmationPersonId
	FROM ConfirmationPerson
	WHERE Id = @LatestId
END
 
GO