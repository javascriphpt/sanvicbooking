IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_NewDeath')
BEGIN
	DROP PROCEDURE SP_NewDeath
END

GO

CREATE PROCEDURE SP_NewDeath(
	@Firstname NVARCHAR(50),
	@MiddleName NVARCHAR(50) = '',
	@LastName NVARCHAR(50),
	@BirthDay NVARCHAR(50),
	@Gender NVARCHAR(10),
	@MotherFirstName NVARCHAR(50),
	@MotherMiddleName NVARCHAR(50),
	@MotherLastName NVARCHAR(50),
	@FatherFirstName NVARCHAR(50),
	@FatherMiddleName NVARCHAR(50),
	@FatherLastName NVARCHAR(50),
	@SpouseFirstname NVARCHAR(50) = '',
	@SpouseMiddleName NVARCHAR(50)  = '',
	@SpouseLastName NVARCHAR(50)  = '',
	@Address1 NVARCHAR(50),
	@Address2 NVARCHAR(50),
	@CityState INT,
	@Province INT,
	@Country INT,
	@DeathRegisterPage INT,
	@DeathRegisterNumber INT,
	@DeathRegisterYear INT,
	@DeathRegisterVolume INT,
	@DeathAddress1 NVARCHAR(50),
	@DeathAddress2 NVARCHAR(50),
	@DeathCityState INT,
	@DeathProvince INT,
	@DeathCountry INT,
	@DateDied NVARCHAR(15),
	@TimeDied NVARCHAR(15),
	@Age INT,
	@BurialAddress1 NVARCHAR(50),
	@BurialAddress2 NVARCHAR(50),
	@BurialCityState INT,
	@BurialProvince INT,
	@BurialCountry INT,
	@BurialDate NVARCHAR(15),
	@AddedBy INT
)

AS
BEGIN
	DECLARE @LatestId INT;
	DECLARE @PersonId INT;

	INSERT INTO Person (
		Firstname,
		MiddleName,
		LastName,
		BirthDay,
		Gender,
		MotherFirstName,
		MotherMiddleName,
		MotherLastName,
		FatherFirstName,
		FatherMiddleName,
		FatherLastName,
		SpouseFirstName,
		SpouseMiddleName,
		SpouseLastName,
		Address1,
		Address2,
		CityState,
		Province,
		Country,
		AddedBy,
		Active
	) VALUES (
		@Firstname,
		@MiddleName,
		@LastName,
		CONVERT(DATETIME,@BirthDay,101),
		@Gender,
		@MotherFirstName,
		@MotherMiddleName,
		@MotherLastName,
		@FatherFirstName,
		@FatherMiddleName,
		@FatherLastName,
		@SpouseFirstName,
		@SpouseMiddleName,
		@SpouseLastName,
		@Address1,
		@Address2,
		@CityState,
		@Province,
		@Country,
		@AddedBy,
		1
	)

	SET @LatestId = SCOPE_IDENTITY();

	SELECT @PersonId = PersonId
	FROM Person
	WHERE Id = @LatestId

	INSERT INTO DeathPerson(
		PersonId,
		DeathRegisterPage,
		DeathRegisterNumber,
		DeathRegisterYear,
		DeathRegisterVolume,
		DeathAddress1,
		DeathAddress2,
		DeathCityState,
		DeathProvince,
		DeathCountry,
		DateDied,
		TimeDied,
		AgeDied,
		BurialAddress1,
		BurialAddress2,
		BurialCityState,
		BurialProvince,
		BurialCountry,
		BurialDate,
		AddedBy,
		Active
	) VALUES (
		@PersonId,
		@DeathRegisterPage,
		@DeathRegisterNumber,
		@DeathRegisterYear,
		@DeathRegisterVolume,
		@DeathAddress1,
		@DeathAddress2,
		@DeathCityState,
		@DeathProvince,
		@DeathCountry,
		CONVERT(DATE,@DateDied,101),
		CONVERT(TIME,@TimeDied,108),
		@Age,
		@BurialAddress1,
		@BurialAddress2,
		@BurialCityState,
		@BurialProvince,
		@BurialCountry,
		CONVERT(DATETIME,@BurialDate,101),
		@AddedBy,
		1
	)

	SET @LatestId = SCOPE_IDENTITY();

	SELECT DeathPersonId
	FROM DeathPerson
	WHERE Id = @LatestId
END
 
GO