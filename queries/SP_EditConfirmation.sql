IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_EditConfirmation')
BEGIN
	DROP PROCEDURE SP_EditConfirmation
END

GO

CREATE PROCEDURE SP_EditConfirmation(
	@Firstname NVARCHAR(50),
	@MiddleName NVARCHAR(50) = '',
	@LastName NVARCHAR(50),
	@Gender NVARCHAR(10),
	@MotherFirstName NVARCHAR(50),
	@MotherMiddleName NVARCHAR(50),
	@MotherLastName NVARCHAR(50),
	@FatherFirstName NVARCHAR(50),
	@FatherMiddleName NVARCHAR(50),
	@FatherLastName NVARCHAR(50), --end for person
	@ConfAddress1 NVARCHAR(50),
	@ConfAddress2 NVARCHAR(50) = '',
	@ConfCityState int,
	@ConfProvince int,
	@ConfCountry int,
	@DateConfirmed NVARCHAR(50),
	@MinisterId int,
	@Sponsor1Firstname NVARCHAR(50) = '',
	@Sponsor1MiddleName NVARCHAR(50)  = '',
	@Sponsor1LastName NVARCHAR(50)  = '',
	@Sponsor2Firstname NVARCHAR(50)  = '',
	@Sponsor2MiddleName NVARCHAR(50)  = '',
	@Sponsor2LastName NVARCHAR(50)  = '',
	@ConfirmationBookNumber INT,
	@ConfirmationPageNumber INT,
	@UpdatedBy INT,
	@ConfirmationPersonId INT
)

AS
BEGIN
	DECLARE @LatestId INT;
	DECLARE @PersonId INT;

	SELECT @PersonId = PersonId
	FROM ConfirmationPerson
	WHERE ConfirmationPersonId = @ConfirmationPersonId

	UPDATE	Person
	SET		Firstname = @Firstname,
			MiddleName = @MiddleName,
			LastName = @LastName,
			Gender = @Gender,
			MotherFirstName = @MotherFirstName,
			MotherMiddleName = @MotherMiddleName,
			MotherLastName = @MotherLastName,
			FatherFirstName = @FatherFirstName,
			FatherMiddleName = @FatherMiddleName,
			FatherLastName = @FatherLastName,
			UpdatedBy = @UpdatedBy
	WHERE	PersonId = @PersonId
	AND		Active = 1

	UPDATE	ConfirmationPerson
	SET		DateConfirmed = CONVERT(DATETIME,@DateConfirmed,101),
			MinisterId = @MinisterId,
			Sponsor1Firstname = @Sponsor1Firstname,
			Sponsor1MiddleName = @Sponsor1MiddleName,
			Sponsor1LastName = @Sponsor1LastName,
			Sponsor2Firstname = @Sponsor2Firstname,
			Sponsor2MiddleName = @Sponsor2MiddleName,
			Sponsor2LastName = @Sponsor2LastName,
			ConfirmationAddress1 = @ConfAddress1,
			ConfirmationAddress2 = @ConfAddress2,
			ConfirmationCityState = @ConfCityState,
			ConfirmationProvince = @ConfProvince,
			ConfirmationCountry = @ConfCountry,
			ConfirmationBookNumber = @ConfirmationBookNumber,
			ConfirmationPageNumber = @ConfirmationPageNumber,
			UpdatedBy = @UpdatedBy
	WHERE	ConfirmationPersonId = @ConfirmationPersonId
	AND		Active = 1

	SELECT @@ROWCOUNT as ConfirmationPersonId
END
 
GO