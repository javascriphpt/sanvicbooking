USE [master]
GO
/****** Object:  Database [SanVicParish]    Script Date: 8/20/2016 11:38:08 PM ******/
CREATE DATABASE [SanVicParish]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SanVicParish', FILENAME = N'D:\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SanVicParish.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SanVicParish_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SanVicParish_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SanVicParish] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SanVicParish].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SanVicParish] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SanVicParish] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SanVicParish] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SanVicParish] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SanVicParish] SET ARITHABORT OFF 
GO
ALTER DATABASE [SanVicParish] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SanVicParish] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [SanVicParish] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SanVicParish] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SanVicParish] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SanVicParish] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SanVicParish] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SanVicParish] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SanVicParish] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SanVicParish] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SanVicParish] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SanVicParish] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SanVicParish] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SanVicParish] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SanVicParish] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SanVicParish] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SanVicParish] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SanVicParish] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SanVicParish] SET RECOVERY FULL 
GO
ALTER DATABASE [SanVicParish] SET  MULTI_USER 
GO
ALTER DATABASE [SanVicParish] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SanVicParish] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SanVicParish] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SanVicParish] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SanVicParish', N'ON'
GO
USE [SanVicParish]
GO
/****** Object:  User [SanVicParish]    Script Date: 8/20/2016 11:38:08 PM ******/
CREATE USER [SanVicParish] FOR LOGIN [SanVicParish] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SanVicParish]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SanVicParish]
GO
/****** Object:  StoredProcedure [dbo].[SP_ChangeLoginStatus]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_ChangeLoginStatus](
	@UserLoginId INT,
	@UserLoginType INT,
	@Return INT = 0
)

AS
BEGIN
	INSERT INTO UserLoginHistory(
		UserLoginId,
		UserLoginTypeId
	) VALUES (
		@UserLoginId,
		@UserLoginType
	)

	IF @Return = 1
	BEGIN
		SELECT MAX(UserLoginHistoryId) as UserLoginHistoryId
		FROM UserLoginHistory
		WHERE UserLoginId = @UserLoginId
	END
END


GO
/****** Object:  StoredProcedure [dbo].[SP_Login]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Login](
	@Username VARCHAR(50),
	@Password VARCHAR(50)
)

AS
BEGIN
	DECLARE @UserLoginId INT;
	DECLARE @UserId INT;

	SELECT @UserLoginId = UserLogin.UserLoginId,
			@UserId = [User].UserId
	FROM	UserLogin
	INNER JOIN [User]
			ON UserLogin.UserId = [User].UserId
	WHERE	UserLogin.Username = @Username
	AND		UserLogin.Password = @Password

	IF @UserLoginId > 0
	BEGIN

		EXEC SP_ChangeLoginStatus @UserLoginId, 400001;

		SELECT @UserLoginId AS UserLoginId,
			   @UserId AS UserId
	END
	ELSE
	BEGIN
		SELECT 0 AS UserLoginId,
			   0 AS UserId
	END
END


GO
/****** Object:  Table [dbo].[BaptismPerson]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BaptismPerson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BaptismPersonId]  AS ((2000000)+[id]) PERSISTED,
	[PersonId] [int] NOT NULL,
	[DateBaptized] [datetime] NOT NULL,
	[MinisterId] [int] NOT NULL,
	[Sponsor1Firstname] [nvarchar](50) NULL,
	[Sponsor1MiddleName] [nvarchar](50) NULL,
	[Sponsor1LastName] [nvarchar](50) NULL,
	[Sponsor2Firstname] [nvarchar](50) NULL,
	[Sponsor2MiddleName] [nvarchar](50) NULL,
	[Sponsor2LastName] [nvarchar](50) NULL,
	[BaptismBookNumber] [int] NOT NULL,
	[BaptismPageNumber] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BaptismRequest]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BaptismRequest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BaptismRequestId]  AS ((3000000)+[id]) PERSISTED,
	[BaptismPersonId] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BaptismRequestReport]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BaptismRequestReport](
	[Name] [nvarchar](250) NOT NULL,
	[BirthPlace] [nvarchar](250) NOT NULL,
	[BirthDate] [int] NOT NULL,
	[BirthMonth] [int] NOT NULL,
	[BirthYear] [int] NOT NULL,
	[MotherName] [nvarchar](250) NOT NULL,
	[FatherName] [nvarchar](250) NOT NULL,
	[Address] [nvarchar](250) NOT NULL,
	[BaptizedDate] [int] NOT NULL,
	[BaptizedMonth] [int] NOT NULL,
	[BaptizedYear] [int] NOT NULL,
	[MinisterName] [nvarchar](250) NOT NULL,
	[MinisterRank] [nvarchar](250) NOT NULL,
	[Sponsor1Name] [nvarchar](250) NULL,
	[Sponsor2Name] [nvarchar](250) NULL,
	[BaptismBookNumber] [int] NOT NULL,
	[BaptismPageNumber] [int] NOT NULL,
	[DateIssued] [date] NOT NULL,
	[Purpose] [nvarchar](100) NULL,
	[ParishPriest] [nvarchar](250) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConfirmationPerson]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfirmationPerson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConfirmationPersonId]  AS ((4000000)+[id]) PERSISTED,
	[PersonId] [int] NOT NULL,
	[DateConfirmed] [datetime] NOT NULL,
	[MinisterId] [int] NOT NULL,
	[Sponsor1Firstname] [nvarchar](50) NULL,
	[Sponsor1MiddleName] [nvarchar](50) NULL,
	[Sponsor1LastName] [nvarchar](50) NULL,
	[Sponsor2Firstname] [nvarchar](50) NULL,
	[Sponsor2MiddleName] [nvarchar](50) NULL,
	[Sponsor2LastName] [nvarchar](50) NULL,
	[ConfirmmationAddress1] [nvarchar](50) NOT NULL,
	[ConfirmmationAddress2] [nvarchar](50) NULL,
	[ConfirmmationCityState] [int] NOT NULL,
	[ConfirmmationProvince] [int] NOT NULL,
	[ConfirmmationCountry] [int] NOT NULL,
	[ConfirmationBookNumber] [int] NOT NULL,
	[ConfirmationPageNumber] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConfirmationRequest]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfirmationRequest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ConfirmationRequestId]  AS ((5000000)+[id]) PERSISTED,
	[ConfirmationPersonId] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConfirmationRequestReport]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConfirmationRequestReport](
	[Name] [nvarchar](200) NOT NULL,
	[MotherName] [nvarchar](200) NOT NULL,
	[FatherName] [nvarchar](200) NOT NULL,
	[ConfirmationDate] [int] NOT NULL,
	[ConfirmationMonth] [int] NOT NULL,
	[ConfirmationYear] [int] NOT NULL,
	[MinisterName] [nvarchar](200) NOT NULL,
	[ConfirmationPlace] [nvarchar](200) NOT NULL,
	[Sponsor1Name] [nvarchar](200) NULL,
	[Sponsor2Name] [nvarchar](200) NULL,
	[ParishPriest] [nvarchar](200) NOT NULL,
	[DateIssue] [date] NOT NULL,
	[ConfirmationBookNumber] [int] NOT NULL,
	[ConfirmationPageNumber] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DeathPerson]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeathPerson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeathPersonId]  AS ((6000000)+[id]) PERSISTED,
	[PersonId] [int] NOT NULL,
	[DeathRegisterPage] [int] NOT NULL,
	[DeathRegisterNumber] [int] NOT NULL,
	[DeathRegisterYear] [int] NOT NULL,
	[DeathRegisterVolume] [int] NOT NULL,
	[DeathAddress1] [nvarchar](50) NOT NULL,
	[DeathAddress2] [nvarchar](50) NULL,
	[DeathCityState] [int] NOT NULL,
	[DeathProvince] [int] NOT NULL,
	[DeathCountry] [int] NOT NULL,
	[DateDied] [date] NOT NULL,
	[TimeDied] [time](7) NOT NULL,
	[AgeDied] [int] NOT NULL,
	[BurialAddress1] [nvarchar](50) NOT NULL,
	[BurialAddress2] [nvarchar](50) NULL,
	[BurialCityState] [int] NOT NULL,
	[BurialProvince] [int] NOT NULL,
	[BurialCountry] [int] NOT NULL,
	[BurialDate] [datetime] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeathRequest]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DeathRequest](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeathRequestId]  AS ((7000000)+[id]) PERSISTED,
	[DeathPersonId] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DeathRequestReport]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeathRequestReport](
	[Name] [nvarchar](200) NOT NULL,
	[DeathRegisterPage] [int] NOT NULL,
	[DeathRegisterNumber] [int] NOT NULL,
	[DeathRegisterYear] [int] NOT NULL,
	[DeathRegisterVolume] [int] NOT NULL,
	[DeathPlace] [nvarchar](200) NOT NULL,
	[DateDied] [date] NOT NULL,
	[TimeDied] [time](7) NOT NULL,
	[AgeDied] [int] NOT NULL,
	[Residence] [nvarchar](200) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[FatherName] [nvarchar](200) NOT NULL,
	[MotherName] [nvarchar](200) NOT NULL,
	[SpouseName] [nvarchar](200) NULL,
	[BurialPlace] [nvarchar](200) NOT NULL,
	[BurialDate] [date] NOT NULL,
	[ParishPriest] [nvarchar](200) NOT NULL,
	[DateIssue] [int] NOT NULL,
	[MonthIssue] [int] NOT NULL,
	[YearIssue] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Minister]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Minister](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MinisterId]  AS ((600000)+[id]) PERSISTED,
	[MinisterFirstName] [nvarchar](50) NOT NULL,
	[MinisterMiddleName] [nvarchar](50) NULL,
	[MinisterLastName] [nvarchar](50) NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MinisterRank]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MinisterRank](
	[MinisterId] [int] NOT NULL,
	[RankId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Person]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId]  AS ((1000000)+[id]) PERSISTED,
	[Firstname] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[BirthAddress1] [nvarchar](50) NOT NULL,
	[BirthAddress2] [nvarchar](50) NULL,
	[BirthCityState] [int] NOT NULL,
	[BirthProvince] [int] NOT NULL,
	[BirthCountry] [int] NOT NULL,
	[BirthDay] [datetime] NOT NULL,
	[Gender] [nvarchar](10) NOT NULL,
	[MotherFirstName] [nvarchar](50) NOT NULL,
	[MotherMiddleName] [nvarchar](50) NULL,
	[MotherLastName] [nvarchar](50) NOT NULL,
	[FatherFirstName] [nvarchar](50) NOT NULL,
	[FatherMiddleName] [nvarchar](50) NULL,
	[FatherLastName] [nvarchar](50) NOT NULL,
	[SpouseFirstName] [nvarchar](50) NULL,
	[SpouseMiddleName] [nvarchar](50) NULL,
	[SpouseLastName] [nvarchar](50) NULL,
	[Address1] [nvarchar](50) NOT NULL,
	[Address2] [nvarchar](50) NULL,
	[CityState] [int] NOT NULL,
	[Province] [int] NOT NULL,
	[Country] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rank]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rank](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RankId]  AS ((700000)+[id]) PERSISTED,
	[RankName] [nvarchar](50) NOT NULL,
	[RankDescription] [nvarchar](50) NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId]  AS ((500000)+[Id]) PERSISTED,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
	[Deleted] [bit] NULL,
	[AddedBy] [int] NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId]  AS ((100000)+[id]) PERSISTED,
	[Firstname] [nvarchar](50) NOT NULL,
	[MiddleName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Mobile] [nchar](20) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NOT NULL,
	[Address2] [nvarchar](50) NULL,
	[CityState] [int] NOT NULL,
	[Province] [int] NOT NULL,
	[Country] [int] NOT NULL,
	[ZipCode] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLogin]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLogin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserLoginId]  AS ((200000)+[id]) PERSISTED,
	[UserId] [int] NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLoginHistory]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLoginHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserLoginHistoryId]  AS ((300000)+[Id]) PERSISTED,
	[UserLoginId] [int] NOT NULL,
	[UserLoginTypeId] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserLoginType]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserLoginType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserLoginTypeId]  AS ((400000)+[Id]) PERSISTED,
	[Name] [nchar](10) NOT NULL,
	[Description] [varchar](150) NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateRemoved] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 8/20/2016 11:38:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[AddedBy] [int] NOT NULL,
	[UpdatedBy] [int] NOT NULL,
	[DeletedBy] [int] NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateUpdated] [datetime] NOT NULL,
	[DateDeleted] [datetime] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_UserRoleType_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_UserRoleType_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_UserRoleType_AddedBy]  DEFAULT ((0)) FOR [AddedBy]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_UserRoleType_UpdatedBy]  DEFAULT ((0)) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_UserRoleType_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [DF_UserRoleType_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_Users_Active]  DEFAULT ((1)) FOR [Active]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_Users_AddedBy]  DEFAULT ((0)) FOR [AddedBy]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_Users_UpdatedBy]  DEFAULT ((0)) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_Users_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_Users_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
ALTER TABLE [dbo].[UserLogin] ADD  CONSTRAINT [DF_UserLogin_AddedBy]  DEFAULT ((0)) FOR [AddedBy]
GO
ALTER TABLE [dbo].[UserLogin] ADD  CONSTRAINT [DF_UserLogin_UpdatedBy]  DEFAULT ((0)) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[UserLogin] ADD  CONSTRAINT [DF_UserLogin_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[UserLogin] ADD  CONSTRAINT [DF_UserLogin_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
ALTER TABLE [dbo].[UserLogin] ADD  CONSTRAINT [DF_UserLogin_DateDeleted]  DEFAULT (getdate()) FOR [DateDeleted]
GO
ALTER TABLE [dbo].[UserLoginHistory] ADD  CONSTRAINT [DF_UserLoginHistory_AddedBy]  DEFAULT ((0)) FOR [AddedBy]
GO
ALTER TABLE [dbo].[UserLoginHistory] ADD  CONSTRAINT [DF_UserLoginHistory_UpdatedBy]  DEFAULT ((0)) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[UserLoginHistory] ADD  CONSTRAINT [DF_UserLoginHistory_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[UserLoginHistory] ADD  CONSTRAINT [DF_UserLoginHistory_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
ALTER TABLE [dbo].[UserLoginType] ADD  CONSTRAINT [DF_UserLoginType_AddedBy]  DEFAULT ((0)) FOR [AddedBy]
GO
ALTER TABLE [dbo].[UserLoginType] ADD  CONSTRAINT [DF_UserLoginType_UpdatedBy]  DEFAULT ((0)) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[UserLoginType] ADD  CONSTRAINT [DF_UserLoginType_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[UserLoginType] ADD  CONSTRAINT [DF_UserLoginType_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_AddedBy]  DEFAULT ((0)) FOR [AddedBy]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_UpdatedBy]  DEFAULT ((0)) FOR [UpdatedBy]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
GO
ALTER TABLE [dbo].[UserRole] ADD  CONSTRAINT [DF_UserRole_DateUpdated]  DEFAULT (getdate()) FOR [DateUpdated]
GO
USE [master]
GO
ALTER DATABASE [SanVicParish] SET  READ_WRITE 
GO
