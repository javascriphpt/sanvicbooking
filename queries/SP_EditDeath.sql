IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_EditDeath')
BEGIN
	DROP PROCEDURE SP_EditDeath
END

GO

CREATE PROCEDURE SP_EditDeath(
	@Firstname NVARCHAR(50),
	@MiddleName NVARCHAR(50) = '',
	@LastName NVARCHAR(50),
	@BirthDay NVARCHAR(50),
	@Gender NVARCHAR(10),
	@MotherFirstName NVARCHAR(50),
	@MotherMiddleName NVARCHAR(50),
	@MotherLastName NVARCHAR(50),
	@FatherFirstName NVARCHAR(50),
	@FatherMiddleName NVARCHAR(50),
	@FatherLastName NVARCHAR(50),
	@SpouseFirstname NVARCHAR(50) = '',
	@SpouseMiddleName NVARCHAR(50)  = '',
	@SpouseLastName NVARCHAR(50)  = '',
	@Address1 NVARCHAR(50),
	@Address2 NVARCHAR(50),
	@CityState INT,
	@Province INT,
	@Country INT,
	@DeathRegisterPage INT,
	@DeathRegisterNumber INT,
	@DeathRegisterYear INT,
	@DeathRegisterVolume INT,
	@DeathAddress1 NVARCHAR(50),
	@DeathAddress2 NVARCHAR(50),
	@DeathCityState INT,
	@DeathProvince INT,
	@DeathCountry INT,
	@DateDied NVARCHAR(50),
	@TimeDied NVARCHAR(50),
	@Age INT,
	@BurialAddress1 NVARCHAR(50),
	@BurialAddress2 NVARCHAR(50),
	@BurialCityState INT,
	@BurialProvince INT,
	@BurialCountry INT,
	@BurialDate NVARCHAR(50),
	@UpdatedBy INT,
	@DeathPersonId INT
)

AS
BEGIN
	DECLARE @LatestId INT;
	DECLARE @PersonId INT;

	SELECT @PersonId = PersonId
	FROM DeathPerson
	WHERE DeathPersonId = @DeathPersonId

	UPDATE Person
	SET	Firstname = @Firstname,
		MiddleName = @MiddleName,
		LastName = @LastName,
		BirthDay = CONVERT(DATETIME,@BirthDay,101),
		Gender = @Gender,
		MotherFirstName = @MotherFirstName,
		MotherMiddleName = @MotherMiddleName,
		MotherLastName = @MotherLastName,
		FatherFirstName = @FatherFirstName,
		FatherMiddleName = @FatherMiddleName,
		FatherLastName = @FatherLastName,
		SpouseFirstName = @SpouseFirstName,
		SpouseMiddleName = @SpouseMiddleName,
		SpouseLastName = @SpouseLastName,
		Address1 = @Address1,
		Address2 = @Address2,
		CityState = @CityState,
		Province = @Province,
		Country = @Country,
		UpdatedBy = @UpdatedBy
	WHERE PersonId = @PersonId
	AND Active = 1


	UPDATE DeathPerson
	SET PersonId = @PersonId,
		DeathRegisterPage = @DeathRegisterPage,
		DeathRegisterNumber = @DeathRegisterNumber,
		DeathRegisterYear = @DeathRegisterYear,
		DeathRegisterVolume = @DeathRegisterVolume,
		DeathAddress1 = @DeathAddress1,
		DeathAddress2 = @DeathAddress2,
		DeathCityState = @DeathCityState,
		DeathProvince = @DeathProvince,
		DeathCountry = @DeathCountry,
		DateDied = CONVERT(DATE,@DateDied,101),
		TimeDied = CONVERT(TIME,@TimeDied,108),
		AgeDied = @Age,
		BurialAddress1 = @BurialAddress1,
		BurialAddress2 = @BurialAddress2,
		BurialCityState = @BurialCityState,
		BurialProvince = @BurialProvince,
		BurialCountry = @BurialCountry,
		BurialDate = CONVERT(DATETIME,@BurialDate,101),
		UpdatedBy = @UpdatedBy
	WHERE DeathPersonId = @DeathPersonId
	AND Active = 1

	SELECT @@ROWCOUNT as DeathPersonId
END
 
GO