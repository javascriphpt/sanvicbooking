IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		WHERE NAME = 'V_Confirmation'
		)
BEGIN
	DROP VIEW V_Confirmation
END
GO

CREATE VIEW V_Confirmation
AS
SELECT		ConfirmationPerson.ConfirmationPersonId,
			CONCAT( Person.Firstname, ' ', Person.MiddleName, ' ', Person.LastName) AS ConfirmationName,
			Person.Gender AS ConfirmationGender,
			CONCAT(Person.MotherFirstName,' ', Person.MotherMiddleName, ' ', Person.MotherLastName) AS ConfirmationMother,
			CONCAT(Person.FatherFirstName,' ', Person.FatherMiddleName, ' ', Person.FatherLastName) AS ConfirmationFather,
			CONVERT(VARCHAR(10), ConfirmationPerson.DateAdded, 101) AS ConfirmationDateConfirmed,


			
			CONVERT(VARCHAR(10), ConfirmationPerson.DateConfirmed, 101) AS DateConfirmed,
			
			ConfirmationPerson.Sponsor1Firstname,
			ConfirmationPerson.Sponsor1MiddleName,
			ConfirmationPerson.Sponsor1LastName,
			ConfirmationPerson.Sponsor2Firstname,
			ConfirmationPerson.Sponsor2MiddleName,
			ConfirmationPerson.Sponsor2LastName,
			ConfirmationPerson.ConfirmationBookNumber,
			ConfirmationPerson.ConfirmationPageNumber,
			ConfirmationPerson.ConfirmationAddress1,
			ConfirmationPerson.ConfirmationAddress2,

			CCity.Name as CCityState,
			CCity.CityId as CCityStateId,
			CProvince.Name as CProvince,
			CProvince.ProvinceId as CProvinceId,
			CCountry.Name as CCountry,
			CCountry.CountryId as CCountryId,

			Person.PersonId,
			
			Person.Firstname,
			Person.MiddleName,
			Person.LastName,
			
			Person.BirthAddress1,
			Person.BirthAddress2,
			BCity.Name as BirthCityState,
			BCity.CityId as BirthCityStateId,
			BProvince.Name as BirthProvince,
			BProvince.ProvinceId as BirthProvinceId,
			BCountry.Name as BirthCountry,
			BCountry.CountryId as BirthCountryId,

			CONVERT(VARCHAR(10), Person.BirthDay, 101) AS BirthDay,
			Person.Gender,
			Person.MotherFirstName,
			Person.MotherMiddleName,
			Person.MotherLastName,
			
			Person.FatherFirstName,
			Person.FatherMiddleName,
			Person.FatherLastName,
			Person.SpouseFirstName,
			Person.SpouseMiddleName,
			Person.SpouseLastName,
			
			Person.Address1,
			Person.Address2,
			RCity.Name as CityState,
			RCity.CityId as CityStateId,
			RProvince.Name as Province,
			RProvince.ProvinceId as ProvinceId,
			RCountry.Name as Country,
			RCountry.CountryId as CountryId,

			Minister.MinisterId,
			Minister.MinisterFirstName,
			Minister.MinisterMiddleName,
			Minister.MinisterLastName,
			[Rank].RankName			
FROM		ConfirmationPerson
INNER JOIN	Person
		ON	ConfirmationPerson.PersonId = Person.PersonId
INNER JOIN	Minister
		ON  ConfirmationPerson.MinisterId = Minister.MinisterId
INNER JOIN	MinisterRank
		ON	Minister.MinisterId = MinisterRank.MinisterId
INNER JOIN	[Rank]
		ON	MinisterRank.RankId = [Rank].RankId
LEFT  JOIN  City as RCity
		ON  Person.CityState = RCity.CityId
LEFT  JOIN  Province as RProvince
		ON  Person.Province = RProvince.ProvinceId
LEFT  JOIN  Country as RCountry
		ON  Person.Country = RCountry.CountryId
LEFT  JOIN  City as BCity
		ON  Person.CityState = BCity.CityId
LEFT  JOIN  Province as BProvince
		ON  Person.Province = BProvince.ProvinceId
LEFT  JOIN  Country as BCountry
		ON  Person.Country = RCountry.CountryId
LEFT  JOIN  City as CCity
		ON  ConfirmationPerson.ConfirmationCityState = CCity.CityId
LEFT  JOIN  Province as CProvince
		ON  ConfirmationPerson.ConfirmationProvince = CProvince.ProvinceId
LEFT  JOIN  Country as CCountry
		ON  ConfirmationPerson.ConfirmationCountry = CCountry.CountryId
WHERE		Person.Active = 1
AND			ConfirmationPerson.Active = 1

GO