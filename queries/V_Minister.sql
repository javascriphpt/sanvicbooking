IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		WHERE NAME = 'V_Minister'
		)
BEGIN
	DROP VIEW V_Minister
END
GO

CREATE VIEW V_Minister
AS
SELECT		Minister.MinisterId,
			Minister.MinisterFirstName,
			Minister.MinisterMiddleName,
			Minister.MinisterLastName,

			[Rank].RankName
FROM		Minister
INNER JOIN	MinisterRank
		ON	Minister.MinisterId = MinisterRank.MinisterId
INNER JOIN	[Rank]
		ON	MinisterRank.RankId = [Rank].RankId

GO