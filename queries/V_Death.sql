IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		WHERE NAME = 'V_Death'
		)
BEGIN
	DROP VIEW V_Death
END
GO

CREATE VIEW V_Death
AS
SELECT		DeathPerson.DeathPersonId,
			CONCAT( Person.Firstname, ' ', Person.MiddleName, ' ', Person.LastName) AS DeathName,
			CONVERT(VARCHAR(10), Person.BirthDay, 101) AS DeathBirthday,
			Person.Gender AS DeathGender,
			CONCAT(Person.MotherFirstName,' ', Person.MotherMiddleName, ' ', Person.MotherLastName) AS DeathMother,
			CONCAT(Person.FatherFirstName,' ', Person.FatherMiddleName, ' ', Person.FatherLastName) AS DeathFather,
			CONCAT(Person.SpouseFirstName,' ', Person.SpouseMiddleName, ' ', Person.SpouseLastName) AS DeathSpouse,
			CONCAT(Person.Address1,' ', Person.Address2, ' ', RCity.Name, ' ', RProvince.Name, ' ', RCountry.Name) AS DeathAddress,
			CONVERT(VARCHAR(10), DeathPerson.DateDied, 101) AS DeathDate,


			Person.Firstname,
			Person.MiddleName,
			Person.LastName,
			Person.Gender,
			Person.BirthDay,
			DeathPerson.AgeDied,
			DeathPerson.DateDied,


			DeathPerson.DeathAddress1,
			DeathPerson.DeathAddress2,
			DCity.Name as DeathCityState,
			DCity.CityId as DeathCityStateId,
			DProvince.Name as DeathProvince,
			DProvince.ProvinceId as DeathProvinceId,
			DCountry.Name as DeathCountry,
			DCountry.CountryId as DeathCountryId,
			CONVERT(VARCHAR(10), DeathPerson.TimeDied, 108) as TimeDied,
			DeathPerson.BurialAddress1,
			DeathPerson.BurialAddress2,
			BCity.Name as BurialCityState,
			BCity.CityId as BurialCityStateId,
			BProvince.Name as BurialProvince,
			BProvince.ProvinceId as BurialProvinceId,
			BCountry.Name as BurialCountry,
			BCountry.CountryId as BurialCountryId,
			CONVERT(VARCHAR(10), DeathPerson.BurialDate, 101) as BurialDate,

			DeathPerson.DeathRegisterPage,
			DeathPerson.DeathRegisterNumber,
			DeathPerson.DeathRegisterYear,
			DeathPerson.DeathRegisterVolume,

			Person.Address1,
			Person.Address2,
			RCity.Name as ResCityState,
			RCity.CityId as ResCityStateId,
			RProvince.Name as ResProvince,
			RProvince.ProvinceId as ResProvinceId,
			RCountry.Name as ResCountry,
			RCountry.CountryId as ResCountryId,

			Person.SpouseFirstName,
			Person.SpouseMiddleName,
			Person.SpouseLastName,

			Person.FatherFirstName,
			Person.FatherMiddleName,
			Person.FatherLastName,

			Person.MotherFirstName,
			Person.MotherMiddleName,
			Person.MotherLastName
					
FROM		DeathPerson
INNER JOIN	Person
		ON	DeathPerson.PersonId = Person.PersonId
LEFT  JOIN  City as RCity
		ON  Person.CityState = RCity.CityId
LEFT  JOIN  Province as RProvince
		ON  Person.Province = RProvince.ProvinceId
LEFT  JOIN  Country as RCountry
		ON  Person.Country = RCountry.CountryId

LEFT  JOIN  City as DCity
		ON  DeathPerson.DeathCityState= DCity.CityId
LEFT  JOIN  Province as DProvince
		ON  DeathPerson.DeathProvince = DProvince.ProvinceId
LEFT  JOIN  Country as DCountry
		ON  DeathPerson.DeathCountry = DCountry.CountryId

LEFT  JOIN  City as BCity
		ON  DeathPerson.BurialCityState= BCity.CityId
LEFT  JOIN  Province as BProvince
		ON  DeathPerson.BurialProvince = BProvince.ProvinceId
LEFT  JOIN  Country as BCountry
		ON  DeathPerson.BurialCountry = BCountry.CountryId

WHERE		Person.Active = 1
AND			DeathPerson.Active = 1

GO