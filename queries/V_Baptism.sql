IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		WHERE NAME = 'V_Baptism'
		)
BEGIN
	DROP VIEW V_Baptism
END
GO

CREATE VIEW V_Baptism
AS
SELECT		BaptismPerson.BaptismPersonId,
			CONCAT( Person.Firstname, ' ', Person.MiddleName, ' ', Person.LastName) AS BaptismPersonName,
			CONVERT(VARCHAR(10), Person.BirthDay, 101) AS BirthDay,
			CONCAT( Person.BirthAddress1, ' ', Person.BirthAddress2, ' ', BCity.Name, ' ', BProvince.Name, ' ', BCountry.Name) AS BaptismBirthPlace,
			Person.Gender AS BaptismGender,
			CONCAT(Person.MotherFirstName,' ', Person.MotherMiddleName, ' ', Person.MotherLastName) AS BaptismMother,
			CONCAT(Person.FatherFirstName,' ', Person.FatherMiddleName, ' ', Person.FatherLastName) AS BaptismFather,
			CONCAT(Person.Address1,' ', Person.Address2, ' ', RCity.Name, ' ', RProvince.Name, ' ', RCountry.Name) AS BaptismAddress,
			CONVERT(VARCHAR(10), BaptismPerson.DateBaptized, 101) AS BaptismDateBaptized,


			
			CONVERT(VARCHAR(10), BaptismPerson.DateBaptized, 101) AS DateBaptized,
			
			BaptismPerson.Sponsor1Firstname,
			BaptismPerson.Sponsor1MiddleName,
			BaptismPerson.Sponsor1LastName,
			BaptismPerson.Sponsor2Firstname,
			BaptismPerson.Sponsor2MiddleName,
			BaptismPerson.Sponsor2LastName,
			BaptismPerson.BaptismBookNumber,
			BaptismPerson.BaptismPageNumber,

			Person.PersonId,
			
			Person.Firstname,
			Person.MiddleName,
			Person.LastName,
			
			Person.BirthAddress1,
			Person.BirthAddress2,
			BCity.Name as BirthCityState,
			BCity.CityId as BirthCityStateId,
			BProvince.Name as BirthProvince,
			BProvince.ProvinceId as BirthProvinceId,
			BCountry.Name as BirthCountry,
			BCountry.CountryId as BirthCountryId,

			
			CONVERT(VARCHAR(10), Person.BirthDay, 101) AS BaptismBirthDay,
			Person.Gender,
			
			
			Person.MotherFirstName,
			Person.MotherMiddleName,
			Person.MotherLastName,
			
			Person.FatherFirstName,
			Person.FatherMiddleName,
			Person.FatherLastName,
			Person.SpouseFirstName,
			Person.SpouseMiddleName,
			Person.SpouseLastName,
			
			Person.Address1,
			Person.Address2,
			RCity.Name as CityState,
			RCity.CityId as CityStateId,
			RProvince.Name as Province,
			RProvince.ProvinceId as ProvinceId,
			RCountry.Name as Country,
			RCountry.CountryId as CountryId,

			Minister.MinisterId,
			Minister.MinisterFirstName,
			Minister.MinisterMiddleName,
			Minister.MinisterLastName,
			[Rank].RankName			
FROM		BaptismPerson
INNER JOIN	Person
		ON	BaptismPerson.PersonId = Person.PersonId
INNER JOIN	Minister
		ON  BaptismPerson.MinisterId = Minister.MinisterId
INNER JOIN	MinisterRank
		ON	Minister.MinisterId = MinisterRank.MinisterId
INNER JOIN	[Rank]
		ON	MinisterRank.RankId = [Rank].RankId
INNER JOIN  City as RCity
		ON  Person.CityState = RCity.CityId
INNER JOIN  Province as RProvince
		ON  Person.Province = RProvince.ProvinceId
INNER JOIN  Country as RCountry
		ON  Person.Country = RCountry.CountryId
INNER JOIN  City as BCity
		ON  Person.CityState = BCity.CityId
INNER JOIN  Province as BProvince
		ON  Person.Province = BProvince.ProvinceId
INNER JOIN  Country as BCountry
		ON  Person.Country = RCountry.CountryId
WHERE		Person.Active = 1
AND			BaptismPerson.Active = 1

GO