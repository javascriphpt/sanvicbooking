IF EXISTS (
		SELECT *
		FROM sys.VIEWS
		WHERE NAME = 'V_ProvinceCountry'
		)
BEGIN
	DROP VIEW V_ProvinceCountry
END
GO

CREATE VIEW V_ProvinceCountry
AS
SELECT		Province.ProvinceId,
			Province.Code,
			Province.Name,
			ProvinceCountry.CountryId
FROM		Province
INNER JOIN	ProvinceCountry
		ON	Province.ProvinceId = ProvinceCountry.ProvinceId

GO