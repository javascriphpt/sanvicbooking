IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_ChangeLoginStatus')
BEGIN
	DROP PROCEDURE SP_ChangeLoginStatus
END

GO

CREATE PROCEDURE SP_ChangeLoginStatus(
	@UserLoginId INT,
	@UserLoginType INT,
	@Return INT = 0
)

AS
BEGIN
	INSERT INTO UserLoginHistory(
		UserLoginId,
		UserLoginTypeId
	) VALUES (
		@UserLoginId,
		@UserLoginType
	)

	IF @Return = 1
	BEGIN
		SELECT MAX(UserLoginHistoryId) as UserLoginHistoryId
		FROM UserLoginHistory
		WHERE UserLoginId = @UserLoginId
	END
END

GO