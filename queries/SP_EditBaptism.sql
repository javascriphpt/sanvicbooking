IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_EditBaptism')
BEGIN
	DROP PROCEDURE SP_EditBaptism
END

GO

CREATE PROCEDURE SP_EditBaptism(
	@BaptismPersonId int,
	@Firstname NVARCHAR(50),
	@MiddleName NVARCHAR(50) = '',
	@LastName NVARCHAR(50),
	@BirthAddress1 NVARCHAR(50),
	@BirthAddress2 NVARCHAR(50) = '',
	@BirthCityState int,
	@BirthProvince int,
	@BirthCountry int,
	@BirthDay NVARCHAR(50),
	@Gender NVARCHAR(10),
	@MotherFirstName NVARCHAR(50),
	@MotherMiddleName NVARCHAR(50),
	@MotherLastName NVARCHAR(50),
	@FatherFirstName NVARCHAR(50),
	@FatherMiddleName NVARCHAR(50),
	@FatherLastName NVARCHAR(50),
	@Address1 NVARCHAR(50),
	@Address2 NVARCHAR(50),
	@CityState int,
	@Province int,
	@Country int,
	@DateBaptized NVARCHAR(50),
	@MinisterId int,
	@Sponsor1Firstname NVARCHAR(50) = '',
	@Sponsor1MiddleName NVARCHAR(50)  = '',
	@Sponsor1LastName NVARCHAR(50)  = '',
	@Sponsor2Firstname NVARCHAR(50)  = '',
	@Sponsor2MiddleName NVARCHAR(50)  = '',
	@Sponsor2LastName NVARCHAR(50)  = '',
	@BaptismBookNumber int,
	@BaptismPageNumber int,
	@UpdatedBy int
)

AS
BEGIN
	DECLARE @PersonId INT;

	SELECT @PersonId = PersonId
	FROM BaptismPerson
	WHERE BaptismPersonId = @BaptismPersonId

	UPDATE 	Person
	SET		Firstname = @Firstname,
			MiddleName = @MiddleName,
			LastName = @LastName,
			BirthAddress1 = @BirthAddress1,
			BirthAddress2 = @BirthAddress2,
			BirthCityState =@BirthCityState ,
			BirthProvince = @BirthProvince,
			BirthCountry = @BirthCountry,
			BirthDay = CONVERT(DATETIME,@BirthDay,101),
			Gender = @Gender,
			MotherFirstName = @MotherFirstName,
			MotherMiddleName = @MotherMiddleName,
			MotherLastName = @MotherLastName,
			FatherFirstName = @FatherFirstName,
			FatherMiddleName = @FatherMiddleName,
			FatherLastName = @FatherLastName,
			Address1 = @Address1,
			Address2 = @Address2,
			CityState = @CityState,
			Province = @Province,
			Country = @Country,
			UpdatedBy = @UpdatedBy
	WHERE 	PersonId = @PersonId
	AND 	Active = 1

	UPDATE 	BaptismPerson
	SET 	DateBaptized = CONVERT(DATETIME,@DateBaptized,101),
			MinisterId = @MinisterId,
			Sponsor1Firstname = @Sponsor1Firstname,
			Sponsor1MiddleName = @Sponsor1MiddleName,
			Sponsor1LastName = @Sponsor1LastName,
			Sponsor2Firstname = @Sponsor2Firstname,
			Sponsor2MiddleName = @Sponsor2MiddleName,
			Sponsor2LastName = @Sponsor2LastName,
			BaptismBookNumber = @BaptismBookNumber,
			BaptismPageNumber = @BaptismPageNumber,
			UpdatedBy = @UpdatedBy
	WHERE 	BaptismPersonId = @BaptismPersonId
	AND 	Active = 1

	SELECT @@ROWCOUNT as BaptismPersonId
END
 
GO