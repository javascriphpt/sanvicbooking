IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_DeleteConfirmation')
BEGIN
	DROP PROCEDURE SP_DeleteConfirmation
END

GO

CREATE PROCEDURE SP_DeleteConfirmation(
	@ConfirmationPersonId int,
	@DeletedBy int
)

AS
BEGIN
	DECLARE @PersonId INT;

	SELECT	@PersonId = PersonId
	FROM	ConfirmationPerson
	WHERE	ConfirmationPersonId = @ConfirmationPersonId

	UPDATE	BaptismPerson
	SET		Active = 0,
			DeletedBy = @DeletedBy,
			DateDeleted = GETDATE()
	WHERE BaptismPersonId = @ConfirmationPersonId

	UPDATE	Person
	SET		Active = 0,
			DeletedBy = @DeletedBy,
			DateDeleted = GETDATE()
	WHERE	PersonId = @PersonId

	SELECT @@ROWCOUNT as UpdatedRows
END
 
GO