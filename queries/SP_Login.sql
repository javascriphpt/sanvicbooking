IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_Login')
BEGIN
	DROP PROCEDURE SP_Login
END

GO

CREATE PROCEDURE SP_Login(
	@Username VARCHAR(50),
	@Password VARCHAR(50)
)

AS
BEGIN
	DECLARE @UserLoginId INT;
	DECLARE @UserId INT;

	SELECT @UserLoginId = UserLogin.UserLoginId,
			@UserId = [User].UserId
	FROM	UserLogin
	INNER JOIN [User]
			ON UserLogin.UserId = [User].UserId
	WHERE	UserLogin.Username = @Username
	AND		UserLogin.Password = @Password

	IF @UserLoginId > 0
	BEGIN

		EXEC SP_ChangeLoginStatus @UserLoginId, 400001;

		SELECT @UserLoginId AS UserLoginId,
			   @UserId AS UserId
	END
	ELSE
	BEGIN
		SELECT 0 AS UserLoginId,
			   0 AS UserId
	END
END

GO