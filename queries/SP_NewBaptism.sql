IF EXISTS(SELECT * FROM sys.procedures where name = 'SP_NewBaptism')
BEGIN
	DROP PROCEDURE SP_NewBaptism
END

GO

CREATE PROCEDURE SP_NewBaptism(
	@Firstname NVARCHAR(50),
	@MiddleName NVARCHAR(50) = '',
	@LastName NVARCHAR(50),
	@BirthAddress1 NVARCHAR(50),
	@BirthAddress2 NVARCHAR(50) = '',
	@BirthCityState int,
	@BirthProvince int,
	@BirthCountry int,
	@BirthDay NVARCHAR(50),
	@Gender NVARCHAR(10),
	@MotherFirstName NVARCHAR(50),
	@MotherMiddleName NVARCHAR(50),
	@MotherLastName NVARCHAR(50),
	@FatherFirstName NVARCHAR(50),
	@FatherMiddleName NVARCHAR(50),
	@FatherLastName NVARCHAR(50),
	@Address1 NVARCHAR(50),
	@Address2 NVARCHAR(50),
	@CityState int,
	@Province int,
	@Country int,
	@DateBaptized NVARCHAR(50),
	@MinisterId int,
	@Sponsor1Firstname NVARCHAR(50) = '',
	@Sponsor1MiddleName NVARCHAR(50)  = '',
	@Sponsor1LastName NVARCHAR(50)  = '',
	@Sponsor2Firstname NVARCHAR(50)  = '',
	@Sponsor2MiddleName NVARCHAR(50)  = '',
	@Sponsor2LastName NVARCHAR(50)  = '',
	@BaptismBookNumber int,
	@BaptismPageNumber int,
	@AddedBy int
)

AS
BEGIN
	DECLARE @LatestId INT;
	DECLARE @PersonId INT;

	INSERT INTO Person (
		Firstname,
		MiddleName,
		LastName,
		BirthAddress1,
		BirthAddress2,
		BirthCityState,
		BirthProvince,
		BirthCountry,
		BirthDay,
		Gender,
		MotherFirstName,
		MotherMiddleName,
		MotherLastName,
		FatherFirstName,
		FatherMiddleName,
		FatherLastName,
		Address1,
		Address2,
		CityState,
		Province,
		Country,
		AddedBy,
		Active
	) VALUES (
		@Firstname,
		@MiddleName,
		@LastName,
		@BirthAddress1,
		@BirthAddress2,
		@BirthCityState,
		@BirthProvince,
		@BirthCountry,
		CONVERT(DATETIME,@BirthDay,101),
		@Gender,
		@MotherFirstName,
		@MotherMiddleName,
		@MotherLastName,
		@FatherFirstName,
		@FatherMiddleName,
		@FatherLastName,
		@Address1,
		@Address2,
		@CityState,
		@Province,
		@Country,
		@AddedBy,
		1
	)

	SET @LatestId = SCOPE_IDENTITY();

	SELECT @PersonId = PersonId
	FROM Person
	WHERE Id = @LatestId

	INSERT INTO BaptismPerson(
		PersonId,
		DateBaptized,
		MinisterId,
		Sponsor1Firstname,
		Sponsor1MiddleName,
		Sponsor1LastName,
		Sponsor2Firstname,
		Sponsor2MiddleName,
		Sponsor2LastName,
		BaptismBookNumber,
		BaptismPageNumber,
		AddedBy,
		Active
	) VALUES (
		@PersonId,
		CONVERT(DATETIME,@DateBaptized,101),
		@MinisterId,
		@Sponsor1Firstname,
		@Sponsor1MiddleName,
		@Sponsor1LastName,
		@Sponsor2Firstname,
		@Sponsor2MiddleName,
		@Sponsor2LastName,
		@BaptismBookNumber,
		@BaptismPageNumber,
		@AddedBy,
		1
	)

	SET @LatestId = SCOPE_IDENTITY();

	SELECT BaptismPersonId
	FROM BaptismPerson
	WHERE Id = @LatestId
END
 
GO